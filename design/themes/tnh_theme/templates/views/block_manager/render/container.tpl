{*<div class="{if $layout_data.layout_width != "fixed"}{else}off-canvas-wrap off-canvas-wrapper"{/if} {$container.user_class}">
    {$content nofilter}
</div>*}
{if $container.user_class}
    <div class="{$container.user_class}">
{/if}
{$content nofilter}
{if $container.user_class}
    </div>
{/if}