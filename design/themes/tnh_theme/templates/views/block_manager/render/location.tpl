<div class="off-canvas-wrap off-canvas-wrapper">
<div id="innerWrap" class="inner-wrap off-canvas-wrapper-inner fixed-header device-pc home-page" data-off-canvas-wrapper>
    <div class="header fixed-wrap grap wide-screen headerContainer">
{if $containers.top_panel}
    {$containers.top_panel nofilter}
{/if}

{if $containers.header}
    {$containers.header nofilter}
{/if}

</div>
<!--End Header-->
{if $containers.content}
    {$containers.content nofilter}


                        <div class="row boxTab">
                            <div class="small-12 columns">
                                <ul class="tabs curation-tabs show-for-large" data-tabs id="multi-box-tabs">
                                    <li aria-selected='true'  class='tabs-title is-active'><a class='' href='#panel1_71553'>Electronics</a></li>
                                    <li aria-selected='true'  class='tabs-title'><a class='' href='#panel1_71554'>Electronics 2</a></li>
                                </ul>        
                                <!--Tabs-->
                                <div class="row ">
                                    <div class="small-12 columns">
                                        <div class="tabs-content curation-tabs-content" data-tabs-content="multi-box-tabs">
                                            <div class="hide-for-large curation-tabs"> 
                                                <div class="small-8 columns">
                                                    <h3 class="mobile-curation-selected-tab">Electronics</h3>
                                                </div>
                                                <!--For mobile-->
                                                <div class="small-4 columns curation-tabs-menu">
                                                    <a href="javascript:void(0);" data-toggle="categories_71553" aria-expanded="false"> more <i class="arrow-down"></i></a>
                                                </div>
                                                <div class="small-12 columns" style="position:relative;">
                                                    <ul id="categories_71553" class="f-dropdown curation-tab-box-dropdown dropdown-pane bottom" data-dropdown-content="categoriesList" data-dropdown aria-hidden="true" data-close-on-click="true" aria-autoclose="true" tabindex="-1">
                                                        <li class=""><a href="#" data-link="#panel1_71553">Electronics</a></li>                              
                                                        <li class=""><a href="#" data-link="#panel1_71553">Electronics</a></li>                              
                                                    </ul>
                                                </div>
                                                <!--End For mobile-->
                                            </div>

                                            <div id="panel1_71553" data-track-banner="multipleTabsBox6957" class="tabs-panel row content  is-active">
                                                <div class="small-12 large-6 columns">
                                                    <div class="featured-category">
                                                        <a href="" prefix="" data-tracking="">
                                                            <img src="templates/public/style/img/blank.gif" alt="" data-lazy="templates/images/1501393019_Tab2-RI1-Mobiles-Tablets.jpg" 
                                                                 data-interchange="[templates/images/S_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, small],
                                                                 [templates/images/M_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, medium],
                                                                 [templates/images/L_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, large],
                                                                 [templates/images/XL_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, xlarge],
                                                                 [templates/images/1501393019_Tab2-RI1-Mobiles-Tablets.jpg, default]" />
                                                            <h4>Mobiles & Tablets</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="small-12 large-6 columns">
                                                    <div class="row">    
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="" prefix="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="Televisions" data-lazy="templates/images/1501393019_Tab2-RI2-Televisions.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI2-Televisions.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI2-Televisions.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI2-Televisions.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI2-Televisions.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI2-Televisions.jpg, default]" />
                                                                    <h4>Televisions</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="" data-lazy="templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI3-DSLR-Cameras.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI3-DSLR-Cameras.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI3-DSLR-Cameras.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI3-DSLR-Cameras.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg, default]" />
                                                                    <h4>DSLR Cameras</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">    
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="" prefix="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="Televisions" data-lazy="templates/images/1501393019_Tab2-RI2-Televisions.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI2-Televisions.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI2-Televisions.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI2-Televisions.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI2-Televisions.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI2-Televisions.jpg, default]" />
                                                                    <h4>Televisions</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="" data-lazy="templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI3-DSLR-Cameras.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI3-DSLR-Cameras.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI3-DSLR-Cameras.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI3-DSLR-Cameras.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg, default]" />
                                                                    <h4>DSLR Cameras</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panel1_71554" class="tabs-panel row content">Hello</div>
                                        </div>
                                    </div>
                                </div>
                                <!--/Tabs-->
                            </div>
                        </div>
    
                        <div class="row" style="margin-bottom:2rem;">
                            <div class="columns small-12 medium-12 large-12 hide-for-small-only"> 
                                <a href="">
                                    <img src="templates/images/standalone-best-buys-ae-en.gif" alt=""></a>
                            </div>
                            <div class="columns small-12 medium-12 large-12 show-for-small-only"> 
                                <a href="">
                                    <img src="templates/images/best-buys-standalone-small-ae-en.gif" alt=""></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="small-12 columns">
                                <h3 class="slider-header recommendation-title"><a href="javascript:void(0);">Popular Products</a></h3>
                            </div>
                        </div>

                        <div class="sliderViewPort row PopularItems bottom-spacing ">
                            <div class="small-12 columns clearPadding-h">
                                <div class="multi-slider preSlickSlider" data-no-of-items="5" data-no-of-items-for-xlarge="7" dir="ltr"  data-track-slider='PopularItems7109'>
                                        
                                        <div class="slide hide">
                                            <div class="mini-placard">
                                                <a href="" class="itemLink" data-enid="2724326956718" data-unitid="19293000322" data-itemprice="2" data-position="1" data-brand-name="|Other" data-category-name="Sporting Goods" title="">&nbsp;</a>
                                                <div class="img-bucket">
                                                    <span class="align-image-vertically">
                                                        <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                    </span>
                                                </div>
                                                <h6 class="title"><a href="" title="">Skull print Multi-Purpose Scarf/Bandana/Mask</a></h6>
                                                <div class="stars-rating">
                                                    <div class="star-rating clearMargin-v-large">
                                                        <a href="javascript:void(0);">
                                                            <div class="stars">
                                                                <div class="colored" style="width: 76%; ">
                                                                    <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                </div>
                                                                <div class="uncolored">
                                                                    <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                </div>
                                                            </div>
                                                        </a>&nbsp;&nbsp;<span class="rating-reviews">(344)</span>                            </div>
                                                </div>
                                                <h5 class="price">
                                                    <span class="was block">3.00</span>
                                                    <span class= "is block sk-clr1">2.00 <small class="currency-text sk-clr1">AED</small></span>
                                                </h5>
                                                <div class="recommendations-fsp-badge ">
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="slide hide">
                                            <div class="mini-placard">
                                                <a href="" class="itemLink" data-enid="2724326956718" data-unitid="19293000322" data-itemprice="2" data-position="1" data-brand-name="|Other" data-category-name="Sporting Goods" title="">&nbsp;</a>
                                                <div class="img-bucket">
                                                    <span class="align-image-vertically">
                                                        <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                    </span>
                                                </div>
                                                <h6 class="title"><a href="" title="">Skull print Multi-Purpose Scarf/Bandana/Mask</a></h6>
                                                <div class="stars-rating">
                                                    <div class="star-rating clearMargin-v-large">
                                                        <a href="javascript:void(0);">
                                                            <div class="stars">
                                                                <div class="colored" style="width: 76%; ">
                                                                    <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                </div>
                                                                <div class="uncolored">
                                                                    <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                </div>
                                                            </div>
                                                        </a>&nbsp;&nbsp;<span class="rating-reviews">(344)</span>                            </div>
                                                </div>
                                                <h5 class="price">
                                                    <span class="was block">3.00</span>
                                                    <span class= "is block sk-clr1">2.00 <small class="currency-text sk-clr1">AED</small></span>
                                                </h5>
                                                <div class="recommendations-fsp-badge ">
                                                </div>
                                            </div>
                                        </div> 
                                    
                                </div>
                            </div>
                        </div>



                        <div class="row boxTab">
                            <div class="small-12 columns filter-tabs-container">
                                <div class="row filter-tabs-header">
                                    <div class="small-6 medium-4 large-6 columns">
                                        <h3 class="slider-header">Electronics</h3>
                                    </div>
                                    <div class="small-6 medium-8 large-6 ddown-container columns">
                                        <a href="javascript:void(0);" class="tiny secondary button expand clearMargin show-for-small-only" data-toggle="tabsDropdownLarge70160">
                                            <i class="fi-list"></i><span class='value'> Laptops</span>
                                        </a>
                                        <ul id="tabsDropdownLarge70160" data-dropdown data-close-on-click="true" class="dropdown-pane f-dropdown filter-group">
                                            <li class="hide"><a href="javascript:void(0);" class="hide" data-id-box="70160" data-id-panel="panel70160">Laptops</a></li>
                                            <li><a href="javascript:void(0);" class="" data-id-box="70161" data-id-panel="panel70160">TVs</a></li>
                                            <li><a href="javascript:void(0);" class="" data-id-box="70162" data-id-panel="panel70160">Accessories</a></li>
                                            <li><a href="javascript:void(0);" class="" data-id-box="70164" data-id-panel="panel70160">Gaming</a></li>
                                        </ul>
                                        
                                        <div class="clearfix show-for-medium">
                                            <ul class="tabs home-tabs" data-tabs>
                                                <li class="tabs-title loaded is-active"><a data-id-box="70160" href="#panel70160">Laptops 1</a>
                                                <li class="tabs-title"><a data-id-box="70161" href="#panel70160">TVs</a></li>
                                                <li class="tabs-title"><a data-id-box="70162" href="#panel70160">Accessories</a></li>
                                                <li class="tabs-title"><a data-id-box="70164" href="#panel70160">Gaming</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                <!--Tabs-->
                                <div class="row box-tab-slider">
                                    <div class="small-12 columns clearPadding-h-large">
                                        <div class="tabs-content" data-tabs-content>
                                            <div class="content is-active loaded" id="panel70160">
                                                <div class="sliderViewPort row">
                                                    <div class="small-12 columns clearPadding-h">
                                                        <div class="multi-slider preSlickSlider " data-no-of-items="5" data-no-of-items-for-xlarge="6" dir="ltr"  data-track-slider='7099'>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="small-12 columns viewAllLink">
                                                            <a href="" class="fr"> View All Laptops </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/Tabs-->
                            </div>
                        </div>

{/if}
                <footer class="wide-screen">
                    <div class="first-row row">
                        <div class="large-3 large-push-9 columns">
                            <div class="callout callout-panel radius customer-service-panel">
                                <h6>Customer Service</h6>
                                <ul class="footer-links">
                                    <li><a href=""><i class="fi-mail"></i> Contact us</a></li>
                                    <li><span rel="tel">(+971)-4-437 0900</span> </li>
                                </ul>
                            </div>
                            <h6 class="">Follow Us</h6>
                            <ul class="menu clearfix">
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-twitter"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-facebook"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-youtube"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-instagram"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-pinterest"></i></a></li>
                            </ul>
                            <h6>Download our apps</h6>
                            <ul class="menu">
                                <li><a href="" class="social"><i class='fi-social-android'></i></a> </li>
                                <li><a href="" class="social"><i class='fi-social-apple'></i></a> </li>
                            </ul>
                        </div>
                        <div class="large-3 large-pull-3 columns show-for-medium">
                            <h6>Popular Searches</h6>
                            <ul class="footer-links">
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                            </ul>
                        </div>
                        <div class="large-3 large-pull-3 columns">
                            <h6>My Account</h6>

                            <ul class="footer-links">
                                <li id="loginLogoutReplace">
                                    <a href="">Log In</a> <a href=""> / Register</a>
                                </li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                            </ul>

                            <h6>Intellectual Property</h6>
                            <ul class="footer-links">
                                <li><a href="">Brand and Copyright Owners</a></li>
                                <li><a href="">Marketplace Sellers</a></li>
                            </ul>
                        </div>
                        <div class="large-3 large-pull-3 columns show-for-medium">
                            <h6>Selling on Souq.com</h6>
                            <ul class="footer-links">
                                <li><a href="">Sell on souq.com</a></li>
                                <li><a href="">How It Works</a></li>
                            </ul>

                            <h6>Buying on Souq.com</h6>
                            <ul class="footer-links">
                                <li><a href="">How to Buy</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="second-row">
                        <div class="row">
                            <div class="large-9 columns">
                                <div class="row">
                                    <div class="small-12 columns">
                                        <strong>&copy; 2017 Souq.com</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 columns footer-lower-links">
                                        <ul class="h-list text-default">
                                            <li><a href="">About Us</a></li>
                                            <li><a href="">Media Center</a></li>
                                            <li><a href="">Careers</a></li>
                                            <li><a href="">Privacy Policy</a></li>
                                            <li><a href="">Terms and Conditions</a></li>
                                            <li><a href="">Affiliate Program</a></li>
                                            <li><a href="">Helpbit Services</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="small-12 large-1 end text-center columns clearPadding-h">

                            </div>
                        </div>
                    </div>
                </footer>
            


{if $containers.footer}
    {$containers.footer nofilter}
{/if}
</div>
</div>

