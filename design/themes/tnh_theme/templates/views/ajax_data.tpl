<div class="sliderViewPort row bestMatchItems">
    <div class="small-12 columns clearPadding-h">
        <div class="multi-slider preSlickSlider" data-no-of-items="5" data-no-of-items-for-xlarge="6" dir="ltr"  data-track-slider='6265'>
            
                {foreach from=$products item=product}
                    {if $product}
                    {assign var="obj_id" value=$product.product_id}
                    {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
                    {include file="common/product_data.tpl" product=$product}
                    <div class="slide hide">
                        <div class="mini-placard  ">
                            <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="{$product.product}">&nbsp;</a>
                            <div class="img-bucket">
                                <span class="align-image-vertically">
                                    <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" title="{$product.product}"><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                </span>
                                <span class="discount">10 %  off</span>                                                                                </div>
                            <h6 class="title"><a href="" title="">{$product.product}</a></h6>
                            <div class="stars-rating">
                                <div class="star-rating clearMargin-v-large">
                                    <a href="javascript:void(0);">
                                        <div class="stars">
                                            <div class="colored" style="width: 50%; ">
                                                <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                            </div>
                                            <div class="uncolored">
                                                <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                            </div>
                                        </div>
                                    </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                </div>
                            </div>
                            <h5 class="price">
                                <span class="was block"></span>
                                <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                            </h5>
                            <div class="recommendations-fsp-badge ">
                                <div class="block fs-ab-black">
                                    <strong class='green-text'>FREE Shipping</strong>
                                </div>
                                <div class="block">
                                    <div class="fbs"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/if}
                {/foreach}
            
        </div>
        <div class="small-12 columns viewAllLink">
            <a href="" class="fr"> View All TVs </a>
        </div>
    </div>
</div>