<!DOCTYPE html>
<html {hook name="index:html_tag"}{/hook} lang="{$smarty.const.CART_LANGUAGE}" dir="{$language_direction}">
<head>
{capture name="page_title"}
{hook name="index:title"}
{if $page_title}
    {$page_title}
{else}
    {foreach from=$breadcrumbs item=i name="bkt"}
        {if $language_direction == 'rtl'}
            {if !$smarty.foreach.bkt.last}{if !$smarty.foreach.bkt.last && !$smarty.foreach.bkt.first} :: {/if}{$i.title|strip_tags}{/if}
        {else}
            {if !$smarty.foreach.bkt.first}{$i.title|strip_tags}{if !$smarty.foreach.bkt.last} :: {/if}{/if}
        {/if}
    {/foreach}
    {if !$skip_page_title && $location_data.title}{if $breadcrumbs|count > 1} - {/if}{$location_data.title}{/if}
{/if}
{/hook}
{/capture}
<title>{$smarty.capture.page_title|strip|trim nofilter}</title>
{include file="meta.tpl"}
{hook name="index:links"}
    <link href="{$logos.favicon.image.image_path|fn_query_remove:'t'}" rel="shortcut icon" type="{$logos.favicon.image.absolute_path|fn_get_mime_content_type}" />
{/hook}
<link rel="stylesheet" type="text/css" href="design/themes/tnh_theme/templates/css/souq.93738d748552c5b631465c4173dabdf9.css" media="all" />
<link rel="stylesheet" type="text/css" href="design/themes/tnh_theme/templates/css/home.93738d748552c5b631465c4173dabdf9.css" media="all" />
<link rel="stylesheet" type="text/css" href="design/themes/tnh_theme/templates/css/custom.css" media="all" />

{literal}
<script type="text/javascript">
    var itemVariances = {};
    var globals = {
        baseUrl: '',
        HOST: '',
        HOSTSSL: '',
        curationUrl: '',
        STATICHOST: 'https://cf1.s3.souqcdn.com/public/style/img/en/',
        STATICCOMMONHOST: 'https://cf1.s3.souqcdn.com/public/style/img/',
        customerCurrency: 'AED',
        moneyExample: '10.50',
        currencyFraction: '2',
        fractionSeparator: '.',
        thousandSeparator: ',',
        language: 'en',
        site: 'ae',
        country: 'ae',
        sitesList: ["ae", "eg", "sa", "kw"],
        siteCountries: ["bh", "om", "qa", "ae"],
        cookieDomain: '.songviytuong.com',
        action: 'index',
        controller: 'home',
        bannerItemTypeName: 'homepage',
        targetedGroup: '',
        context: 'browser',
        debug: false,
        LoggerService: '',
        env: 'live',
        locale: 'en-US',
        modules: ['home'],
        device: 'pc',
        actualDevice: 'pc',
        deepLink: '',
        androidPackage: 'com.souq.app',
        windowsMarket: '',
        androidMarket: '',
        iOSMarket: '',
        subDomains: '{"ae":"uae","sa":"saudi","eg":"egypt","kw":"kuwait"}',
        jsLogging: 0,
        subDomains: {"ae": "uae", "sa": "saudi", "eg": "egypt", "kw": "kuwait"},
        itemTypes: "",
        ditemType: "",
        phalconVersion: "2.0.8",
        search_string: "",
        user_ip: "113.161.37.58",
        custom_filter: null,
        itemSettings: null,
        adnHost: "x.sokrati.com/",
        adnEnabled: "1",
        pickupAreaUsed: 0,
        pickupEnableMap: 0,
        selectedPickupAddress: 0,
        FBAPPID: 501126536647943,
        appBoyKey: "2e4ae497-9aed-4a69-8a2d-91cd396ab384",
        gtm_id: "GTM-N2RCPF",
        AppBoySafariPushId: "web.com.souq.mweb",
        bInAppMessage: "1",
        layoutSettings: {
            ApplyZoomInGallry: ""
        }
    };

    globals.DFP = {"country": "ae", "language": "en"};
    globals.APPBOY = null;
    globals.AppBoyCustomEvent = null;
    globals.bins = null;
    globals.itemBins = null;
    globals.promoPayments = null;
</script>{/literal}
<script type="text/javascript" src="design/themes/tnh_theme/templates/js/loader.93738d748552c5b631465c4173dabdf9.js"></script>
{hook name="index:head_scripts"}{/hook}
</head>

<body id="Ad-body-holder">
    {hook name="index:body"}
        {hook name="index:content"}
            {render_location}
        {/hook}
{*        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/libs.93738d748552c5b631465c4173dabdf9.js"></script>*}
        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/libs.bk.js"></script>
{*        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/index.93738d748552c5b631465c4173dabdf9.js"></script>*}
        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/index.bk.js"></script>
{*        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/souq.93738d748552c5b631465c4173dabdf9.js"></script>*}
        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/souq.bk.js"></script>
        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/s_code_new.js"></script>
        <script type="text/javascript" src="design/themes/tnh_theme/templates/js/s_plugin_new.js"></script>
        {include file="common/scripts.tpl"}
    {/hook}
</body>
</html>
