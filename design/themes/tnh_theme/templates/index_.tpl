﻿<!doctype html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale = 1.0">
        <title>PHP - Shop in Dubai, Abu Dhabi, UAE | Online Shopping, Best Place to Buy and Sell Electronics, Fashion, Clothing, Watches, Books, Grocery and more Deals | Souq.com</title>
        <meta name="robots" content="index,follow">
        <meta name="description" content="The largest e-commerce site in the Arab world, where thousands of buyers &amp; sellers buy and sell products over a trusted platform that offers various local payment options and integrated delivery methods.">
        <meta name="keywords" content="Shop in Dubai, Shop in  Abu Dhabi, Shop in UAE, Electronics, Computers, Mobile Phones, Fashion Brands, Clothing, Online Shopping, Deals, UAE, Dubai, UAE, Souq, Buy, Sell, Online Market">
        <link rel="stylesheet" type="text/css" href="templates/css/souq.93738d748552c5b631465c4173dabdf9.css" />
        <link rel="stylesheet" type="text/css" href="templates/css/home.93738d748552c5b631465c4173dabdf9.css" />
        <link rel="stylesheet" type="text/css" href="templates/css/custom.css" />
        <link rel="shortcut icon" type="image/x-icon" href="templates/images/souq-new.ico" />
        {literal}
        <script type="text/javascript">
            var itemVariances = {};
            var globals = {
                baseUrl: 'https://cf1.s3.souqcdn.com/public/dist/js/',
                HOST: '',
                HOSTSSL: '',
                curationUrl: '',
                STATICHOST: 'https://cf1.s3.souqcdn.com/public/style/img/en/',
                STATICCOMMONHOST: 'https://cf1.s3.souqcdn.com/public/style/img/',
                customerCurrency: 'AED',
                moneyExample: '10.50',
                currencyFraction: '2',
                fractionSeparator: '.',
                thousandSeparator: ',',
                language: 'en',
                site: 'ae',
                country: 'ae',
                sitesList: ["ae", "eg", "sa", "kw"],
                siteCountries: ["bh", "om", "qa", "ae"],
                cookieDomain: '.songviytuong.com',
                action: 'index',
                controller: 'home',
                bannerItemTypeName: 'homepage',
                targetedGroup: '',
                context: 'browser',
                debug: false,
                LoggerService: '',
                env: 'live',
                locale: 'en-US',
                modules: ['home'],
                device: 'pc',
                actualDevice: 'pc',
                deepLink: '',
                androidPackage: 'com.souq.app',
                windowsMarket: 'https://www.microsoft.com/en-us/store/apps/souq/9wzdncrdm3ts',
                androidMarket: '',
                iOSMarket: 'https://itunes.apple.com/jo/app/souq.com-swq/id675000850?mt=8',
                subDomains: '{"ae":"uae","sa":"saudi","eg":"egypt","kw":"kuwait"}',
                jsLogging: 0,
                subDomains: {"ae": "uae", "sa": "saudi", "eg": "egypt", "kw": "kuwait"},
                itemTypes: "",
                ditemType: "",
                phalconVersion: "2.0.8",
                search_string: "",
                user_ip: "113.161.37.58",
                custom_filter: null,
                itemSettings: null,
                adnHost: "x.sokrati.com/",
                adnEnabled: "1",
                pickupAreaUsed: 0,
                pickupEnableMap: 0,
                selectedPickupAddress: 0,
                FBAPPID: 501126536647943,
                appBoyKey: "2e4ae497-9aed-4a69-8a2d-91cd396ab384",
                gtm_id: "GTM-N2RCPF",
                AppBoySafariPushId: "web.com.souq.mweb",
                bInAppMessage: "1",
                layoutSettings: {
                    ApplyZoomInGallry: ""
                }
            };

            globals.DFP = {"country": "ae", "language": "en"};
            globals.APPBOY = null;
            globals.AppBoyCustomEvent = null;
            globals.bins = null;
            globals.itemBins = null;
            globals.promoPayments = null;
        </script>
        {/literal}
        <script type="text/javascript" src="templates/js/loader.93738d748552c5b631465c4173dabdf9.js"></script>
    </head>
    <body id="Ad-body-holder">
        <div id="fb-root"></div>
        <!-- GLOBAL WRAPPER -->
        <div class="off-canvas-wrap off-canvas-wrapper">
            <!-- INNER WRAP -->
            <div id="innerWrap" class="inner-wrap off-canvas-wrapper-inner fixed-header device-pc home-page" data-off-canvas-wrapper>
                <div class="header fixed-wrap grap wide-screen headerContainer">

                    <div class="hide-for-small-only clearfix">
                        <div class="souq-top-bar">
                            <div class="row">
                                <ul class="teasers-list
                                    small-5 small-push-1 columns show-for-xlarge ">
                                    <li id="freeShippingTeaser" class="free-shipping-teaser">
                                        <i class="fi-cargo-truck"></i>&nbsp;
                                        <a href="javascript:void(0);" title="All orders of 150.00 AED or more on fulfilled by Souq items across any product category qualify for FREE Shipping." data-tooltip="" aria-describedby="free-shipping-tooltip" data-yeti-box="free-shipping-tooltip" data-toggle="free-shipping-tooltip" data-resize="free-shipping-tooltip" data-options=""><strong>Free Shipping</strong></a> </li>
                                    <li id="returnsTeaser">
                                        <i class="fi-loop"></i>&nbsp;
                                        <a href="javascript:void(0);" title="Changed your mind, you can return your product and get a full refund." data-tooltip="" data-options=""><small>Free Returns</small></a> </li>
                                    <li id="CODTeaser" style="position: relative;">
                                        <i class="fi-dollar"></i>&nbsp;
                                        <a href="javascript:void(0);" title="Pay for your order in cash at the moment the shipment is delivered to your doorstep." data-tooltip="" data-options=""><small>Cash on Delivery</small></a> </li>
                                    <li class="show-for-xlarge"><i class="fi-telephone"></i>&nbsp;
                                        <span dir="ltr">
                                            <small>(+971)-4-437 0900</small>
                                        </span>
                                    </li>
                                </ul>

                                <ul class="h-list account-list small-12 medium-12 large-8 xlarge-6 columns">
                                    <li id="isPrime" class="hide">

                                    </li>
                                    <li class="dodLink show-for-non-minimal">
                                        <a href="https://deals.souq.com/ae-en/">Daily Deals</a> </li>

                                    <li class="show-for-non-minimal">
                                        <a href="https://sell.souq.com">Sell with Us</a> </li>

                                    <li class="myAccountLink show-for-non-minimal">
                                        <a href="/ae-en/account.php">Track Orders</a> </li>

                                    <li id="Ship_To" class="region with-flyout" data-flyout-content><a href="javascript: void(0);" data-flyout="cbt-popUp" title="Ship to United Arab Emirates"><i class="header-country-flag flag_ae"></i></a>
                                        <div class="flyout cbt-popUp">
                                            <ul id="cbt-popUp" class="option2">
                                                <li>
                                                    <label><b>Ship to </b>
                                                    </label>
                                                    <select id="selectCountries" name="selectCountries">
                                                        <option value="ae" selected="selected">UAE</option>
                                                        <option value="eg">Egypt</option>
                                                        <option value="sa">Saudi</option>
                                                        <option value="kw">Kuwait</option>
                                                        <option disabled="disabled">-- International Shipping Countries --</option>
                                                        <option value="bh">Bahrain</option>
                                                        <option value="om">Oman</option>
                                                        <option value="qa">Qatar</option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <label><b>Language</b>
                                                    </label>
                                                    <input type="radio" id="languge-typeen" name="languge-type" value="en" checked="checked" />
                                                    <label for="languge-typeen">English</label>
                                                    <input type="radio" id="languge-typear" name="languge-type" value="ar" />
                                                    <label for="languge-typear">عربي</label>
                                                    <a href="javascript:void(0);" id="change-contries" class="button tiny expand">Save & Continue</a> </li>
                                                <li class="local">Souq.com Local Sites</li>
                                                <li class="countries">
                                                    <a href="/">UAE</a> <a href="https://egypt.souq.com/">Egypt</a> <a href="https://saudi.souq.com/">Saudi</a> <a href="https://kuwait.souq.com/">Kuwait</a> </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="region">
                                        <a href="/ae-ar/" class="lang-link">العربية</a> </li>
                                    <li id="login_user_name_topbar" class=" ellipsis-loading with-flyout user_login login-user-name text-center remove-pointer-events has-tip">
                                        <span class="White" id="userNameLoading">...</span>
                                        <a id="userNameField_topbar" href="javascript:void(0);" class="truncate userNameField"></a>
                                        <!--  -->
                                        <div class="flyout user-menu hidden">
                                            <ul id="userName_topbar" class="filter-group">
                                                <li class="login-li row loginRemoval">
                                                    <div class="small-12 columns">
                                                        <a href="/ae-en/login.php" class="login-button" local="">Log In</a> </div>
                                                    <div class="to-sign-up small-12 columns">
                                                        <span class="register-now">Don't have an account?			<a href="/ae-en/register.php" class="login_to_register">Sign Up</a>		</span>
                                                    </div>

                                                </li>

                                                <li><a href="/ae-en/account.php" class="change-if-seller">My Orders</a> </li>
                                                <li><a href="/ae-en/account_addresses.php">My Addresses</a> </li>
                                                <li><a href="/ae-en/account_lists.php">Wish Lists</a> </li>
                                                <li><a href="/ae-en/account_settings.php">Account Settings</a> </li>
                                                <li class="toRemove"><a href="/ae-en/account_overview.php">Account Summary</a> </li>
                                                <li> </li>

                                                <li class="toRemove"><a href="/ae-en/logout.php">Logout</a> </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="header-middle-row">
                        <div class="row">
                            <div class="small-5 medium-5 large-3 columns">
                                <ul class="h-list logoList text-default">
                                    <li class="burger-menu">
                                        <a data-toggle="offCanvasLeft" href="javascript:void(0);" class="left-off-canvas-toggle">
                                            <i class="fi-menu"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/ae-en/"><img src="https://cf1.s3.souqcdn.com/public/style/img/en/souq-logo-v2.png" class="logo" alt="Souq.com Logo" title="Souq.com" data-interchange="[https://cf1.s3.souqcdn.com/public/style/img/en/souq-logo-v2-X2.png, retina]" /></a>                <span class="header-title-text hide"></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="mobile-menu show-for-small-only small-7 columns">
                                <div id="mini-cart_mobile" class="mini-cart mini-cart_mobile_aj with-flyout" data-xleft-items="0" data-flyout-content>
                                    <a href="/ae-en/shopping_cart.php" class="disabled" data-options="align:left" data-flyout="cartDrop_mobile"><i class='fi-cart'></i> <span class='cart-item-count-aj show-mini-cart-aj cart-icon-item-count hide'></span></a>  
                                    <div id="cartDrop_mobile" class="cart-dropdown flyout  cartDropItem" data-dropdown-content></div>
                                </div>
                                <ul class="clearfix">
                                    <li class="mobile-search-button ">
                                        <i class="fi-magnifying-glass"></i>
                                    </li>
                                    <li id="login_user_name_mobile" class="login-user-name ellipsis-loading with-flyout user_login" data-flyout-content>
                                        <a id="userNameField_mobile" href="javascript:void(0);" class="userNameField truncate" data-options="align:bottom" data-flyout="userName_mobile"></a>
                                        <div class="flyout user-menu">
                                            <ul id="userName_mobile" class="filter-group">
                                                <li  class="login-li row loginRemoval">
                                                    <div class="small-12 columns">
                                                        <a href="/ae-en/login.php" class="login-button" local="">Log In</a>	</div>
                                                    <div class="to-sign-up small-12 columns">
                                                        <span class="register-now">Don't have an account?			<a href="/ae-en/register.php" class="login_to_register">Sign Up</a>		</span>
                                                    </div>

                                                </li>

                                                <li><a href="/ae-en/account.php" class="change-if-seller">My Orders</a>	</li>
                                                <li><a href="/ae-en/account_addresses.php">My Addresses</a>	</li>
                                                <li><a href="/ae-en/account_lists.php">Wish Lists</a>	</li>
                                                <li><a href="/ae-en/account_settings.php">Account Settings</a>	</li>
                                                <li class="toRemove"><a href="/ae-en/account_overview.php">Account Summary</a>	</li>
                                                <li>	</li>

                                                <li class="toRemove"><a href="/ae-en/logout.php">Logout</a>	</li>	
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="small-12 medium-7 large-9 columns extra search-wrapper clearPadding-h-small " id="search-mobile">
                                <div id="mini-cart_topbar" class="mini-cart mini-cart_topbar_aj with-flyout" data-xleft-items="0" data-flyout-content>
                                    <a href="/ae-en/shopping_cart.php" class="disabled" data-options="align:left" data-flyout="cartDrop_mobile"><i class='fi-cart'></i> <span class='cart-item-count-aj show-mini-cart-aj cart-icon-item-count hide'></span></a>  
                                    <div id="cartDrop_topbar" class="cartDropItem cart-dropdown flyout " data-dropdown-content></div>
                                </div>

                                <div class="search clearfix">
                                    <div class="wrap  small-12 medium-9 large-7 columns">
                                        <div>
                                            <form action="/ae-en/search_results.php" id="searchForm" method="get"><input type="hidden" id="hitsCfs" name="hitsCfs" value="bc1a0621c5b7a22b176a94830c93c3a2" /><input type="hidden" id="hitsCfsMeta" name="hitsCfsMeta" value="vOD9d6iA6iuwDFl+xBbszg9dx3EcY2v+OqOM8hOoNgjj6VoZN0OMKDedm8oAX5hDhtNQuKmUXMFG1K0wyLATka1DjpwP+foc1Ukw+MFWhiPwl3DjbI6cTlXT0UMHSVkbbJYphFqLC/qgQyPsrEFHcod9TZAAodAeP1Ys3OlObgQ=" />            <div id="search_box" class="row collapse relative">
                                                    <div class="columns small-10  large-11 relative">                    <input type="text" id="search_value" name="q" placeholder="What are you looking for?" autocomplete="off" tabindex="-1">                </div>


                                                    <div class="columns small-2 large-1 relative">
                                                        <div class="button radius postfix relative">
                                                            <input type="submit" id="searchButton" value="" class="formButton expand block" />                                                            <i class="fi-magnifying-glass"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="souq-nav">
                        <div class="level0-container">
                            <div class="row">
                                <ul id="megaMenuNav" class="small-12 menu-list dropdown menu" data-dropdown-menu="menu-0" data-hover-delay="300" data-closing-time="0">
                                    <li class="level0 menu-link no-child">
                                        <a href="/ae-en/shop-all-categories/c/" class="menu-tr-enabled" data-menu-id="menu-0" data-tr-menu-name="All Categories" data-tr-menu-pos="1" data-tr-box-type="Flyout" data-tr-menu-date="05/28/2017">Danh mục</a>                            
                                    </li>

                                    <li class="level0 menu-link fashion-menu-link is-dropdown-submenu-parent">
                                        <a href=""><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 130 15" style="max-width: 120px;">
                                            <g>
                                            <g stroke="null" id="svg_1">
                                            <g stroke="null" id="svg_2">
                                            <path stroke="null" id="svg_3" d="m8.84463,6.45076l-3.59947,-0.79988c-0.72717,-0.14543 -0.94532,-0.36358 -0.94532,-0.72717c0,-0.29087 0.21815,-0.50902 0.8726,-0.50902l6.58085,0l0,-1.92699c0,-0.54537 -0.4363,-0.98167 -0.98167,-0.98167l-5.74461,0c-2.79959,0 -4.25392,1.59976 -4.25392,3.67219c0,1.81791 1.12711,2.94502 3.38132,3.45404l3.59947,0.79988c0.72717,0.18179 0.94532,0.36358 0.94532,0.72717c0,0.29087 -0.21815,0.50902 -0.8726,0.50902l-6.83536,0l0,1.92699c0,0.54537 0.4363,0.98167 0.98167,0.98167l5.59918,0c3.3086,0 4.6175,-1.59976 4.6175,-3.52675c0.03636,-1.99971 -1.12711,-3.12681 -3.34496,-3.59947"/>
                                            <path stroke="null" id="svg_4" d="m19.97027,10.70467c-1.7452,0 -3.19953,-1.41797 -3.19953,-3.19953c0,-1.7452 1.41797,-3.19953 3.19953,-3.19953c1.7452,0 3.19953,1.41797 3.19953,3.19953c0,1.78156 -1.45433,3.19953 -3.19953,3.19953m0,-9.48951c-3.45404,0 -6.28998,2.83595 -6.28998,6.28998c0,3.45404 2.83595,6.28998 6.28998,6.28998c3.45404,0 6.28998,-2.83595 6.28998,-6.28998c0,-3.45404 -2.83595,-6.28998 -6.28998,-6.28998"/>
                                            <path stroke="null" id="svg_5" d="m38.87658,8.26867l0,-6.87172c0,0 0,0 0,0l-2.07242,0c-0.54537,0 -1.01803,0.4363 -1.01803,0.98167l0,5.89004c0,1.3089 -1.12711,2.36329 -2.47236,2.29057c-1.23618,-0.07272 -2.1815,-1.16347 -2.1815,-2.36329l0,-5.81733c0,-0.54537 -0.4363,-0.98167 -1.01803,-0.98167l-2.07242,0c0,0 0,0 0,0l0,6.76264c0,2.90866 2.29057,5.38103 5.23559,5.45374c3.0541,0.10907 5.59918,-2.32693 5.59918,-5.34467"/>
                                            <path stroke="null" id="svg_6" d="m46.91176,10.66832c-1.7452,0 -3.19953,-1.41797 -3.19953,-3.19953c0,-1.7452 1.41797,-3.19953 3.19953,-3.19953c1.7452,0 3.19953,1.41797 3.19953,3.19953c0,0.29087 -0.03636,0.58173 -0.10907,0.83624l-1.19982,-1.19982l-2.25421,2.1815l1.23618,1.23618c-0.29087,0.10907 -0.58173,0.14543 -0.8726,0.14543m5.41738,0c0.54537,-0.94532 0.8726,-2.03606 0.8726,-3.19953c0,-3.45404 -2.83595,-6.28998 -6.28998,-6.28998c-3.45404,0 -6.28998,2.83595 -6.28998,6.28998c0,3.45404 2.83595,6.28998 6.28998,6.28998c1.16347,0 2.29057,-0.32722 3.19953,-0.8726l0.61809,0.61809c0.39994,0.39994 1.05439,0.39994 1.45433,0l1.49069,-1.49069c0,0 0,0 0,0l-1.34526,-1.34526z"/>
                                            </g>
                                            <g stroke="null" id="svg_7">
                                            <path stroke="null" id="svg_8" d="m57.74652,1.90597l8.03518,0l0,1.16347l-6.76264,0l0,3.99941l6.03548,0l0,1.16347l-6.03548,0l0,4.83565l-1.27254,0l0,-11.16199z"/>
                                            <path stroke="null" id="svg_9" d="m71.05366,1.83325l1.16347,0l5.09016,11.23471l-1.34526,0l-1.3089,-2.94502l-6.07183,0l-1.3089,2.94502l-1.3089,0l5.09016,-11.23471zm3.12681,7.16258l-2.54508,-5.67189l-2.54508,5.67189l5.09016,0z"/>
                                            <path stroke="null" id="svg_10" d="m78.57982,11.4682l0.76352,-0.90896c1.16347,1.05439 2.29057,1.56341 3.81762,1.56341c1.49069,0 2.47236,-0.79988 2.47236,-1.89063l0,-0.03636c0,-1.05439 -0.54537,-1.63612 -2.90866,-2.10878c-2.58144,-0.54537 -3.7449,-1.38161 -3.7449,-3.23589l0,-0.03636c0,-1.7452 1.56341,-3.0541 3.67219,-3.0541c1.63612,0 2.79959,0.47266 3.92669,1.38161l-0.72717,0.98167c-1.01803,-0.8726 -2.03606,-1.23618 -3.19953,-1.23618c-1.45433,0 -2.36329,0.79988 -2.36329,1.81791l0,0.03636c0,1.05439 0.58173,1.63612 3.01774,2.1815c2.47236,0.54537 3.63583,1.45433 3.63583,3.16317l0,0.03636c0,1.92699 -1.59976,3.16317 -3.81762,3.16317c-1.7452,-0.03636 -3.19953,-0.65445 -4.54479,-1.81791z"/>
                                            <path stroke="null" id="svg_11" d="m89.52366,1.90597l1.27254,0l0,4.98108l6.39906,0l0,-4.98108l1.27254,0l0,11.16199l-1.27254,0l0,-5.01744l-6.43542,0l0,5.01744l-1.27254,0l0,-11.16199l0.03636,0z"/>
                                            <path stroke="null" id="svg_12" d="m101.74005,1.90597l1.27254,0l0,11.16199l-1.27254,0l0,-11.16199z"/>
                                            <path stroke="null" id="svg_13" d="m105.77582,7.5415l0,0c0,-3.12681 2.32693,-5.81733 5.70825,-5.81733c3.38132,0 5.67189,2.65415 5.67189,5.74461l0,0.03636c0,3.09045 -2.32693,5.78097 -5.70825,5.78097c-3.38132,0 -5.67189,-2.69051 -5.67189,-5.74461zm10.07124,0l0,0c0,-2.58144 -1.85427,-4.65386 -4.39935,-4.65386s-4.36299,2.03606 -4.36299,4.58114l0,0.03636c0,2.54508 1.85427,4.6175 4.39935,4.6175s4.36299,-2.07242 4.36299,-4.58114z"/>
                                            <path stroke="null" id="svg_14" d="m119.81011,1.90597l1.16347,0l7.01715,8.94414l0,-8.94414l1.23618,0l0,11.16199l-1.01803,0l-7.19894,-9.16229l0,9.16229l-1.23618,0l0,-11.16199l0.03636,0z"/>
                                            </g>
                                            </g>
                                            </g>
                                            </svg>
                                        </a>
                                        <ul class="fashion-menu-content type-menu-content menu menu-content small-12 columns">
                                            <div class="level1-container">
                                                <div class="row">
                                                    <ul id="megaMenuNav" class="small-12 menu-list dropdown menu" data-dropdown-menu="menu-1" data-hover-delay="300" data-closing-time="0">
                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="" data-menu-id="menu-0">New Arrivals</a>
                                                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-5 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <h4>Women</h4>
                                                                                <ul>
                                                                                    <li><a href="">Clothing</a></li>
                                                                                    <li><a href="">Footwear</a></li>
                                                                                    <li><a href="">Bags & Wallets</a></li>
                                                                                    <li><a href="">Watches</a></li>
                                                                                    <li><a href="">Sunglasses & Frames</a></li>
                                                                                    <li><a href="">Jewelry</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <h4>Women</h4>
                                                                                <ul>
                                                                                    <li><a href="">Clothing</a></li>
                                                                                    <li><a href="">Footwear</a></li>
                                                                                    <li><a href="">Bags & Wallets</a></li>
                                                                                    <li><a href="">Watches</a></li>
                                                                                    <li><a href="">Sunglasses & Frames</a></li>
                                                                                    <li><a href="">Jewelry</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <h4>Women</h4>
                                                                                <ul>
                                                                                    <li><a href="">Clothing</a></li>
                                                                                    <li><a href="">Footwear</a></li>
                                                                                    <li><a href="">Bags & Wallets</a></li>
                                                                                    <li><a href="">Watches</a></li>
                                                                                    <li><a href="">Sunglasses & Frames</a></li>
                                                                                    <li><a href="">Jewelry</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="medium-7 text-opposite columns">
                                                                    <div class="row" style=" margin-bottom:10px;">
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <a href=""><img src="templates/public/style/img/blank.gif" data-src="templates/images/Fly-Out-newarrivals-womenfashion-en.jpg" alt=""/></a>
                                                                            </div>    
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <a href=""><img src="templates/public/style/img/blank.gif" data-src="templates/images/Fly-Out-newarrivals-womenfashion-en.jpg" alt=""/></a>
                                                                            </div>    
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <a href=""><img src="templates/public/style/img/blank.gif" data-src="templates/images/Fly-Out-newarrivals-womenfashion-en.jpg" alt=""/></a>
                                                                            </div>    
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                            </ul>

                                                        </li>
                                                        
                                                        <li class="level1 menu-link no-child">
                                                            <a href="" data-menu-id="menu-3">Kids</a>                            
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                    <li class="level0 menu-link fmcg-menu-link is-dropdown-submenu-parent">
                                        <a href="https://supermarket.souq.com/ae-en/">    Supermarket</a>
                                        <ul class="fmcg-menu-content type-menu-content menu menu-content small-12 columns">
                                            <div class="level1-container">
                                                <div class="row">
                                                    <ul id="megaMenuNav" class="small-12 menu-list dropdown menu" data-dropdown-menu="menu-1" data-hover-delay="300" data-closing-time="0">
                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/beverages/cc/268" class="menu-tr-enabled" data-menu-id="menu-0" data-tr-menu-name="Beverages" data-tr-menu-pos="1" data-tr-box-type="Flyout" data-tr-menu-date="07/12/17">Beverages</a>
                                                            
                                                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-2 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/water/c/3708">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/beverages/water_beverages_190717_en.jpg" alt="Water">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-2 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/soft-drinks/c/3672">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/beverages/soda_beverages_190717_en.jpg" alt="Sodas">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-2 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/juices/c/3709">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/beverages/Juices_beverages_190717_en.jpg" alt="Juices">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-2 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/energy-and-sports-drinks/c/3711">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/beverages/Energy-Sports-Drinks_beverages_190717_en.jpg" alt="Energy &amp; Sports Drinks">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-2 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/powder-drinks/c/3713">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/beverages/Powder-Drinks_beverages_190717_en.jpg" alt="Powder Drinks">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-2 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/malt-beverages/c/3728">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/beverages/Malt-Beverages_beverages_190717_en.jpg" alt="Malt Beverages">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>      
                                                                    </div>
                                                                </div>                
                                                            </ul>

                                                        </li>

                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/food-cupboard/cc/281" class="menu-tr-enabled" data-menu-id="menu-1" data-tr-menu-name="Food Cupboard" data-tr-menu-pos="2" data-tr-box-type="MarketingBox" data-tr-menu-date="7/12/2017">Food Cupboard</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li>
                                                                                        <a href="https://supermarket.souq.com/ae-en/confectionery/c/3715">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_confectionery_en.jpg" alt="Confectionery"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/rice-and-pulses/c/3592">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_rice_en.jpg" alt="Rice & Pulses"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li>
                                                                                        <a href="https://supermarket.souq.com/ae-en/breakfast/c/3625">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_breakfast_en.jpg" alt="Breakfast"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/pasta-and-noodles/c/3752">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_pastas_en.jpg" alt="Pastas and Noodles"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li>
                                                                                        <a href="https://supermarket.souq.com/ae-en/cooking-products/c/3591">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_cooking_en.jpg" alt="Cooking Products"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/spices-and-preservatives/c/3671">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_condiments_en.jpg" alt="Spices & Condiments"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li>
                                                                                        <a href="https://supermarket.souq.com/ae-en/snacking/c/3590">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_snacking_en.jpg" alt="Snacking"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/can-food/c/3714">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/FoodCupboard/31/FoodCupboard_canned_en.jpg" alt="Canned Food"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div>                </ul>

                                                        </li>

                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/dairy-products/cc/286" class="menu-tr-enabled" data-menu-id="menu-2" data-tr-menu-name="Dairy Products" data-tr-menu-pos="3" data-tr-box-type="Flyout" data-tr-menu-date="7/12/2017">Dairy Products</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/uht-milk/c/3716">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/dairy-products/30/DairyProducts_uhtMilk_en.jpg" alt="UHT Milk">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/powder-and-evaporated-milk/c/3744">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/dairy-products/30/DairyProducts_powder_en.jpg" alt="Powder & Evaporated Milk">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 4px 2px 4px;"><a href="https://supermarket.souq.com/ae-en/cheese/c/3718">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/dairy-products/30/DairyProducts_cheese_en.jpg" alt="Cheese">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>     
                                                                    </div>
                                                                </div>                </ul>

                                                        </li>

                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/beauty/cc/285" class="menu-tr-enabled" data-menu-id="menu-3" data-tr-menu-name="Beauty" data-tr-menu-pos="4" data-tr-box-type="Flyout" data-tr-menu-date="7/12/2017">Beauty</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-4 columns">

                                                                            <a href="https://supermarket.souq.com/ae-en/personal-care/c/3719">
                                                                                <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Beauty/30/Beaut_pc_en.jpg" alt="Personal Care"></a>
                                                                            <br><br>
                                                                            <a href="https://supermarket.souq.com/ae-en/skin-care/c/3594">
                                                                                <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Beauty/30/Beaut_sc_en.jpg" alt="Skin Care"></a>

                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <a href="https://supermarket.souq.com/ae-en/hair-care/c/3593">
                                                                                <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Beauty/30/Beaut_hc_en.jpg" alt="Hair Care"></a>
                                                                            <br><br>
                                                                            <a href="https://supermarket.souq.com/ae-en/men-s-grooming/c/3595">
                                                                                <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Beauty/30/Beaut_mg_en.jpg" alt="Men's Grooming"></a>

                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <a href="https://supermarket.souq.com/ae-en/make-up/c/3720">
                                                                                <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Beauty/30/Beaut_makeup_en.jpg" alt="Make Up"></a>

                                                                        </div>
                                                                    </div>  
                                                                </div>                </ul>

                                                        </li>

                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/home-supplies/cc/273" class="menu-tr-enabled" data-menu-id="menu-4" data-tr-menu-name="Homecare" data-tr-menu-pos="5" data-tr-box-type="Flyout" data-tr-menu-date="7/12/2017">Homecare</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 5px 2px 5px;"><a href="https://supermarket.souq.com/ae-en/laundry/c/3721">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Homecare/30/Homecare_laundry_en.jpg" alt="Laundry">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 5px 2px 5px;"><a href="https://supermarket.souq.com/ae-en/cleaning-supplies/c/3722">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Homecare/30/Homecare_supplies_en.jpg" alt="Cleaning Supplies">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 5px 2px 5px;"><a href="https://supermarket.souq.com/ae-en/plastic-and-paper-products/c/3723">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Homecare/30/Homecare_paper_en.jpg" alt="Paper & Plastic products">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:15px;">
                                                                                    <li style="margin:2px 5px 2px 5px;"><a href="https://supermarket.souq.com/ae-en/air-treatment/c/3598">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/Homecare/30/Homecare_fresheners_en.jpg" alt="Air Fresheners">
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div>                </ul>

                                                        </li>

                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/baby-world/cc/270" class="menu-tr-enabled" data-menu-id="menu-5" data-tr-menu-name="Baby World" data-tr-menu-pos="6" data-tr-box-type="MarketingBox" data-tr-menu-date="7/12/2017">Baby World</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/baby-food/c/3620">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/BabyWorld/jul/28/BabyWorld_BabyFood_en.jpg" alt="Baby Food"></a><br><br><br>
                                                                                    </li>
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/baby-wipes/c/3725">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/BabyWorld/jul/28/BabyWorld_Babywipes_en.jpg" alt="Baby Wipes"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/disposable-diapers/c/3724">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/BabyWorld/jul/28/BabyWorld_Diapers_en.jpg" alt="Disposable Diapers"></a><br><br><br>
                                                                                    </li>
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/baby-bath-care/c/3726">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/BabyWorld/jul/28/BabyWorld_BabyBath_en.jpg" alt="Baby Bath Care"></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-4 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/baby-skin-care/c/3727">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/BabyWorld/jul/28/BabyWorld_BabySkin_en.jpg" alt="Baby Skin Care"></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>  
                                                                    </div>
                                                                </div>                </ul>

                                                        </li>


                                                        <li class="level1 menu-link is-dropdown-submenu-parent">
                                                            <a href="https://supermarket.souq.com/ae-en/healthy-food/cc/272" class="menu-tr-enabled" data-menu-id="menu-7" data-tr-menu-name="Healthy Food" data-tr-menu-pos="8" data-tr-box-type="Flyout" data-tr-menu-date="7/12/2017">Healthy Food</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                                                <div class="medium-12 columns container">
                                                                    <div class="row">
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/organic/c/3667">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_organic_en.jpg" alt="Organic"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/low-fat-products/c/3731">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_LowFat_en.jpg" alt="Low Fat Products"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/gluten-free/c/3729">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_gultenfree_en.jpg" alt="Gluten Free"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/vitamins-and-minerals/c/3732">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_vitamins_en.jpg" alt="Vitamins & Minerals"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/sugar-free/c/3730">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_sugarfree_en.jpg" alt="Sugar Free"></a>
                                                                                        <br><br>
                                                                                        <a href="https://supermarket.souq.com/ae-en/sport-nutrition/c/3733">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_nutrition_en.jpg" alt="Sport Nutrition"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="medium-3 columns">
                                                                            <div class="row">
                                                                                <ul style=" margin-bottom:5px;">
                                                                                    <li style="margin:4px 5px 4px 5px;"><a href="https://supermarket.souq.com/ae-en/food-supplements/c/3734">
                                                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/fmcg/HealthyFood/31/HealthyFood_supplements_en.jpg" alt="Food Supplements"></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div>                </ul>

                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>	</ul>
                                    </li>
                                    <li class="level0 menu-link is-dropdown-submenu-parent">
                                        <a href="/ae-en/ccc-electronics-cms-ae/c/" class="menu-tr-enabled" data-menu-id="menu-1" data-tr-menu-name="Electronics" data-tr-menu-pos="2" data-tr-box-type="Flyout" data-tr-menu-date="05/28/2017">Electronics</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                            <div class="medium-9 columns container">
                                                <div class="row">
                                                    <div class="medium-3 columns">
                                                        <div class="row">
                                                            <h4><a href="" style="text-decoration:none; color:#000000;">Laptops</a></h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li>
                                                                    <a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Laptops-MacBooks-Listing', '/ae-en/laptop-notebook/apple/new/a-7-c/l/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Laptops-MacBooks-Listing';s.eVar20 = 'HP|Flyout|20170518|Electronics-Laptops-MacBooks-Listing';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/laptop-notebook/apple/new/a-7-c/l/?sortby=sr">MacBooks</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Laptops-2in1-listings', 'http://uae.souq.com/ae-en/laptop-notebook/2-in-1/a-5700/l/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Laptops-2in1-listings';s.eVar20 = 'HP|Flyout|20170518|Electronics-Laptops-2in1-listings';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="http://uae.souq.com/ae-en/laptop-notebook/2-in-1/a-5700/l/">2 in 1 Laptops</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Laptops-Gaming-search', 'http://uae.souq.com/ae-en/gaming/laptops---and---notebooks-75/a-t/s/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Laptops-Gaming-search';s.eVar20 = 'HP|Flyout|20170518|Electronics-Laptops-Gaming-search';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/laptop-notebook/msi%7Calienware%7Cmonster/a-7/l/">Gaming Laptops</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Laptops-all-in-one-listing', 'http://uae.souq.com/ae-en/computer/l/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Laptops-all-in-one-listing';s.eVar20 = 'HP|Flyout|20170518|Electronics-Laptops-all-in-one-listing';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/computer/l/">All In One/Desktops</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Laptops-buying-guide-cms', 'http://uae.souq.com/ae-en/ccc-laptops-buying-guide-ae/c/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Laptops-buying-guide-cms';s.eVar20 = 'HP|Flyout|20170518|Electronics-Laptops-buying-guide-cms';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/ccc-laptops-buying-guide-ae/c/">Laptop Buying Guide</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Laptops-accessories-search', '/ae-en/laptops-or-notebooks-or-pcs-or-accessory-or-hard-disk-or-router-or-game-or-console-or-printer-or-scanner/computer---and---laptop-accessories-187%7Chard-drives-55%7Crouter-88%7Cprinters-192%7Cscanners-89%7Cusb-flash-drives-369%7Cmouse-47%7Ckeyboards-61/new/a-t-c/s/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Laptops-accessories-search';s.eVar20 = 'HP|Flyout|20170518|Electronics-Laptops-accessories-search';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/laptops-or-notebooks-or-pcs-or-accessory-or-hard-disk-or-router-or-game-or-console-or-printer-or-scanner/computer---and---laptop-accessories-187%7Chard-drives-55%7Crouter-88%7Cprinters-192%7Cscanners-89%7Cusb-flash-drives-369%7Cmouse-47%7Ckeyboards-61/new/a-t-c/s/">Computer Accessories</a></li>
                                                            </ul>
                                                            <h4><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-CMS', 'http://uae.souq.com/ae-en/ccc-large-appliances-ae/c/');s.linkTrackVars = 'eVar3,eVar20';s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-CMS';s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-CMS';s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                   href="/ae-en/ccc-large-appliances-ae/c/" style="text-decoration:none; color:#000000;">Large Appliances</a></h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-refrigerators-listings', 'http://uae.souq.com/ae-en/refrigerators-freezers/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-refrigerators-listings'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-refrigerators-listings'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/refrigerators-freezers/l/">Refrigerators</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-washing-machines-search', 'http://uae.souq.com/ae-en/washing-machine/washing-machines-519%7Cwashers-dryers-2-in-1-552/new/a-t-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-washing-machines-search'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-washing-machines-search'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/washing-machine/washing-machines-519%7Cwashers-dryers-2-in-1-552/new/a-t-c/s/">Washing Machines</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-ovens-listing', 'http://uae.souq.com/ae-en/ovens-ranges/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-ovens-listing'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-ovens-listing'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/ovens-ranges/l/">Ovens & Ranges</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-AC-listing', 'http://uae.souq.com/ae-en/ac-large-appliance-ae/c/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-AC-listing'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-AC-listing'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/ac-large-appliance-ae/c/">Air Conditioners</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-AC-microwave-listing', 'http://uae.souq.com/ae-en/microwave/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-AC-microwave-listing'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-AC-microwave-listing'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/microwave/l/">Microwaves</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Large-appliances-watercoolers-listing', '/ae-en/water-coolers/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Large-appliances-watercoolers-listing'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Large-appliances-watercoolers-listing'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/water-coolers/l/">Water Coolers & Dispensers</a></li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="medium-3 columns">
                                                        <div class="row">
                                                            <h4><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TV-audio-video-CMS', 'http://uae.souq.com/ae-en/ae-tv-home-theater/c/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TV-audio-video-CMS'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TV-audio-video-CMS'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                   href="/ae-en/ae-tv-home-theater/c/" style="text-decoration:none; color:#000000;">Tv, Audio & Video</a></h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TV-audio-video-TV-listing', 'http://uae.souq.com/ae-en/lcd-led-dlp-tv/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TV-audio-video-TV-listing'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TV-audio-video-TV-listing'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/lcd-led-dlp-tv/l/">TVs</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TV-audio-video-speakers-search', 'http://uae.souq.com/ae-en/theater-or-speaker-or-az-bz-cz/home-theater-systems-12%7Cspeakers-22/a-t/s/?_=1482140614258&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TV-audio-video-speakers-search'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TV-audio-video-speakers-search'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/theater-or-speaker-or-az-bz-cz/home-theater-systems-12%7Cspeakers-22/a-t/s/?_=1482140614258&page=1">Speakers & Home Theaters</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TVAudioVideo-Headphones', 'http://uae.souq.com/ae-en/headphones-ae/c/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-Headphones'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-Headphones'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/headphones-ae/c/">Headphones</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TVAudioVideo-TVAccessories', 'http://uae.souq.com/ae-en/tv-cable-or-tv-accessories-or-satellite-accessories-or-media-gateways/tv-and-satellite-accessories-28%7Csatellite-receivers-249%7Ctuners-411%7Cmedia-gateways-407%7Ctv-mounts-374/new/a-t-c/s/?sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-TVAccessories'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-TVAccessories'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/tv-cable-or-tv-accessories-or-satellite-accessories-or-media-gateways/tv-and-satellite-accessories-28%7Csatellite-receivers-249%7Ctuners-411%7Cmedia-gateways-407%7Ctv-mounts-374/new/a-t-c/s/?sortby=sr">TV Accessories</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TVAudioVideo-MP3Players', 'http://uae.souq.com/ae-en/mp3-player/new/a-c/l/?sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-MP3Players'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-MP3Players'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/mp3-player/new/a-c/l/?sortby=sr">MP3 and MP4 Players</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TVAudioVideo-Projectors', '/ae-en/projector/new/a-c/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-Projectors'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TVAudioVideo-Projectors'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/projector/new/a-c/l/">Projectors</a></li>
                                                            </ul>
                                                            <h4><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances', 'http://uae.souq.com/ae-en/ae_small_appliances/c/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                   href="/ae-en/ae_small_appliances/c/" style="text-decoration:none; color:#000000;">Small Appliances</a></h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances-Blenders', 'http://uae.souq.com/ae-en/blenders_mixers/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances-Blenders'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances-Blenders'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/blenders_mixers/l/">Blenders & Mixers</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances-JuiceExtractor', 'http://uae.souq.com/ae-en/juice-extractor/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances-JuiceExtractor'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances-JuiceExtractor'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/juice-extractor/l/">Juice Extractors</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances-FoodPreparation', 'http://uae.souq.com/ae-en/food-processor/food-preparation-392%7Cfood-processors-506%7Cspecialty-kitchen-appliances-57/a-t/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances-FoodPreparation'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances-FoodPreparation'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/food-processor/food-preparation-392%7Cfood-processors-506%7Cspecialty-kitchen-appliances-57/a-t/s/">Food Preparation</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances-VacuumCleaner', 'http://uae.souq.com/ae-en/vacuums/vacuum-cleaners-413%7Cvacuums-and-floor-care-253/new/a-t-c/s/?sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances-VacuumCleaner'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances-VacuumCleaner'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/vacuums/vacuum-cleaners-413%7Cvacuums-and-floor-care-253/new/a-t-c/s/?sortby=sr">Vacuum Cleaners</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances-CoffeeMachine', 'http://uae.souq.com/ae-en/hot-beverage-maker/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances-CoffeeMachine'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances-CoffeeMachine'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/hot-beverage-maker/l/">Coffee Machines</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-SmallAppliances-Fryers', 'http://uae.souq.com/ae-en/deep-fryer/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-SmallAppliances-Fryers'; s.eVar20 = 'HP|Flyout|20170518|Electronics-SmallAppliances-Fryers'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/deep-fryer/l/">Fryers</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="medium-3 columns">
                                                        <div class="row">
                                                            <h4><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras', 'http://uae.souq.com/ae-en/camera-electronics-ae/c/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                   href="/ae-en/camera-electronics-ae/c/" style="text-decoration:none; color:#000000;">Cameras</a></h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras-DSLRs', 'http://uae.souq.com/ae-en/digital-camera/slr-camera/new/a-5778-c/l/?rpp=50&sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras-DSLRs'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras-DSLRs'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/digital-camera/slr-camera/new/a-5778-c/l/?rpp=50&sortby=sr">DSLRs</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras-Point&Shoot', 'http://uae.souq.com/ae-en/digital-camera/compact-camera/a-5778/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras-Point&Shoot'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras-Point&Shoot'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/digital-camera/compact-camera/a-5778/l/">Point & Shoot</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras-ActionCameras', 'http://uae.souq.com/ae-en/action-camera/digital-cameras-14%7Ccamcorders-15/a-t/s/?_=1485424119423&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras-ActionCameras'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras-ActionCameras'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/action-camera/digital-cameras-14%7Ccamcorders-15/a-t/s/?_=1485424119423&page=1">Action Cameras</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras-Camcorders', 'http://uae.souq.com/ae-en/camcorder/new/a-c/l/?sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras-Camcorders'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras-Camcorders'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/camcorder/new/a-c/l/?sortby=sr">Camcorders</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras-Lenses', 'http://uae.souq.com/ae-en/lens/interchangeable-lenses-375/a-t/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras-Lenses'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras-Lenses'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/lens/interchangeable-lenses-375/a-t/s/">Lenses & Flashes</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Cameras-Accessories', 'http://uae.souq.com/ae-en/camera-camcorder-accessories/new/a-c/l/?sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Cameras-Accessories'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Cameras-Accessories'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/camera-camcorder-accessories/new/a-c/l/?sortby=sr">Accessories</a></li>
                                                            </ul>
                                                            <h4><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming', 'http://uae.souq.com/ae-en/electronics-gaming-ae/c/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                   href="/ae-en/electronics-gaming-ae/c/" style="text-decoration:none; color:#000000;">Gaming Consoles & Titles</a></h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming-SonyPlaystation', 'http://uae.souq.com/ae-en/playstation/video-games-4%7Cgame-consoles-6%7Cgames-gadgets---and---accessories-7%7Cskins---and---decals-517/a-t/s/?_=1491408577389§ion=2&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming-SonyPlaystation'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming-SonyPlaystation'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/playstation/video-games-4%7Cgame-consoles-6%7Cgames-gadgets---and---accessories-7%7Cskins---and---decals-517/a-t/s/">Sony Playstation</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming-MicrosoftXbox', 'http://uae.souq.com/ae-en/xbox/game-consoles-6%7Cvideo-games-4%7Cgames-gadgets---and---accessories-7/a-t/s/?_=1491408680358&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming-MicrosoftXbox'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming-MicrosoftXbox'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/xbox/game-consoles-6%7Cvideo-games-4%7Cgames-gadgets---and---accessories-7%7Cskins---and---decals-517/a-t/s/">Microsoft Xbox</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming-Nintendo', 'http://uae.souq.com/ae-en/nintendo/game-consoles-6%7Cvideo-games-4%7Cgames-gadgets---and---accessories-7/a-t/s/?_=1491408725272&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming-Nintendo'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming-Nintendo'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/nintendo/game-consoles-6%7Cvideo-games-4%7Cgames-gadgets---and---accessories-7/a-t/s/">Nintendo</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming-LatestGameTitles', 'http://uae.souq.com/ae-en/games/l/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming-LatestGameTitles'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming-LatestGameTitles'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/games/l/">Latest Game Titles</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming-Accessories', 'http://uae.souq.com/ae-en/games-console-accessories/200-999-aed/a-cp/l/?page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming-Accessories'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming-Accessories'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/games-console-accessories/200-999-aed/a-cp/l/">Accessories</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-Gaming-Monitors', 'http://uae.souq.com/ae-en/monitors/computer-monitors-188/a-t/s/?page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-Gaming-Monitors'; s.eVar20 = 'HP|Flyout|20170518|Electronics-Gaming-Monitors'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/computer-monitor/l/">Monitors</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="medium-3 columns">
                                                        <div class="row">
                                                            <h4>Top Brands</h4> 
                                                            <ul style=" margin-bottom:15px;">
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Samsung', 'http://uae.souq.com/ae-en/samsung/televisions-10%7Chome-theater-systems-12%7Cspeakers-22%7Csmart-watches-511/samsung/new/a-t-7-c/s/?rpp=50&_=1459779734773&sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Samsung'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Samsung'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/samsung/televisions-10%7Chome-theater-systems-12%7Cspeakers-22%7Csmart-watches-511/samsung/new/a-t-7-c/s/?rpp=50&_=1459779734773&sortby=sr">Samsung</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Apple-search', '/ae-en/apple/laptops---and---notebooks-75%7Cheadphones---and---headsets-373%7Cmp3---and---mp4-players-23%7Cchargers-351%7Ctv-and-satellite-accessories-28%7Chard-drives-55%7Cmouse-47%7Cpower-banks-562/apple/a-t-7/s/?_=1495110260832&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Apple-search'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Apple-search'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/apple/laptops---and---notebooks-75%7Cheadphones---and---headsets-373%7Cmp3---and---mp4-players-23%7Cchargers-351%7Ctv-and-satellite-accessories-28%7Chard-drives-55%7Cmouse-47%7Cpower-banks-562/apple/a-t-7/s/?_=1495110260832&page=1">Apple</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Sony', 'http://uae.souq.com/ae-en/sony/video-games-4%7Cheadphones---and---headsets-373%7Cgame-consoles-6%7Cdigital-cameras-14%7Cgames-gadgets---and---accessories-7%7Cprojectors-13/sony%7Csony-ericsson/new/a-t-7-c/s/?_=1485430912358&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Sony'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Sony'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/sony/video-games-4%7Cheadphones---and---headsets-373%7Cgame-consoles-6%7Cdigital-cameras-14%7Cgames-gadgets---and---accessories-7%7Cprojectors-13/sony%7Csony-ericsson/new/a-t-7-c/s/?_=1485430912358&page=1">Sony</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Canon', 'http://uae.souq.com/ae-en/canon/canon/new/a-7-c/s/?rpp=50&sortby=sr'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Canon'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Canon'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/canon/canon/new/a-7-c/s/?rpp=50&sortby=sr">Canon</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Microsoft', 'http://uae.souq.com/ae-en/microsoft-store/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Microsoft'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Microsoft'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/microsoft-store/p/">Microsoft</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Gopro', 'http://uae.souq.com/ae-en/gopro-uae/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Gopro'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Gopro'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/gopro-uae/p/">GoPro</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-HP', 'http://uae.souq.com/ae-en/hp/hp/new/a-7-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-HP'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-HP'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/hp/hp/new/a-7-c/s/">HP</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Dell', 'http://uae.souq.com/ae-en/dellstore-uae/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Dell'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Dell'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/dellstore-uae/p/">Dell</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-LG', 'http://uae.souq.com/ae-en/lg/televisions-10%7Ccomputer-monitors-188%7Cprojectors-13%7Chome-theater-systems-12%7Cmicrowaves-444%7Cspeakers-22%7Csmart-watches-511/lg/new/a-t-7-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-LG'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-LG'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/lg/televisions-10%7Ccomputer-monitors-188%7Cprojectors-13%7Chome-theater-systems-12%7Cmicrowaves-444%7Cspeakers-22%7Csmart-watches-511/lg/new/a-t-7-c/s/">LG</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Kenwood', 'http://uae.souq.com/ae-en/kenwood/kenwood/new/a-7-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Kenwood'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Kenwood'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/kenwood/kenwood/new/a-7-c/s/">Kenwood</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Braun', '/ae-en/braun/blenders---and---mixers-504%7Cfood-preparation-392%7Cfood-processors-506%7Ckettles-389%7Cjuicers---and---presses-384/braun/new/a-t-7-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Braun'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Braun'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/braun/blenders---and---mixers-504%7Cfood-preparation-392%7Cfood-processors-506%7Ckettles-389%7Cjuicers---and---presses-384/braun/new/a-t-7-c/s/?ref=nav&_=1496838989297&page=1">Braun</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Delonghi', 'http://uae.souq.com/ae-en/delonghi/new/a-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Delonghi'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Delonghi'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="http://uae.souq.com/ae-en/delonghi/new/a-c/s/">Delonghi</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Playstation', 'http://uae.souq.com/ae-en/playstation/sony/new/a-7-c/s/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Playstation'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Playstation'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="http://uae.souq.com/ae-en/playstation/sony/new/a-7-c/s/">Playstation</a></li>
                                                                <li><a onClick="pushData('internal_promotions', 'HP Flyout', 'Electronics-TopBrands-Philips', 'http://uae.souq.com/ae-ar/philips/deep-fryers-387%7Cirons-393%7Cjuicers---and---presses-384%7Cfood-processors-506%7Cfood-preparation-392%7Ccoffee---and---espresso-makers-418%7Ckettles-389%7Cvacuum-cleaners-413%7Crice-cooker-386%7Cblenders---and---mixers-504%7Cspecialty-kitchen-appliances-57/a-t/s/?_=1487608387958&page=1'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|Flyout|20170518|Electronics-TopBrands-Philips'; s.eVar20 = 'HP|Flyout|20170518|Electronics-TopBrands-Philips'; s.tl(this, 'o', 'Internal Campaign Tracking');"
                                                                       href="/ae-en/philips/deep-fryers-387%7Cirons-393%7Cjuicers---and---presses-384%7Cfood-processors-506%7Cfood-preparation-392%7Ccoffee---and---espresso-makers-418%7Ckettles-389%7Cvacuum-cleaners-413%7Crice-cooker-386%7Cblenders---and---mixers-504%7Cspecialty-kitchen-appliances-57/philips/a-t-7/s/">Philips</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="medium-3 text-opposite columns">
                                                <div class="row">
                                                    <a href="/ae-en/ae-laptops-exchange/c/">
                                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/may/07/Fly-Out-electronics-laptop-exchange-en.jpg" alt="Laptop Exchange Offer"> <!--image path-->
                                                    </a><br><br>
                                                    <a href="/ae-en/lcd-led-dlp-tv/l/">
                                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/fly-out/2017/may/07/Fly-Out-electronics-tv-en.jpg" alt="Best Deals on TVs"> <!--image path-->
                                                    </a>
                                                </div>
                                            </div>                </ul>

                                    </li>



                                    <li class="level0 menu-link is-dropdown-submenu-parent">
                                        <a href="/ae-en/ae-brand-stores/c/" data-menu-id="menu-7">Brand Stores</a>                            <ul class="menu-content center-menu-content small-12 columns">
                                            <!-- Brand Logos -->
                                            <div class="small-7 columns" style="margin-top: 25px;">
                                                <!--------------------- Row 01 --------------------->
                                                <div class="row">
                                                    <!-- Logo 01 -->    
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onClick="pushData('internal_promotions', 'Motorola Flyout', 'Flyout-Motorola-Brandstore 1807/2017', '/ae-en/motorola-uae/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'Motorola|Flyout|20160714|Flyout-Motorola-Brandstore'; s.eVar20 = 'Motorola|Flyout|20160714|Flyout-Motorola-Brandstore'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/motorola-uae/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/logos/flyout/motorola-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Motorola"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 02 -->        
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onClick="pushData('internal_promotions', 'CCC-Samsung Flyout', 'CCC-Samsung-Brandstore 25/01/2017', '/ae-en/samsung-store/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'CCC-Samsung|Flyout|20160515|CCC-Samsung'; s.eVar20 = 'CCC-Samsung|Flyout|20160509|CCC-Samsung'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/samsung-store/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2016_LP/brand-stores-cms/logo/samsung-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Samsung"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 03 -->        
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onclick="pushData('internal_promotions', 'OnePlus FlyOut', 'OnePlus-Brandstore 3001/2017', '/ae-en/oneplus-store/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'OnePlus|FlyOut|080617|OnePlus BrandStore'; s.eVar20 = 'OnePlus|FlyOut|080617|OnePlusBrandStore'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/oneplus-store/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/flyout/logos/oneplus.png" style="border-bottom: 1px solid #e8ebee;" alt="OnePlus"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 04 -->
                                                    <div class="medium-3 columns bottom-padding">
                                                        <a onclick="pushData('internal_promotions', 'Dell FlyOut', 'Dell-Brandstore 3001/2017', '/ae-en/dellstore-uae/p/');
                                                                s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Dell|FlyOut|030216|Dell-BrandStore';
                                                                s.eVar20 = 'Dell|FlyOut|030216|GoPro-BrandStore';
                                                                s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/dellstore-uae/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/dell-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Dell"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                </div>
                                                <!--------------------- Row 02 --------------------->
                                                <div class="row">
                                                    <!-- Logo 04 -->
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onclick="pushData('internal_promotions', 'Cerelac FlyOut', 'Cerelac-Brandstore 3001/2017', '/ae-en/cerelac/p/');
                                                                s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Cerelac|FlyOut|030216|CerelacBrandStore';
                                                                s.eVar20 = 'Cerelac|FlyOut|030216|CerelacBrandStore'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/cerelac/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/cerelac-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Cerelac"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 04 -->
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onClick="pushData('internal_promotions', 'Vimto Flyout', 'Vimto-Brandstore 1605/2017', '/ae-en/aujancocacola/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'Vimto|Flyout|20161008|Purina';
                                                                s.eVar20 = 'Vimto|Flyout|20170516|Vimto';
                                                                s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/aujancocacola/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/logos/flyout/vimto-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Vimto"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 04 -->
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onclick="pushData('internal_promotions', 'Philips Flyout', 'Philips-Brandstore 3001/2017', '/ae-en/philips/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'Philips|Flyout|20161026|Philips'; s.eVar20 = 'Philips|Flyout|20161026|Philips'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/philips/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/philips-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Philips"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 04 -->
                                                    <div class="medium-3 columns bottom-padding">
                                                        <a onClick="pushData('internal_promotions', 'Pepsi Flyout', 'Pepsi-Brandstore 3001/2017', '/ae-en/pepsicostore/p/');
                                                                s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'Pepsi|Flyout|20160515|Pepsi'; s.eVar20 = 'Pepsi|Flyout|20160509|Pepsi'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/pepsicostore/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/pepsi-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Pepsi"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                </div>
                                                <!--------------------- Row 03 --------------------->
                                                <div class="row">
                                                    <!-- Logo 05 -->
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onclick="pushData('internal_promotions', 'Microsoft FlyOut', 'Microsoft-Brandstore 1807/2017', '/ae-en/microsoft-store/p/'); s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Microsoft|FlyOut|030216|Microsoft-BrandStore';
                                                                s.eVar20 = 'Microsoft|FlyOut|180717|Xiaomi-BrandStore';
                                                                s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/microsoft-store/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2016_LP/brand-stores-cms/logo/microsoft-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Microsoft "/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 06 -->      
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onClick="pushData('internal_promotions', 'Marina Flyout', 'Marina-Brandstore 02/04/2017', '/ae-en/marina-home-outlet/p/');
                                                                s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Marina|Flyout|20161008|Brandstore-Marina';
                                                                s.eVar20 = 'Marina|Flyout|20161008|Brandstore-Marina'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/marina-home-outlet/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/FlyLogo.png" style="border-bottom: 1px solid #e8ebee;" alt="Marina "/> <!--image path-->
                                                        </a>
                                                    </div>
                                                    <!-- Logo 07 -->
                                                    <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                                        <a onclick="pushData('internal_promotions', 'Puma FlyOut', 'Puma-Brandstore 3001/2017', '/ae-en/only-uae/p/');
                                                                s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Puma|FlyOut|030216|Puma-BrandStore';
                                                                s.eVar20 = 'Forever 21|FlyOut|030216|Puma-BrandStore';
                                                                s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/puma-uae/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2016_LP/brand-stores-cms/logo/puma-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Puma"/> <!--image path-->
                                                        </a>
                                                    </div>


                                                    <!-- Logo 08 -->    
                                                    <div class="medium-3 columns bottom-padding">
                                                        <a onclick="pushData('internal_promotions', 'Only Flyout', 'Only-Brandstore 3001/2017', '/ae-en/Only-uae/p/'); s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Only|Flyout|20161008|Brandstore-Only';
                                                                s.eVar20 = 'Only|Flyout|20161008|Brandstore-Only';
                                                                s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/Only-uae/p/">
                                                            <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/only02-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Only"/> <!--image path-->
                                                        </a>
                                                    </div>
                                                </div>    
                                                <!-- Row 04 - Shop All -->	
                                                <div class="row" style="text-align:center;">
                                                    <ul>
                                                        <li>             
                                                            <a onClick="pushData('internal_promotions', 'Shop All Brands Flyout', 'Shop All Brands-brandstore 3001/2017', '/ae-en/ae-brand-stores/c/');
                                                                    s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'Shop All Brands|Flyout|20161023|brandstore-Shop All Brands';
                                                                    s.eVar20 = 'Shop All Brands|Flyout|20161023|brandstore-Shop All Brands';
                                                                    s.tl(this, 'o', 'Internal Campaign Tracking');" 
                                                               href="/ae-en/ae-brand-stores/c/">
                                                                <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/en-btn-flyout.png" alt="Shop All Brands"></a>           
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- End Row 04 - Shop All -->	         
                                            </div>
                                            <!-- End Brand Logos --> 



                                            <!-- Adv Banner --> 
                                            <div class="small-5 columns">
                                                <a onClick="pushData('internal_promotions', 'Motorola July 18 Flyout', 'Motorola-brandstore 3001/2017', '/ae-en/ae-motorola-g5plus/c/');
                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                        s.eVar3 = 'Motorola|Flyout|20161023|brandstore-Motorola';
                                                        s.eVar20 = 'Motorola|Flyout|20161023|brandstore-Motorola';
                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" 
                                                   href="/ae-en/ae-motorola-g5plus/c/">
                                                    <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/motorola/g5plus/moto-g5-flyout.jpg" alt="Moto G5 Plus"/> <!--image path-->
                                                </a>
                                            </div>
                                            <!-- End Adv Banner -->                </ul>

                                    </li>


                                </ul>
                            </div>
                        </div>    </div>
                </div>

                <div id="appboy-newsfeed-cont">
                    <i class="close-button close-newsfeed">×</i>
                </div>
                <aside class="main-side-canvas off-canvas position-left" id="offCanvasLeft" data-off-canvas data-position="left" data-close-on-click="true">
                    <ul class="off-canvas-list mobile-ofc vertical menu">
                        <li>
                            <dl class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                                <dt class="font-normal"><a href="" class="level2">Home</a></dt>
                                <dt class="font-normal"><a href="" class="level2">Deals</a></dt>
                                <dt class="font-normal"><a href="" class="level2">Track Orders</a></dt>
                                <dt class="font-normal"><a href="" class="level2">My Account</a></dt>
                                <dt class="font-normal"><a href="" class="level2">Contact Us</a></dt>
                                <dt class="font-normal">
                                    <a id="appboy-newsfeed">
                                        Newsfeed <span id="appboy-newsfeed-counter" style="display: none;"></span>
                                    </a>
                                </dt>
                            </dl>
                        </li>
                        <li><label>Categories</label></li>
                        <li>
                            <dl class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                                <dd class="accordion-navigation accordion-item" data-accordion-item>
                                    <a href="#" class="level2 accordion-title tr-enabled" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Souq Fashion <i></i></a>
                                    <div id="panel6" class="content accordion-content" data-tab-content>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">New Arrivals</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Women</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Men</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Handbags</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Watches</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Sports</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Kids</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Outlet</a>
                                    </div>
                                </dd>
                                <dt class="font-normal">
                                    <a href="/ae-en/advanced_search.php" class="level2">Advanced Search</a>
                                </dt>
                            </dl>
                        </li>
                    </ul>
                </aside>

                <div class="js-off-canvas-exit"></div>
                <!-- //end -->

                <main class="main-section">
                    <div class="row show-for-small-only-override">
                        <div class="small-12 medium-12 large-12 xlarge-9 columns">
                            <div class="row">
                                <div class="xlarge-6 large-6 columns" style="line-height:50px;">
                                    <div class="panel callout-panel dod-callout">
                                        <a href="" class="row text-center block" onClick="pushData('internal_promotions', 'HP DODBanner', 'Deals ', 'http://deals.souq.com/ae-en/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'HP|DODBanner|20150809|Deal oF the day'; s.eVar20 = 'HP|DODBanner|20150809|Deal oF the day'; s.tl(this, 'o', 'Internal Campaign Tracking');
                                                ">
                                            <div class="small-6 medium-4 columns" style=" margin-top:-5px !important;">
                                                <img src="templates/images/deals-en2.png" alt="">
                                            </div>
                                            <div class="medium-5 columns hide-for-small-only">
                                                <img src="templates/images/DOD-240217-features-3.gif" alt="">
                                            </div>
                                            <div class="small-6 medium-3 columns">
                                                <div class="small button alert tiny" style="background-color: #252933;
                                                     background-image: -webkit-gradient(linear,right top,right bottom,from(#252933),to(#252933));
                                                     background-image: -webkit-linear-gradient(top,#252933,#252933); height:30px; margin-top:9px; padding-top:2px;">Shop Now</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="xlarge-6 large-6 columns hide-for-medium-down">
                                    <a href=""><img src="templates/images/tlb_bags_en.jpg" style="height:60px" alt="Top deals on bags"></a>
                                </div> 
                            </div>
                        </div>

                        <div class="large-5 xlarge-3 hide-for-large-down columns">
                            <div class="spring-dynamic-dfp" data-width="300" data-height="60" style="height:60px;">
                                <img src="templates/images/banner3.jpg" alt="" border="0">
                            </div>
                        </div>
                    </div>

                    <div id="content-body">
                        <div class="row">
                            <!--SLIDER-->
                            <div class="small-12 xlarge-9 rel">
                                <div class="large-12 columns market-box" data-track-banner="marketingBox6518">
                                    <div class="slide feature-panel hide">
                                        <a href="" onclick=""><img src="templates/public/style/img/blank.gif" class="slick-loading" data-lazy="templates/images/slider1.jpg" alt="Super Saver" /></a>
                                        <a href="" class="overlay-label">Slider1</a>
                                    </div>
                                    
                                    <div class="slide feature-panel hide">
                                        <a href="" onclick=""><img src="templates/public/style/img/blank.gif" class="slick-loading" data-lazy="templates/images/slider2.jpg" alt="Super Saver" /></a>
                                        <a href="" class="overlay-label">Slider2</a>
                                    </div>
                                    
                                    <div class="slide feature-panel hide">
                                        <a href="" onclick=""><img src="templates/public/style/img/blank.gif" class="slick-loading" data-lazy="templates/images/slider3.jpg" alt="Super Saver" /></a>
                                        <a href="" class="overlay-label">Slider3</a>
                                    </div>
                                    
                                    <div class="slide feature-panel hide">
                                        <a href="" onclick=""><img src="templates/public/style/img/blank.gif" class="slick-loading" data-lazy="templates/images/slider4.jpg" alt="Super Saver" /></a>
                                        <a href="" class="overlay-label">Slider4</a>
                                    </div>
                                </div>
                            </div>
                            <!--END SLIDER-->

                            <div class="show-for-xlarge-up xlarge-3 columns">
                                <div class="large-12 columns">
                                    <div class="row ad">
                                        <div class="show-for-medium-up small-12 columns ads-box clearPadding-h">
                                            <a href=""><img src="templates/images/banner1.jpg" alt="" border="0" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-12 columns xlarge-top-banner">
                                    <div class="row ad">
                                        <div class="show-for-medium-up small-12 columns ads-box clearPadding-h">
                                            <a href="">
                                                <img src="templates/images/banner2.jpg" alt="" border="0" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-bottom:2rem;">
                            <div class="columns small-12 medium-12 large-12 hide-for-small-only"> 
                                <a href=""><img src="templates/images/standalone-incredible-iphone-sale-v2-en.jpg" alt="The Incredible iPhone Sale"></a>
                            </div>
                            <div class="columns small-12 medium-12 large-12 show-for-small-only"> 
                                <a href="">
                                    <img src="templates/images/AMB-Incredible-iphone-sale-v2-en.jpg" alt="The Incredible iPhone Sale"></a>
                            </div>
                        </div>
                        <div class="row boxTab">
                            <div class="small-12 columns">
                                <ul class="tabs curation-tabs show-for-large" data-tabs id="multi-box-tabs">
                                    <li aria-selected='true'  class='tabs-title is-active'><a class='' href='#panel1_71553'>Electronics</a></li>
                                    <li aria-selected='true'  class='tabs-title'><a class='' href='#panel1_71554'>Electronics 2</a></li>
                                </ul>        
                                <!--Tabs-->
                                <div class="row ">
                                    <div class="small-12 columns">
                                        <div class="tabs-content curation-tabs-content" data-tabs-content="multi-box-tabs">
                                            <div class="hide-for-large curation-tabs"> 
                                                <div class="small-8 columns">
                                                    <h3 class="mobile-curation-selected-tab">Electronics</h3>
                                                </div>
                                                <!--For mobile-->
                                                <div class="small-4 columns curation-tabs-menu">
                                                    <a href="javascript:void(0);" data-toggle="categories_71553" aria-expanded="false"> more <i class="arrow-down"></i></a>
                                                </div>
                                                <div class="small-12 columns" style="position:relative;">
                                                    <ul id="categories_71553" class="f-dropdown curation-tab-box-dropdown dropdown-pane bottom" data-dropdown-content="categoriesList" data-dropdown aria-hidden="true" data-close-on-click="true" aria-autoclose="true" tabindex="-1">
                                                        <li class=""><a href="#" data-link="#panel1_71553">Electronics</a></li>                              
                                                        <li class=""><a href="#" data-link="#panel1_71553">Electronics</a></li>                              
                                                    </ul>
                                                </div>
                                                <!--End For mobile-->
                                            </div>

                                            <div id="panel1_71553" data-track-banner="multipleTabsBox6957" class="tabs-panel row content  is-active">
                                                <div class="small-12 large-6 columns">
                                                    <div class="featured-category">
                                                        <a href="" prefix="" data-tracking="">
                                                            <img src="templates/public/style/img/blank.gif" alt="" data-lazy="templates/images/1501393019_Tab2-RI1-Mobiles-Tablets.jpg" 
                                                                 data-interchange="[templates/images/S_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, small],
                                                                 [templates/images/M_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, medium],
                                                                 [templates/images/L_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, large],
                                                                 [templates/images/XL_1501393019_Tab2-RI1-Mobiles-Tablets.jpg, xlarge],
                                                                 [templates/images/1501393019_Tab2-RI1-Mobiles-Tablets.jpg, default]" />
                                                            <h4>Mobiles & Tablets</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="small-12 large-6 columns">
                                                    <div class="row">    
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="" prefix="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="Televisions" data-lazy="templates/images/1501393019_Tab2-RI2-Televisions.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI2-Televisions.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI2-Televisions.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI2-Televisions.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI2-Televisions.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI2-Televisions.jpg, default]" />
                                                                    <h4>Televisions</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="" data-lazy="templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI3-DSLR-Cameras.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI3-DSLR-Cameras.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI3-DSLR-Cameras.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI3-DSLR-Cameras.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg, default]" />
                                                                    <h4>DSLR Cameras</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">    
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="" prefix="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="Televisions" data-lazy="templates/images/1501393019_Tab2-RI2-Televisions.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI2-Televisions.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI2-Televisions.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI2-Televisions.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI2-Televisions.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI2-Televisions.jpg, default]" />
                                                                    <h4>Televisions</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="small-6 large-6 columns">
                                                            <div class="secondary-category">
                                                                <a href="">
                                                                    <img src="templates/public/style/img/blank.gif" alt="" data-lazy="templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg" 
                                                                         data-interchange="[templates/images/S_1501393019_Tab2-RI3-DSLR-Cameras.jpg, small],
                                                                         [templates/images/M_1501393019_Tab2-RI3-DSLR-Cameras.jpg, medium],
                                                                         [templates/images/L_1501393019_Tab2-RI3-DSLR-Cameras.jpg, large],
                                                                         [templates/images/XL_1501393019_Tab2-RI3-DSLR-Cameras.jpg, xlarge],
                                                                         [templates/images/1501393019_Tab2-RI3-DSLR-Cameras.jpg, default]" />
                                                                    <h4>DSLR Cameras</h4>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panel1_71554" class="tabs-panel row content">Hello</div>
                                        </div>
                                    </div>
                                </div>
                                <!--/Tabs-->
                            </div>
                        </div><div class="row" style="margin-bottom:2rem;">
                            <div class="columns small-12 medium-12 large-12 hide-for-small-only"> 
                                <a href="">
                                    <img src="templates/images/standalone-best-buys-ae-en.gif" alt=""></a>
                            </div>
                            <div class="columns small-12 medium-12 large-12 show-for-small-only"> 
                                <a href="">
                                    <img src="templates/images/best-buys-standalone-small-ae-en.gif" alt=""></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="small-12 columns">
                                <h3 class="slider-header recommendation-title"><a href="javascript:void(0);">Popular Products</a></h3>
                            </div>
                        </div>

                        <div class="sliderViewPort row PopularItems bottom-spacing ">
                            <div class="small-12 columns clearPadding-h">
                                <div class="multi-slider preSlickSlider" data-no-of-items="5" data-no-of-items-for-xlarge="7" dir="ltr"  data-track-slider='PopularItems7109'>
                                        
                                        <div class="slide hide">
                                            <div class="mini-placard">
                                                <a href="" class="itemLink" data-enid="2724326956718" data-unitid="19293000322" data-itemprice="2" data-position="1" data-brand-name="|Other" data-category-name="Sporting Goods" title="">&nbsp;</a>
                                                <div class="img-bucket">
                                                    <span class="align-image-vertically">
                                                        <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                    </span>
                                                </div>
                                                <h6 class="title"><a href="" title="">Skull print Multi-Purpose Scarf/Bandana/Mask</a></h6>
                                                <div class="stars-rating">
                                                    <div class="star-rating clearMargin-v-large">
                                                        <a href="javascript:void(0);">
                                                            <div class="stars">
                                                                <div class="colored" style="width: 76%; ">
                                                                    <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                </div>
                                                                <div class="uncolored">
                                                                    <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                </div>
                                                            </div>
                                                        </a>&nbsp;&nbsp;<span class="rating-reviews">(344)</span>                            </div>
                                                </div>
                                                <h5 class="price">
                                                    <span class="was block">3.00</span>
                                                    <span class= "is block sk-clr1">2.00 <small class="currency-text sk-clr1">AED</small></span>
                                                </h5>
                                                <div class="recommendations-fsp-badge ">
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="slide hide">
                                            <div class="mini-placard">
                                                <a href="" class="itemLink" data-enid="2724326956718" data-unitid="19293000322" data-itemprice="2" data-position="1" data-brand-name="|Other" data-category-name="Sporting Goods" title="">&nbsp;</a>
                                                <div class="img-bucket">
                                                    <span class="align-image-vertically">
                                                        <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                    </span>
                                                </div>
                                                <h6 class="title"><a href="" title="">Skull print Multi-Purpose Scarf/Bandana/Mask</a></h6>
                                                <div class="stars-rating">
                                                    <div class="star-rating clearMargin-v-large">
                                                        <a href="javascript:void(0);">
                                                            <div class="stars">
                                                                <div class="colored" style="width: 76%; ">
                                                                    <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                </div>
                                                                <div class="uncolored">
                                                                    <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                </div>
                                                            </div>
                                                        </a>&nbsp;&nbsp;<span class="rating-reviews">(344)</span>                            </div>
                                                </div>
                                                <h5 class="price">
                                                    <span class="was block">3.00</span>
                                                    <span class= "is block sk-clr1">2.00 <small class="currency-text sk-clr1">AED</small></span>
                                                </h5>
                                                <div class="recommendations-fsp-badge ">
                                                </div>
                                            </div>
                                        </div> 
                                    
                                </div>
                            </div>
                        </div>



                        <div class="row boxTab">
                            <div class="small-12 columns filter-tabs-container">
                                <div class="row filter-tabs-header">
                                    <div class="small-6 medium-4 large-6 columns">
                                        <h3 class="slider-header">Electronics</h3>
                                    </div>
                                    <div class="small-6 medium-8 large-6 ddown-container columns">
                                        <a href="javascript:void(0);" class="tiny secondary button expand clearMargin show-for-small-only" data-toggle="tabsDropdownLarge70160">
                                            <i class="fi-list"></i><span class='value'> Laptops</span>
                                        </a>
                                        <ul id="tabsDropdownLarge70160" data-dropdown data-close-on-click="true" class="dropdown-pane f-dropdown filter-group">
                                            <li class="hide"><a href="javascript:void(0);" class="hide" data-id-box="70160" data-id-panel="panel70160">Laptops</a></li>
                                            <li><a href="javascript:void(0);" class="" data-id-box="70161" data-id-panel="panel70160">TVs</a></li>
                                            <li><a href="javascript:void(0);" class="" data-id-box="70162" data-id-panel="panel70160">Accessories</a></li>
                                            <li><a href="javascript:void(0);" class="" data-id-box="70164" data-id-panel="panel70160">Gaming</a></li>
                                        </ul>
                                        
                                        <div class="clearfix show-for-medium">
                                            <ul class="tabs home-tabs" data-tabs>
                                                <li class="tabs-title loaded is-active"><a data-id-box="70160" href="#panel70160">Laptops 1</a>
                                                <li class="tabs-title"><a data-id-box="70161" href="#panel70160">TVs</a></li>
                                                <li class="tabs-title"><a data-id-box="70162" href="#panel70160">Accessories</a></li>
                                                <li class="tabs-title"><a data-id-box="70164" href="#panel70160">Gaming</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                <!--Tabs-->
                                <div class="row box-tab-slider">
                                    <div class="small-12 columns clearPadding-h-large">
                                        <div class="tabs-content" data-tabs-content>
                                            <div class="content is-active loaded" id="panel70160">
                                                <div class="sliderViewPort row">
                                                    <div class="small-12 columns clearPadding-h">
                                                        <div class="multi-slider preSlickSlider " data-no-of-items="5" data-no-of-items-for-xlarge="6" dir="ltr"  data-track-slider='7099'>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="slide hide">
                                                                <div class="mini-placard  ">
                                                                    <a href="" class="itemLink" data-enid="2" data-unitid="0" data-itemprice="3099" data-position="1" data-brand-name="|" data-category-name="" title="">&nbsp;</a>
                                                                    <div class="img-bucket">
                                                                        <span class="align-image-vertically">
                                                                            <a href="" title=""><img src="templates/public/style/img/blank.gif" data-lazy="templates/images/item_L_10660000_14114107.jpg" alt="" /></a>
                                                                        </span>
                                                                        <span class="discount">10 %  off</span>                                                                                </div>
                                                                    <h6 class="title"><a href="" title="">Apple MacBook Air Laptop</a></h6>
                                                                    <div class="stars-rating">
                                                                        <div class="star-rating clearMargin-v-large">
                                                                            <a href="javascript:void(0);">
                                                                                <div class="stars">
                                                                                    <div class="colored" style="width: 50%; ">
                                                                                        <i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i><i class="fi-star on"></i>
                                                                                    </div>
                                                                                    <div class="uncolored">
                                                                                        <i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i><i class="fi-star off"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a>&nbsp;&nbsp;<span class="rating-reviews">(173)</span>
                                                                        </div>
                                                                    </div>
                                                                    <h5 class="price">
                                                                        <span class="was block"></span>
                                                                        <span class= "is block sk-clr1">3,099.00 <small class="currency-text sk-clr1">AED</small></span>
                                                                    </h5>
                                                                    <div class="recommendations-fsp-badge ">
                                                                        <div class="block fs-ab-black">
                                                                            <strong class='green-text'>FREE Shipping</strong>
                                                                        </div>
                                                                        <div class="block">
                                                                            <div class="fbs"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="small-12 columns viewAllLink">
                                                            <a href="" class="fr"> View All Laptops </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/Tabs-->
                            </div>
                        </div>
                    </div>
                </main>

                <footer class="wide-screen">
                    <div class="first-row row">
                        <div class="large-3 large-push-9 columns">
                            <div class="callout callout-panel radius customer-service-panel">
                                <h6>Customer Service</h6>
                                <ul class="footer-links">
                                    <li><a href=""><i class="fi-mail"></i> Contact us</a></li>
                                    <li><span rel="tel">(+971)-4-437 0900</span> </li>
                                </ul>
                            </div>
                            <h6 class="">Follow Us</h6>
                            <ul class="menu clearfix">
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-twitter"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-facebook"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-youtube"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-instagram"></i></a></li>
                                <li><a rel="nofollow" href="" class="social" target="_blank"><i class="fi-social-pinterest"></i></a></li>
                            </ul>
                            <h6>Download our apps</h6>
                            <ul class="menu">
                                <li><a href="" class="social"><i class='fi-social-android'></i></a> </li>
                                <li><a href="" class="social"><i class='fi-social-apple'></i></a> </li>
                            </ul>
                        </div>
                        <div class="large-3 large-pull-3 columns show-for-medium">
                            <h6>Popular Searches</h6>
                            <ul class="footer-links">
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                                <li><a href="">IPhone 7</a></li>
                            </ul>
                        </div>
                        <div class="large-3 large-pull-3 columns">
                            <h6>My Account</h6>

                            <ul class="footer-links">
                                <li id="loginLogoutReplace">
                                    <a href="">Log In</a> <a href=""> / Register</a>
                                </li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                                <li><a href="">My Shopping Cart</a></li>
                            </ul>

                            <h6>Intellectual Property</h6>
                            <ul class="footer-links">
                                <li><a href="">Brand and Copyright Owners</a></li>
                                <li><a href="">Marketplace Sellers</a></li>
                            </ul>
                        </div>
                        <div class="large-3 large-pull-3 columns show-for-medium">
                            <h6>Selling on Souq.com</h6>
                            <ul class="footer-links">
                                <li><a href="">Sell on souq.com</a></li>
                                <li><a href="">How It Works</a></li>
                            </ul>

                            <h6>Buying on Souq.com</h6>
                            <ul class="footer-links">
                                <li><a href="">How to Buy</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="second-row">
                        <div class="row">
                            <div class="large-9 columns">
                                <div class="row">
                                    <div class="small-12 columns">
                                        <strong>&copy; 2017 Souq.com</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 columns footer-lower-links">
                                        <ul class="h-list text-default">
                                            <li><a href="">About Us</a></li>
                                            <li><a href="">Media Center</a></li>
                                            <li><a href="">Careers</a></li>
                                            <li><a href="">Privacy Policy</a></li>
                                            <li><a href="">Terms and Conditions</a></li>
                                            <li><a href="">Affiliate Program</a></li>
                                            <li><a href="">Helpbit Services</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="small-12 large-1 end text-center columns clearPadding-h">

                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- /INNER WRAP -->
        </div>
                <script type="text/javascript" src="design/themes/tnh_theme/templates/js/libs.93738d748552c5b631465c4173dabdf9.js"></script>
<script type="text/javascript" src="design/themes/tnh_theme/templates/js/index.93738d748552c5b631465c4173dabdf9.js"></script>
<script type="text/javascript" src="design/themes/tnh_theme/templates/js/souq.93738d748552c5b631465c4173dabdf9.js"></script>
<script type="text/javascript" src="design/themes/tnh_theme/templates/js/s_code_new.js"></script>
<script type="text/javascript" src="design/themes/tnh_theme/templates/js/s_plugin_new.js"></script>
        {include file="common/scripts.tpl"}
        
    </body>
</html>