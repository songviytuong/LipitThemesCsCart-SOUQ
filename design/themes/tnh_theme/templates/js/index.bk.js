! function() {
    var a = !1;
    ! function(b, c) {
        function d() {
            var b = define;
            define = function() {
                arguments.length && "string" == typeof arguments[0] ? (define = b, a = arguments[0], window.hasAlmondJS ? define.apply(this, arguments) : c()) : (define = b, c())
            }
        }
        b.SouqLoading || (b.hasDefine = "function" == typeof b.define && b.define.amd, b.hasRequireJS = b.hasDefine && "function" == typeof b.require && "object" == typeof b.require.s, b.hasAlmondJS = b.hasDefine && !b.hasRequireJS), b.hasDefine ? (d(), define("components/dropdown", [], c)) : c()
    }(window, function() {
        function b() {
            return "components/dropdown"
        }
        var c = b(),
            d = function() {
                if (window.hasRequireJS)
                    if (arguments.length && "string" == typeof arguments[0] && arguments[0] === c) {
                        var b = Array.prototype.slice.call(arguments);
                        a && a === c || (a ? b[0] = a : b.shift()), window.define.apply(this, b)
                    } else window.define.apply(this, arguments);
                else window.define.apply(this, arguments)
            };
        d.amd = {
            jQuery: !0
        }, d("components/dropdown", ["core"], function(a) {
            var b = new a.Component("Dropdown", {
                    selector: "",
                    DropdownId: null,
                    attrId: null
                }),
                c = b.dom,
                d = a.utils.flags();
            return b.run = function(a) {
                this.base(a)
            }, b.ready = function() {
                if (!this.options.selector || !this.options.DropdownId) return void a.log.warn(new Error("failed to get dropdown selectors"));
                c(this.options.selector).removeAttr("disabled").click(function(a) {
                    a.preventDefault();
                    var b = null,
                        e = c(a.currentTarget),
                        f = e.closest(".row");
                    f.find("[id^=" + this.options.DropdownId + "]").find("li a.hide").removeClass("hide"), f.find("[id^=" + this.options.DropdownId + "]").find("li.hide").removeClass("hide"), e.closest("li a").addClass("hide"), e.closest("li").addClass("hide"), c(e.closest("[data-dropdown]").trigger("closed.fndtn.dropdown")).foundation("close"), e.closest("ul").removeClass("open").css(d.rtl ? "right" : "left", "-9999px"), f.find("a[data-toggle*=" + this.options.DropdownId + "] span.value").html(" " + c(a.currentTarget).html());
                    var g = c("#" + e.attr("data-id-panel")),
                        h = g.closest(".tabs-content");
                    if (h.length) {
                        var i = h.find(".active");
                        h.css("min-height", i.height()), i.removeClass("active"), g.addClass("active")
                    }
                    b = this.options.attrId ? e.attr(this.options.attrId) : this.options.DropdownId, this.sandbox.publish("dropdown:click", e, h, g, b)
                }.bind(this)), c("[data-dropdown]").off("show.zf.dropdown").on("show.zf.dropdown", function() {
                    a.sandbox.publish("dropdown:opened", {}, this)
                }).off("hide.zf.dropdown").on("hide.zf.dropdown", function() {
                    a.sandbox.publish("dropdown:closed", {}, this)
                })
            }, b.reload = function() {
                this.ready()
            }, b
        });
        var c = b();
        if (void 0 === c) throw new Error((a || "Component") + " Build Error : no identifer detected!");
        if (window.hasDefine) {
            if (window.hasAlmondJS) return window.require(c)
        } else require(c)
    })
}(), define("components/multi-tabs-curation", ["souq", "jquery", "components/dropdown"], function(a, b, c) {
        var d = new a.Component("multi-tabs-curation", {
            tabsSelector: ".curation-tabs",
            dropdownSelector: ".curation-tab-box-dropdown",
            boxDropdownTitle: ".mobile-curation-selected-tab"
        });
        return d.ready = function() {
            b(d.options.dropdownSelector).each(function() {
                var a = b(this).attr("id");
                c.clone(a).run({
                    DropdownId: a,
                    selector: "#" + a + "  li a"
                })
            }), a.sandbox.subscribe("dropdown:click", function(a) {
                var c = ".tabs-title a[href='" + b(a).data("link") + "']";
                b(a).parents(d.options.tabsSelector).find(d.options.boxDropdownTitle).text(b(a).text()), b(c).trigger("click")
            }), b(d.options.tabsSelector).on("toggled", function(a, c) {
                var e = d.options.dropdownSelector + ' a[data-link="' + b(c).children("a").attr("href") + '"]';
                b(c).parent(d.options.tabsSelector).siblings().find(d.options.boxDropdownTitle).text(b(c).text()), b(e).parent().siblings().each(function() {
                    b(this).removeClass("hide")
                }), b(e).parent().addClass("hide")
            })
        }, d
    }), define("components/mobileLeadAcquisition", ["souq", "jquery"], function(a, b) {
        function c() {
            var b = e("#mobileLeadAcquisitionCookieName").val();
            if (!a.utils.getCookie(b)) {
                new window.Foundation.Reveal(e("#leadAcquisitionContainer")).open(), a.utils.setCookie(b, !0);
                var c = a.tracking.getTracker(),
                    d = a.getConfig("HOST"),
                    f = d.split("/");
                c && c.t ? (c.pageName = "souq:" + a.getConfig("country") + ":MobileLeadPage", c.channel = "MobileLead", c.prop8 = a.getConfig("language"), c.prop1 = "HomePage", c.prop2 = "MobileLead", c.prop3 = "HomePage", c.eVar38 = a.getConfig("country"), c.s_SSLLink = "http://", c.server_name = f[2] ? f[2] : "uae.souq.com", c.t(this, "o", "Mobile Leads Reveal Tracking"), delete c.s_SSLLink, delete c.server_name) : a.log.logError(new Error("Omniture (s) not defined!"))
            }
        }
        var d = new a.Component("mobileLeadAcquisition", {}),
            e = d.dom;
        return d.ready = function() {
            e("#mobileLeadAcquisitionButton").on("click", function() {
                document.getElementById("mobileLeadAcquisitionButton").disabled = !0, e(e("#leadAcquisitionContainer").find(".error")[0]).text("");
                var c = e("#mobileCountryCode").text().replace("+", "") + e("#mobileLeadAcquisitionPhone").val().replace(/^0+/, ""),
                    d = a.utils.getCookie("idc"),
                    f = a.utils.getCookie("c_Ident");
                b.ajax({
                    url: "Action.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        action: "ajaxRemote",
                        ajaxrequest: "Utils_handleMobileLeadAcquisitionPopup",
                        mobile: c,
                        id_customer: d || null,
                        c_ident: f,
                        country: a.getConfig("country"),
                        language: a.getConfig("language")
                    },
                    success: function(b) {
                        if (!0 === b.success) {
                            e("#leadAcquisitionContainer").foundation("close");
                            var c = a.getConfig("HOST"),
                                d = c.split("/"),
                                f = a.tracking.getTracker();
                            f && f.t && (f.pageName = "souq:" + a.getConfig("site") + ":MobileLeadPage", f.channel = "MobileLead", f.prop8 = a.getConfig("language"), f.prop1 = "HomePage", f.prop2 = "MobileLead", f.prop3 = "HomePage", f.eVar38 = a.getConfig("site"), f.s_SSLLink = "http://", f.server_name = d[2] ? d[2] : "uae.souq.com", f.linkTrackVars = "events", f.linkTrackEvents = f.events = "event22", f.t(this, "o", "Mobile Leads Tracking")), e("#leadAcquisitionThankYouContainer").foundation("open"), setTimeout(function() {
                                e("#leadAcquisitionThankYouContainer").foundation("close")
                            }, 5e3)
                        } else e(e("#leadAcquisitionContainer").find(".error")[0]).text(b.message), document.getElementById("mobileLeadAcquisitionButton").disabled = !1
                    }
                })
            }), e(".close-button").on("click", function() {
                e("#leadAcquisitionContainer").foundation("close")
            }), e("#mobileLeadAcquisitionPhone").on("keypress", function(a) {
                return 13 === a.keyCode && e("#mobileLeadAcquisitionButton").click(), a.charCode >= 48 && a.charCode <= 57 || 8 === a.keyCode || 46 === a.keyCode
            }), c()
        }, d
    }),
    function() {
        var a = !1;
        ! function(b, c) {
            function d() {
                var b = define;
                define = function() {
                    arguments.length && "string" == typeof arguments[0] ? (define = b, a = arguments[0], window.hasAlmondJS ? define.apply(this, arguments) : c()) : (define = b, c())
                }
            }
            b.SouqLoading || (b.hasDefine = "function" == typeof b.define && b.define.amd, b.hasRequireJS = b.hasDefine && "function" == typeof b.require && "object" == typeof b.require.s, b.hasAlmondJS = b.hasDefine && !b.hasRequireJS), b.hasDefine ? (d(), define("components/form", [], c)) : c()
        }(window, function() {
            function b() {
                return "components/form"
            }
            var c = b(),
                d = function() {
                    if (window.hasRequireJS)
                        if (arguments.length && "string" == typeof arguments[0] && arguments[0] === c) {
                            var b = Array.prototype.slice.call(arguments);
                            a && a === c || (a ? b[0] = a : b.shift()), window.define.apply(this, b)
                        } else window.define.apply(this, arguments);
                    else window.define.apply(this, arguments)
                };
            d.amd = {
                    jQuery: !0
                },
                function() {
                    "use strict";

                    function a(a, b, c, d, e, f) {
                        b[a] && (c.push(a), !0 !== b[a] && 1 !== b[a] || d.push(e + a + "/" + f))
                    }

                    function b(a, b, c, d, e) {
                        var f = d + b + "/" + e;
                        require._fileExists(a.toUrl(f + ".js")) && c.push(f)
                    }

                    function c(a, b, d) {
                        var e;
                        for (e in b) !b.hasOwnProperty(e) || a.hasOwnProperty(e) && !d ? "object" == typeof b[e] && (!a[e] && b[e] && (a[e] = {}), c(a[e], b[e], d)) : a[e] = b[e]
                    }
                    var e = /(^.*(^|\/)nls(\/|$))([^\/]*)\/?([^\/]*)/;
                    d("i18n", ["module"], function(d) {
                        var f = d.config ? d.config() : {};
                        return {
                            version: "2.0.4",
                            load: function(d, g, h, i) {
                                i = i || {}, i.locale && (f.locale = i.locale);
                                var j, k, l, m = e.exec(d),
                                    n = m[1],
                                    o = m[4],
                                    p = m[5],
                                    q = o.split("-"),
                                    r = [],
                                    s = {},
                                    t = "";
                                if (m[5] ? (n = m[1], j = n + p) : (j = d, p = m[4], o = f.locale, o || (o = f.locale = "undefined" == typeof navigator ? "root" : (navigator.language || navigator.userLanguage || "root").toLowerCase()), q = o.split("-")), i.isBuild) {
                                    for (r.push(j), b(g, "root", r, n, p), k = 0; k < q.length; k++) l = q[k], t += (t ? "-" : "") + l, b(g, t, r, n, p);
                                    g(r, function() {
                                        h()
                                    })
                                } else g([j], function(b) {
                                    var d, e = [];
                                    for (a("root", b, e, r, n, p), k = 0; k < q.length; k++) d = q[k], t += (t ? "-" : "") + d, a(t, b, e, r, n, p);
                                    g(r, function() {
                                        var a, d, f;
                                        for (a = e.length - 1; a > -1 && e[a]; a--) f = e[a], d = b[f], !0 !== d && 1 !== d || (d = g(n + f + "/" + p)), c(s, d);
                                        h(s)
                                    })
                                })
                            }
                        }
                    })
                }(), d("components/captcha/nls/labels", {
                    root: {
                        sCaptchaError: "You entered wrong numbers, please try again."
                    },
                    "en-US": !0,
                    "ar-AE": !0
                }), d("components/captcha/nls/en-US/labels", {
                    sCaptchaError: "You entered wrong numbers, please try again."
                }), d("extensions/api", ["core", "jquery"], function(a, b) {
                    var c = new a.Extension("API"),
                        d = function(b, c, d, e) {
                            if (this.baseUrl = a.utils.flags().host, b) this.url = -1 === b.indexOf("http") && this.baseUrl ? this.baseUrl + "/" + b : b;
                            else if (a.getConfig("debug")) throw new Error("Request parameter missing: URL");
                            c && (this.action = c), d && (this.method = d), e && (this.cache = e)
                        };
                    return d.prototype.url = "", d.prototype.action = "", d.prototype.data = {}, d.prototype.method = "GET", d.prototype.dataType = "json", d.prototype.cache = !1, d.prototype.baseUrl = "", d.prototype.send = function(d, e, f, g, h) {
                        var i = {
                            url: this.url,
                            cache: this.cache,
                            type: this.method,
                            timeout: 6e4,
                            headers: {},
                            data: {},
                            dataType: this.dataType,
                            context: "ajax"
                        };
                        i.data.action = this.action, delete this.data.action, delete d.action, d.serviceController && d.serviceMethod && (i.data.ajaxrequest = d.serviceController + "_" + d.serviceMethod), delete d.serviceController, delete d.serviceMethod, a.utils.extend(i.data, d.data), a.utils.extend(i.data, this.data), e = d.success || e, f = d.error || f, g = d.beforeSend || g, h = d.complete || h, i.success = function(b, d, f) {
                            try {
                                a.getConfig("debug") && (a.log.info("-------------------------------------------------"), a.log.info("Request:", i.url, i.data), a.log.info("Ajax received!", b), a.log.info("-------------------------------------------------")), c.sandbox.publish("AjaxSuccessHandler", b), e && e(b, d, f)
                            } catch (b) {
                                if (a.getConfig("devMode")) throw b;
                                a.log.logError(b)
                            }
                        }, i.error = function(b, c, d) {
                            a.getConfig("debug") && (a.log.info("-------------------------------------------------"), a.log.info("Request:", i.url, JSON.stringify(i.data)), a.log.info("Ajax error!", b), a.log.info("-------------------------------------------------")), f && f(b, c, d)
                        }, i.beforeSend = function(a, b) {
                            g && g(a, b)
                        }, i.complete = function(a, b) {
                            h && h(a, b)
                        }, b.ajax(i)
                    }, d.prototype.baseUrl = a.utils.flags().host, d.prototype.title = this.title, a.API = {
                        Request: d
                    }, a.api
                }), d("components/captcha/nls/ar-AE/labels", {
                    sCaptchaError: "لقد قمت بإدخال الأرقام خطأ, الرجاء المحاولة مره أخرى."
                }), d("components/captcha", ["core", "i18n!components/captcha/nls/labels", "extensions/api", "components/captcha/nls/ar-AE/labels"], function(a, b) {
                    var c = new a.Component("Captcha"),
                        d = c.dom,
                        e = "#imgCaptcha",
                        f = "#idCaptcha";
                    return c.run = function() {
                        this.base()
                    }, c.ready = function() {
                        this.drawCaptcha()
                    }, c.drawCaptcha = function(b) {
                        new a.API.Request("Action.php", "ajaxRemote", "POST").send({
                            serviceController: "Utils",
                            serviceMethod: "drawCaptcha",
                            beforeSend: function() {
                                d(e).attr("src", a.getConfig("STATICCOMMONHOST") + "ajax-loader.gif")
                            },
                            success: function(a) {
                                d(e).attr("src", a.captcha_img), d(f).val(a.captcha_id), b && b(a.captcha_id)
                            },
                            error: function() {
                                a.log.warn(new Error("failed to load captcha")), b && b(!1)
                            }
                        })
                    }, c.captchaError = function() {
                        return b.sCaptchaError
                    }, c.checkCaptcha = function(b, c) {
                        if (!b) return c(!1);
                        new a.API.Request("Action.php", "ajaxRemote", "POST").send({
                            serviceController: "Utils",
                            serviceMethod: "checkCaptcha",
                            data: {
                                captcha: b,
                                id_captcha: d(f).val()
                            },
                            success: function(a) {
                                "success" === a.result ? c && c(!0) : c && c(!1)
                            },
                            error: function() {
                                a.log.warn(new Error("failed to check captcha"))
                            }
                        })
                    }, c.captchaGeneration = function() {
                        var a = Math.ceil(10 * Math.random()),
                            b = Math.ceil(10 * Math.random());
                        return {
                            output: " " + a + " + " + b + " ",
                            result: a + b
                        }
                    }, c
                }),
                function() {
                    function a() {
                        if (!(this instanceof a)) return new a;
                        this.coerceType = {}, this.fieldType = b(g), this.fieldValidate = b(i), this.fieldFormat = b(h), this.defaultOptions = b(n), this.schema = {}
                    }
                    var b = function(a) {
                            if (null === a || "object" != typeof a) return a;
                            var c;
                            if (a instanceof Date) return c = new Date, c.setTime(a.getTime()), c;
                            if (a instanceof RegExp) return c = new RegExp(a);
                            if (a instanceof Array) {
                                c = [];
                                for (var d = 0, e = a.length; d < e; d++) c[d] = b(a[d]);
                                return c
                            }
                            if (a instanceof Object) {
                                c = {};
                                for (var f in a) a.hasOwnProperty(f) && (c[f] = b(a[f]));
                                return c
                            }
                            throw new Error("Unable to clone object!")
                        },
                        c = function(a) {
                            var c = a.length - 1,
                                d = a[c].key,
                                e = a.slice(0);
                            return e[c].object[d] = b(e[c].object[d]), e
                        },
                        e = function(a, b) {
                            var c = a.length - 1,
                                d = a[c].key;
                            b[c].object[d] = a[c].object[d]
                        },
                        f = {
                            type: !0,
                            not: !0,
                            anyOf: !0,
                            allOf: !0,
                            oneOf: !0,
                            $ref: !0,
                            $schema: !0,
                            id: !0,
                            exclusiveMaximum: !0,
                            exclusiveMininum: !0,
                            properties: !0,
                            patternProperties: !0,
                            additionalProperties: !0,
                            items: !0,
                            additionalItems: !0,
                            required: !0,
                            default: !0,
                            title: !0,
                            description: !0,
                            definitions: !0,
                            dependencies: !0
                        },
                        g = {
                            null: function(a) {
                                return null === a
                            },
                            string: function(a) {
                                return "string" == typeof a
                            },
                            boolean: function(a) {
                                return "boolean" == typeof a
                            },
                            number: function(a) {
                                return "number" == typeof a && !isNaN(a)
                            },
                            integer: function(a) {
                                return "number" == typeof a && a % 1 == 0
                            },
                            object: function(a) {
                                return a && "object" == typeof a && !Array.isArray(a)
                            },
                            array: function(a) {
                                return Array.isArray(a)
                            },
                            date: function(a) {
                                return a instanceof Date
                            }
                        },
                        h = {
                            alpha: function(a) {
                                return /^[a-zA-Z]+$/.test(a)
                            },
                            alphanumeric: function(a) {
                                return /^[a-zA-Z0-9]+$/.test(a)
                            },
                            identifier: function(a) {
                                return /^[-_a-zA-Z0-9]+$/.test(a)
                            },
                            hexadecimal: function(a) {
                                return /^[a-fA-F0-9]+$/.test(a)
                            },
                            numeric: function(a) {
                                return /^[0-9]+$/.test(a)
                            },
                            "date-time": function(a) {
                                return !isNaN(Date.parse(a)) && -1 === a.indexOf("/")
                            },
                            uppercase: function(a) {
                                return a === a.toUpperCase()
                            },
                            lowercase: function(a) {
                                return a === a.toLowerCase()
                            },
                            hostname: function(a) {
                                return a.length < 256 && /^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$/.test(a)
                            },
                            uri: function(a) {
                                return /[-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?/.test(a)
                            },
                            email: function(a) {
                                return /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/.test(a)
                            },
                            ipv4: function(a) {
                                return !!(/^(\d?\d?\d)\.(\d?\d?\d)\.(\d?\d?\d)\.(\d?\d?\d)$/.test(a) && a.split(".").sort()[3] <= 255)
                            },
                            ipv6: function(a) {
                                return /^((?=.*::)(?!.*::.+::)(::)?([\dA-F]{1,4}:(:|\b)|){5}|([\dA-F]{1,4}:){6})((([\dA-F]{1,4}((?!\3)::|:\b|$))|(?!\2\3)){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})$/.test(a)
                            }
                        },
                        i = {
                            readOnly: function(a, b) {
                                return !1
                            },
                            minimum: function(a, b, c) {
                                return !(a < b || c.exclusiveMinimum && a <= b)
                            },
                            maximum: function(a, b, c) {
                                return !(a > b || c.exclusiveMaximum && a >= b)
                            },
                            multipleOf: function(a, b) {
                                return a / b % 1 == 0 || "number" != typeof a
                            },
                            pattern: function(a, b) {
                                if ("string" != typeof a) return !0;
                                var c, d;
                                return "string" == typeof b ? c = b : (c = b[0], d = b[1]), new RegExp(c, d).test(a)
                            },
                            minLength: function(a, b) {
                                return a.length >= b || "string" != typeof a
                            },
                            maxLength: function(a, b) {
                                return a.length <= b || "string" != typeof a
                            },
                            minItems: function(a, b) {
                                return a.length >= b || !Array.isArray(a)
                            },
                            maxItems: function(a, b) {
                                return a.length <= b || !Array.isArray(a)
                            },
                            uniqueItems: function(a, b) {
                                for (var c, d = {}, e = 0, f = a.length; e < f; e++) {
                                    if (c = JSON.stringify(a[e]), d.hasOwnProperty(c)) return !1;
                                    d[c] = !0
                                }
                                return !0
                            },
                            minProperties: function(a, b) {
                                if ("object" != typeof a) return !0;
                                var c = 0;
                                for (var d in a) a.hasOwnProperty(d) && (c += 1);
                                return c >= b
                            },
                            maxProperties: function(a, b) {
                                if ("object" != typeof a) return !0;
                                var c = 0;
                                for (var d in a) a.hasOwnProperty(d) && (c += 1);
                                return c <= b
                            },
                            enum: function(a, b) {
                                var c, d, e;
                                if ("object" == typeof a) {
                                    for (e = JSON.stringify(a), c = 0, d = b.length; c < d; c++)
                                        if (e === JSON.stringify(b[c])) return !0
                                } else
                                    for (c = 0, d = b.length; c < d; c++)
                                        if (a === b[c]) return !0; return !1
                            }
                        },
                        j = function(a) {
                            return -1 === a.indexOf("://") ? a : a.split("#")[0]
                        },
                        k = function(a, b, c) {
                            var d, e, f, g;
                            if (-1 === (f = c.indexOf("#"))) return a.schema.hasOwnProperty(c) ? [a.schema[c]] : null;
                            if (f > 0)
                                if (g = c.substr(0, f), c = c.substr(f + 1), a.schema.hasOwnProperty(g)) b = [a.schema[g]];
                                else {
                                    if (!b || b[0].id !== g) return null;
                                    b = [b[0]]
                                } else {
                                if (!b) return null;
                                c = c.substr(1)
                            }
                            if ("" === c) return [b[0]];
                            if ("/" === c.charAt(0)) {
                                for (c = c.substr(1), d = b[0], e = c.split("/"); e.length > 0;) {
                                    if (!d.hasOwnProperty(e[0])) return null;
                                    d = d[e[0]], b.push(d), e.shift()
                                }
                                return b
                            }
                            return null
                        },
                        l = function(a, b) {
                            var c, d, e, f, g = a.length - 1,
                                h = /^(\d+)/.exec(b);
                            if (h) {
                                if (b = b.substr(h[0].length), (e = parseInt(h[1], 10)) < 0 || e > g) return;
                                if (f = a[g - e], "#" === b) return f.key
                            } else f = a[0];
                            if (d = f.object[f.key], "" === b) return d;
                            if ("/" === b.charAt(0)) {
                                for (b = b.substr(1), c = b.split("/"); c.length > 0;) {
                                    if (c[0] = c[0].replace(/~1/g, "/").replace(/~0/g, "~"), !d.hasOwnProperty(c[0])) return;
                                    d = d[c[0]], c.shift()
                                }
                                return d
                            }
                        },
                        m = function(a, b, d, g) {
                            var h, i, j, n, o, p, q, r, s, t, u = !1,
                                v = {},
                                w = b.length - 1,
                                x = b[w],
                                y = d.length - 1,
                                z = d[y].object,
                                A = d[y].key,
                                B = z[A];
                            if (x.hasOwnProperty("$ref")) return b = k(a, b, x.$ref), b ? m(a, b, d, g) : {
                                $ref: x.$ref
                            };
                            if (x.hasOwnProperty("type"))
                                if ("string" == typeof x.type) {
                                    if (g.useCoerce && a.coerceType.hasOwnProperty(x.type) && (B = z[A] = a.coerceType[x.type](B)), !a.fieldType[x.type](B)) return {
                                        type: x.type
                                    }
                                } else {
                                    for (u = !0, h = 0, i = x.type.length; h < i && u; h++) a.fieldType[x.type[h]](B) && (u = !1);
                                    if (u) return {
                                        type: x.type
                                    }
                                }
                            if (x.hasOwnProperty("allOf"))
                                for (h = 0, i = x.allOf.length; h < i; h++)
                                    if (r = m(a, b.concat(x.allOf[h]), d, g)) return r;
                            if (g.useCoerce || g.useDefault || g.removeAdditional) {
                                if (x.hasOwnProperty("oneOf")) {
                                    for (h = 0, i = x.oneOf.length, j = 0; h < i; h++)
                                        if (new_stack = c(d), r = m(a, b.concat(x.oneOf[h]), new_stack, g)) v = r;
                                        else {
                                            if ((j += 1) > 1) break;
                                            e(new_stack, d)
                                        }
                                    if (j > 1) return {
                                        oneOf: !0
                                    };
                                    if (j < 1) return v;
                                    v = {}
                                }
                                if (x.hasOwnProperty("anyOf")) {
                                    for (h = 0, i = x.anyOf.length; h < i; h++)
                                        if (new_stack = c(d), !(r = m(a, b.concat(x.anyOf[h]), new_stack, g))) {
                                            e(new_stack, d);
                                            break
                                        }
                                    if (r) return r
                                }
                                if (x.hasOwnProperty("not") && !(r = m(a, b.concat(x.not), c(d), g))) return {
                                    not: !0
                                }
                            } else {
                                if (x.hasOwnProperty("oneOf")) {
                                    for (h = 0, i = x.oneOf.length, j = 0; h < i; h++)
                                        if (r = m(a, b.concat(x.oneOf[h]), d, g)) v = r;
                                        else if ((j += 1) > 1) break;
                                    if (j > 1) return {
                                        oneOf: !0
                                    };
                                    if (j < 1) return v;
                                    v = {}
                                }
                                if (x.hasOwnProperty("anyOf")) {
                                    for (h = 0, i = x.anyOf.length; h < i && (r = m(a, b.concat(x.anyOf[h]), d, g)); h++);
                                    if (r) return r
                                }
                                if (x.hasOwnProperty("not") && !(r = m(a, b.concat(x.not), d, g))) return {
                                    not: !0
                                }
                            }
                            if (x.hasOwnProperty("dependencies"))
                                for (p in x.dependencies)
                                    if (x.dependencies.hasOwnProperty(p) && B.hasOwnProperty(p))
                                        if (Array.isArray(x.dependencies[p])) {
                                            for (h = 0, i = x.dependencies[p].length; h < i; h++)
                                                if (!B.hasOwnProperty(x.dependencies[p][h])) return {
                                                    dependencies: !0
                                                }
                                        } else if (r = m(a, b.concat(x.dependencies[p]), d, g)) return r;
                            if (Array.isArray(B)) {
                                if (x.hasOwnProperty("items"))
                                    if (Array.isArray(x.items)) {
                                        for (h = 0, i = x.items.length; h < i; h++) null !== (r = m(a, b.concat(x.items[h]), d.concat({
                                            object: B,
                                            key: h
                                        }), g)) && (v[h] = r, u = !0);
                                        if (B.length > i && x.hasOwnProperty("additionalItems"))
                                            if ("boolean" == typeof x.additionalItems) {
                                                if (!x.additionalItems) return {
                                                    additionalItems: !0
                                                }
                                            } else
                                                for (h = i, i = B.length; h < i; h++) null !== (r = m(a, b.concat(x.additionalItems), d.concat({
                                                    object: B,
                                                    key: h
                                                }), g)) && (v[h] = r, u = !0)
                                    } else
                                        for (h = 0, i = B.length; h < i; h++) null !== (r = m(a, b.concat(x.items), d.concat({
                                            object: B,
                                            key: h
                                        }), g)) && (v[h] = r, u = !0);
                                else if (x.hasOwnProperty("additionalItems") && "boolean" != typeof x.additionalItems)
                                    for (h = 0, i = B.length; h < i; h++) null !== (r = m(a, b.concat(x.additionalItems), d.concat({
                                        object: B,
                                        key: h
                                    }), g)) && (v[h] = r, u = !0);
                                if (u) return {
                                    schema: v
                                }
                            } else {
                                s = [], v = {};
                                for (p in B) B.hasOwnProperty(p) && s.push(p);
                                if (g.checkRequired && x.required)
                                    for (h = 0, i = x.required.length; h < i; h++) B.hasOwnProperty(x.required[h]) || (v[x.required[h]] = {
                                        required: !0
                                    }, u = !0);
                                if (n = x.hasOwnProperty("properties"), o = x.hasOwnProperty("patternProperties"), n || o)
                                    for (h = s.length; h--;) {
                                        if (t = !1, n && x.properties.hasOwnProperty(s[h]) && (t = !0, null !== (r = m(a, b.concat(x.properties[s[h]]), d.concat({
                                                object: B,
                                                key: s[h]
                                            }), g)) && (v[s[h]] = r, u = !0)), o)
                                            for (p in x.patternProperties) x.patternProperties.hasOwnProperty(p) && s[h].match(p) && (t = !0, null !== (r = m(a, b.concat(x.patternProperties[p]), d.concat({
                                                object: B,
                                                key: s[h]
                                            }), g)) && (v[s[h]] = r, u = !0));
                                        t && s.splice(h, 1)
                                    }
                                if (g.useDefault && n && !u)
                                    for (p in x.properties) x.properties.hasOwnProperty(p) && !B.hasOwnProperty(p) && x.properties[p].hasOwnProperty("default") && (B[p] = x.properties[p].default);
                                if (g.removeAdditional && n && !0 !== x.additionalProperties && "object" != typeof x.additionalProperties)
                                    for (h = 0, i = s.length; h < i; h++) delete B[s[h]];
                                else if (x.hasOwnProperty("additionalProperties"))
                                    if ("boolean" == typeof x.additionalProperties) {
                                        if (!x.additionalProperties)
                                            for (h = 0, i = s.length; h < i; h++) v[s[h]] = {
                                                additional: !0
                                            }, u = !0
                                    } else
                                        for (h = 0, i = s.length; h < i; h++) null !== (r = m(a, b.concat(x.additionalProperties), d.concat({
                                            object: B,
                                            key: s[h]
                                        }), g)) && (v[s[h]] = r, u = !0);
                                if (u) return {
                                    schema: v
                                }
                            }
                            for (q in x) x.hasOwnProperty(q) && !f.hasOwnProperty(q) && ("format" === q ? a.fieldFormat.hasOwnProperty(x[q]) && !a.fieldFormat[x[q]](B, x, d, g) && (v[q] = !0, u = !0) : a.fieldValidate.hasOwnProperty(q) && !a.fieldValidate[q](B, x[q].hasOwnProperty("$data") ? l(d, x[q].$data) : x[q], x, d, g) && (v[q] = !0, u = !0));
                            return u ? v : null
                        },
                        n = {
                            useDefault: !1,
                            useCoerce: !1,
                            checkRequired: !0,
                            removeAdditional: !1
                        };
                    a.prototype = {
                        validate: function(a, b, c) {
                            var d = [a],
                                e = null,
                                f = [{
                                    object: {
                                        __root__: b
                                    },
                                    key: "__root__"
                                }];
                            if ("string" == typeof a && !(d = k(this, null, a))) throw new Error("jjv: could not find schema '" + a + "'.");
                            if (c)
                                for (var g in this.defaultOptions) this.defaultOptions.hasOwnProperty(g) && !c.hasOwnProperty(g) && (c[g] = this.defaultOptions[g]);
                            else c = this.defaultOptions;
                            return e = m(this, d, f, c), e ? {
                                validation: e.hasOwnProperty("schema") ? e.schema : e
                            } : null
                        },
                        resolveRef: function(a, b) {
                            return k(this, a, b)
                        },
                        addType: function(a, b) {
                            this.fieldType[a] = b
                        },
                        addTypeCoercion: function(a, b) {
                            this.coerceType[a] = b
                        },
                        addCheck: function(a, b) {
                            this.fieldValidate[a] = b
                        },
                        addFormat: function(a, b) {
                            this.fieldFormat[a] = b
                        },
                        addSchema: function(a, b) {
                            if (!b && a && (b = a, a = void 0), b.hasOwnProperty("id") && "string" == typeof b.id && b.id !== a) {
                                if ("/" === b.id.charAt(0)) throw new Error("jjv: schema id's starting with / are invalid.");
                                this.schema[j(b.id)] = b
                            } else if (!a) throw new Error("jjv: schema needs either a name or id attribute.");
                            a && (this.schema[j(a)] = b)
                        }
                    }, "undefined" != typeof module && void 0 !== module.exports ? module.exports = a : "function" == typeof d && d.amd ? d("jjv", [], function() {
                        return a
                    }) : window.jjv = a
                }(), d("extensions/validation", ["core", "jjv"], function(a, b) {
                    function c() {
                        return h || (h = new b), h
                    }

                    function d(a, b) {
                        c().addType(a, b)
                    }

                    function e(a, b) {
                        c().addCheck(a, b)
                    }

                    function f(a, b) {
                        c().addSchema(a, b)
                    }

                    function g(a, b) {
                        return c().validate(a, b)
                    }
                    var h, i = a.dom;
                    d("date", function(a) {
                        return /^(0[1-9]|[1-2][0-9]|3[0-1]|[1-9])\/(0[1-9]|1[0-2]|[1-9])\/[0-9]{4}$/.test(a)
                    }), d("PositiveNumber", function(a) {
                        return !!/^\d+$/.test(a) && parseInt(a) > 0
                    }), d("file", function(a) {
                        try {
                            return a && (a.files && 0 === a.files.length || a.files.length && a.files[0].size && a.files[0].type)
                        } catch (a) {
                            return !1
                        }
                    }), d("phoneNumber", function(a) {
                        try {
                            var b = a;
                            i("#country_code_mobile_label").length && "" !== i("#country_code_mobile_label").text() && (b = "+" === i("#country_code_mobile_label").text()[0] ? i("#country_code_mobile_label").text() + a : "+" + i("#country_code_mobile_label").text() + a);
                            var c = /^\+?\d+$/.test(b),
                                d = b.substring(4, 6),
                                e = b.substring(3, 5),
                                f = b.substring(4, 5),
                                g = 7 === b.substring(6, b.length).length,
                                h = 8 === b.substring(5, b.length).length,
                                j = 7 === b.substring(5, b.length).length,
                                k = "50" === d || "58" === d || "51" === d || "52" === d || "54" === d || "55" === d || "56" === d,
                                l = "50" === d || "51" === d || "52" === d || "53" === d || "54" === d || "55" === d || "56" === d || "57" === d || "58" === d || "59" === d,
                                m = "10" === e || "11" === e || "12" === e,
                                n = "3" === f,
                                o = "7" === f || "9" === f,
                                p = "3" === f || "5" === f || "6" === f || "7" === f,
                                q = "971" === b.substring(1, 4),
                                r = "965" === b.substring(1, 4),
                                s = "966" === b.substring(1, 4),
                                t = "20" === b.substring(1, 3),
                                u = "973" === b.substring(1, 4),
                                v = "968" === b.substring(1, 4),
                                w = "974" === b.substring(1, 4),
                                x = g && k && q,
                                y = h && m && t,
                                z = !1,
                                A = u && j && n,
                                B = v && j && o,
                                C = w && j && p,
                                D = g && l && s;
                            if (r) {
                                var E = b.substring(4, b.length),
                                    F = /(?:5(?:[05]\d{2}|1[0-7]\d|2(?:22|5[25])|66\d)|6(?:0[034679]\d|222|5[015-9]\d|6\d{2}|7[067]\d|9[0369]\d)|9(?:0[09]\d|22\d|4[01479]\d|55\d|6[0679]\d|[79]\d{2}|8[057-9]\d))\d{4}/g,
                                    G = E.match(F);
                                G && null !== G && (z = !0)
                            }
                            return !(!c || !(D || x || z || y || A || C || B))
                        } catch (a) {
                            return !1
                        }
                    }), e("size", function(a, b) {
                        try {
                            return 0 === a.files.length || a.files[0].size < b
                        } catch (a) {
                            return !1
                        }
                    }), e("mimetype", function(b, c) {
                        if (0 === b.files.length) return !0;
                        var d = c.split(",");
                        return -1 !== a.utils.inArray(b.files[0].type, d)
                    }), e("qitafdeposit", function(a, b) {
                        return parseInt(a) <= parseInt(b)
                    }), d("name", function(a) {
                        return /^[a-z\u0621-\u064A \-\u00fc\u00f6\u00e4\u00dc\u00d6\u00c4\u00df]+$/i.test(a)
                    }), d("sadadalias", function(a) {
                        return /^[a-zA-Z0-9-_@.]*[a-zA-Z][a-zA-Z0-9-_@.]+$/i.test(a)
                    }), d("hmflname", function(a) {
                        if (-1 !== a.indexOf("..") || -1 !== a.indexOf("__") || -1 !== a.indexOf("--") || !a) return !1;
                        var b = /[._-]$/;
                        return null === a.match(b) && (b = /\._|\.-|\.'|_\.|_-|_'|-\.|-_|-'|'_|'-|'\./, null === a.match(b) && /^[a-z0-9\u0621-\u064A][a-z0-9\u0621-\u064A\s\-\_\.\']*$/i.test(a))
                    }), d("digits", function(a) {
                        return /^\d+$/.test(a)
                    }), d("nonZeroString", function(a) {
                        return !("0" === a || !a.length)
                    }), e("equalsTo", function(a, b) {
                        return "object" == typeof b ? a === i(b).val() : parseInt(a) === b
                    }), e("checkEmail", function(a, b) {
                        return b
                    }), e("checkAjaxResponse", function(a, b) {
                        return b
                    }), d("sq_flname_regx", function(a) {
                        return -1 === a.indexOf("=") && -1 === a.indexOf("'") && (a.length < 2 || /^[A-Za-z\d='\u0621-\u064A][A-Za-z\d='\u0621-\u064A\ ]+$/.test(a))
                    }), d("pseudonymname", function(a) {
                        if (!(a = a.toLowerCase()) || -1 !== a.indexOf("..") || -1 !== a.indexOf("__") || -1 !== a.indexOf("--") || -1 !== a.indexOf("'") || -1 !== a.indexOf("dod") || -1 !== a.indexOf("www") || -1 !== a.indexOf("souq")) return !1;
                        var b = /[._-]$/;
                        return null === a.match(b) && (b = /(\.|\-|\_|dot)+(com|edu|gov|mil|me|net|org|biz|info|us|ca|uk|co|kw|eg|ae|sa)/, null === a.match(b) && (b = /\._|\.-|\.'|_\.|_-|_'|-\.|-_|-'|'_|'-|'\./, null === a.match(b) && (b = /^\d+$/, null === a.match(b) && /^[a-z0-9\u0621-\u064A][a-z0-9\u0621-\u064A\s\-\_\.\']*$/i.test(a))))
                    }), d("radio", function(a) {
                        return "" !== a
                    }), a.validation = {
                        addValidationType: d,
                        addValidationCheck: e,
                        addSchema: f,
                        validate: g
                    }
                }), d("components/form", ["core", "components/captcha", "extensions/validation"], function(a, b) {
                    function c(a, b) {
                        a.value.length > b && (a.value = a.value.substring(0, b))
                    }

                    function d() {
                        for (var a = arguments[0], b = 0; b < arguments.length - 1; b += 1) {
                            var c = new RegExp("\\{" + b + "\\}", "gm");
                            a = a.replace(c, arguments[b + 1])
                        }
                        return a
                    }
                    var e = new a.Component("Form", {
                        url: "",
                        action: "",
                        formSelector: "",
                        blockSelector: "",
                        emailSelector: "",
                        captchaSelector: null,
                        data: null,
                        formChangeHandler: null,
                        submitAjax: null,
                        scrollOnValidation: !0,
                        translations: null,
                        sortEnabled: !1,
                        disableTextAreaLimit: !1
                    });
                    e.defaultData = {}, e.currentSchema = "empty";
                    var f = e.dom,
                        g = {},
                        h = -1,
                        i = "error",
                        j = "no_error",
                        k = !1;
                    return e.run = function(a) {
                        this.base(a), this.options.captchaSelector && b.run();
                        var c = {
                            type: "object",
                            properties: {},
                            required: []
                        };
                        this.addSchema("empty", c), this.defaultData = this.readFormData()
                    }, e.ready = function() {
                        this.enable(), this.options.captchaSelector && f(this.options.reloadCaptchaSelector).click(function() {
                            return b.drawCaptcha(), !1
                        });
                        var d = f(this.options.formSelector),
                            i = d;
                        this.options.blockSelector && (i = f(this.options.blockSelector)), d.submit(function(b) {
                            var c = b.currentTarget;
                            return b.preventDefault(), this.checkErrorsAndCaptcha(function(b) {
                                if (b) e.sandbox.publish("Validation:Error:" + this.currentSchema, b);
                                else if (e.sandbox.publish("Form:" + this.options.formSelector + ":beforeSubmit"), this.options.submitAjax)
                                    if (this.options.url && this.options.action) {
                                        var f = new a.API.Request(this.options.url, this.options.action, d.attr("method")),
                                            h = {};
                                        if (this.options.data) {
                                            if ("object" == typeof this.options.data) f.data = this.options.data;
                                            else if ("object" != typeof(h = this.options.data())) return d.attr("action", this.options.url), c.submit(), !1
                                        } else {
                                            a.utils.map(d.serializeArray(), function(a) {
                                                h[a.name] = a.value
                                            });
                                            for (var j in g[this.currentSchema].properties) void 0 === h[j] && (h[j] = "");
                                            f.data = h
                                        }
                                        f.data = h, f.send({
                                            beforeSend: function() {
                                                i.block()
                                            },
                                            complete: function() {
                                                i.unblock()
                                            },
                                            success: function(b) {
                                                e.sandbox.publish("Form:" + this.options.formSelector + ":Success"), this.options.submitAjax.call(this, !0, b), a.log.log("success:", b)
                                            }.bind(this),
                                            error: function(b) {
                                                b && (e.sandbox.publish("Form:" + this.options.formSelector + ":Error"), this.options.submitAjax.call(this, !1, {
                                                    message: b.responseText
                                                }), a.log.warn(new Error("failed to submit Ajax : " + b.responseText)))
                                            }.bind(this)
                                        })
                                    } else this.options.submitAjax.call(this);
                                else c.submit()
                            }.bind(this)), !1
                        }.bind(this)), f(this.options.formSelector).find("input,select,textarea").bind("change", function(a, b) {
                            var c = !0;
                            b && (b.bCheckError || (c = !1)), this.options.formChangeHandler && this.options.formChangeHandler.call(a.currentTarget, a), -1 !== h && clearTimeout(h), c && (h = "captcha" === a.currentTarget.id ? setTimeout(function() {
                                this.validateCaptcha(null), h = -1
                            }.bind(this), 100) : "radio" === a.currentTarget.type ? setTimeout(function() {
                                this.checkErrors(a.currentTarget.name), h = -1
                            }.bind(this), 100) : setTimeout(function() {
                                this.checkErrors(a.currentTarget.id), h = -1
                            }.bind(this), 100))
                        }.bind(this));
                        var j = this;
                        this.options.textSelector && f(this.options.textSelector).length && f(this.options.textSelector).on("keyup keydown", function() {
                            j.options.disableTextAreaLimit || c(this, 500)
                        }), this.options.textSelector && f(this.options.emailSelector) && f(this.options.emailSelector).on("keyup keydown", function() {
                            c(this, 75), this.value !== this.value.replace(/\s+/g, "") && (this.value = this.value.replace(/\s+/g, ""))
                        })
                    }, e.reload = function() {
                        this.ready(document, !0)
                    }, e.open = function() {
                        f(this.options.formSelector).fadeIn(300), f(this.options.formSelector).children().fadeIn(300), this.reset(), this.refreshForm(), this.options.scrollOnValidation && a.scrollToElement(f(this.options.formSelector))
                    }, e.close = function() {
                        f(this.options.formSelector).fadeOut(300), f(this.options.formSelector).children().fadeOut(300)
                    }, e.showSubmit = function() {
                        f(this.options.formSelector).find(this.options.submitSelector).show().removeClass("hide")
                    }, e.hideSubmit = function() {
                        f(this.options.formSelector).find(this.options.submitSelector).hide().addClass("hide")
                    }, e.disable = function() {
                        f(this.options.formSelector).find(this.options.submitSelector).attr("disabled", !0)
                    }, e.enable = function() {
                        f(this.options.formSelector).find(this.options.submitSelector).attr("disabled", !1), f(this.options.formSelector).find(this.options.submitSelector).removeAttr("disabled")
                    }, e.reset = function(a, b) {
                        function c(b) {
                            d.find("input:radio[name=" + b + "]").each(function() {
                                this.value === a[b] ? (this.checked = !0, f(this).attr("checked", "checked")) : (this.checked = !1, f(this).removeAttr("checked"))
                            })
                        }
                        a = a || this.defaultData;
                        var d = f(this.options.formSelector);
                        for (var e in g[this.currentSchema].properties)
                            if (g[this.currentSchema].properties.hasOwnProperty(e)) {
                                var h = d.find("#" + e),
                                    i = h.is('[type="hidden"]');
                                if (b && i) continue;
                                h.length ? (h.val(a[e]), h.is('[type="checkbox"]') ? parseInt(a[e]) ? (h.attr("checked", "checked"), h.prop("checked", !0)) : (h.attr("checked", ""), h.prop("checked", !1)) : d.find("input:radio[name=" + e + "]").length && c(e)) : d.find("input:radio[name=" + e + "]").length && c(e)
                            }
                    }, e.makeCurrentDataDefault = function() {
                        this.defaultData = this.readFormData()
                    }, e.addSchema = function(b, c) {
                        g.hasOwnProperty(b) || a.utils.isObject(g[b]) ? a.log.warn("Schema " + b + " already added") : (g[b] = c, a.validation.addSchema(b, c))
                    }, e.getSchema = function(a) {
                        return g[a]
                    }, e.addRequiredField = function(a) {
                        g[this.currentSchema].required || (g[this.currentSchema].required = []), -1 === g[this.currentSchema].required.indexOf(a) && g[this.currentSchema].required.push(a), this.selectRequiredSpan(a).show()
                    }, e.updateProperty = function(a, b, c) {
                        g[this.currentSchema].properties[a] = b, this.refreshForm(), c || this.refreshForm()
                    }, e.removeRequiredField = function(a) {
                        var b = g[this.currentSchema].required.indexOf(a); - 1 !== b && g[this.currentSchema].required.splice(b, 1), this.selectRequiredSpan(a).hide(), this.selectErrorSpan(a).hide(), this.selectLabelSpan(a).removeClass(i), f("#error_message_" + a).addClass("hide")
                    }, e.setCurrentSchema = function(b) {
                        if (this.currentSchema = b, !g[b]) return void a.log.warn(new Error("failed to get schema for :" + b));
                        a.getConfig("devMode") && a.log.log("Active Schema:", b, g[b]), this.refreshForm()
                    }, e.refreshForm = function() {
                        var a = g[this.currentSchema].properties;
                        for (var b in a)
                            if (a.hasOwnProperty(b)) {
                                var c = this.selectField(b),
                                    d = c.is('[type="hidden"]'),
                                    e = g[this.currentSchema].required && -1 !== g[this.currentSchema].required.indexOf(b);
                                g[this.currentSchema].unused && -1 !== g[this.currentSchema].unused.indexOf(b) ? (c.closest("li").removeClass().hide(), c.hide(), d && !e || (this.selectLabelSpan(b).hide(), this.selectErrorSpan(b).hide())) : d && !e || (c.closest("li").show(), c.parent().show(), c.show(), this.selectLabelSpan(b).show(), this.selectErrorSpan(b).show(),
                                    g[this.currentSchema].required && -1 !== g[this.currentSchema].required.indexOf(b) ? this.selectRequiredSpan(b).show() : this.selectRequiredSpan(b).hide()), d && !e || this.handleFieldError(b, null), this.sortEnabled && this.indexingForm()
                            }
                    }, e.indexingForm = function() {
                        var a = ".shipping-new-addresses";
                        f(a).children("li").sort(function(a, b) {
                            return +a.dataset.order - +b.dataset.order
                        }).appendTo(f(a)), this.sortForm()
                    }, e.sortForm = function() {
                        var a = [],
                            b = this.aOptionalFields || [],
                            c = g[this.currentSchema].properties;
                        for (var d in c)
                            if (c.hasOwnProperty(d)) {
                                var e = this.selectField(d),
                                    h = e.is('[disabled="disabled"]'),
                                    i = e.closest("li").hasClass("field-changable"),
                                    j = g[this.currentSchema].unused && -1 !== g[this.currentSchema].unused.indexOf(d);
                                if (!h && !i && !j) {
                                    var k = !1;
                                    (g[this.currentSchema].required && -1 !== g[this.currentSchema].required.indexOf(d) || -1 !== b.indexOf(d)) && (k = !0), k ? (e.closest("li").removeClass("notRequired"), e.closest("li").removeClass("hide"), -1 !== g[this.currentSchema].required.indexOf(d) && e.closest("li").addClass("isRequired")) : e.closest("li").removeClass("isRequired").addClass("notRequired").addClass("hide")
                                }
                            }
                        var l = f("li.notRequired");
                        a.push(l.detach()), f(".from-action").before(a)
                    }, e.handleErrors = function(b) {
                        b && a.getConfig("devMode") && a.log.log("errors:", b.validation);
                        var c, d = !1;
                        for (var e in g[this.currentSchema].properties) this.handleFieldError(e, b) && !d && (d = !0, c = e);
                        d && this.options.scrollOnValidation && (a.scrollToElement(f("#" + c)), f("#" + c).focus())
                    }, e.checkErrors = function(b) {
                        if (a.getConfig("devMode") && a.log.log("Form Data", this.readFormData()), g[this.currentSchema].properties[b]) {
                            var c = a.validation.validate(this.currentSchema, this.readFormData());
                            if (c && a.getConfig("devMode") && a.log.log("Form errors", c), b ? this.handleFieldError(b, c, !0) : this.handleErrors(c), c) return !0
                        }
                        return !1
                    }, e.checkErrorsAndCaptcha = function(c) {
                        if (!this.options.captchaSelector) {
                            var d = a.validation.validate(this.currentSchema, this.readFormData());
                            return this.handleErrors(d), void(c && c(d))
                        }
                        b.checkCaptcha(f(this.options.captchaSelector).val(), function(d) {
                            var e = a.validation.validate(this.currentSchema, this.readFormData());
                            d ? (k = !1, a.getConfig("devMode") && a.log.log("Captcha is Valid!")) : (k = !0, b.drawCaptcha(), f("#captcha").val(""), e || (e = {}), e.validation = a.utils.extend({
                                captcha: {
                                    format: !0
                                }
                            }, e.validation), a.getConfig("devMode") && a.log.log("Captcha is invalid!")), this.handleErrors(e), c && c(e)
                        }.bind(this))
                    }, e.checkFeildErrors = function(b, c) {
                        var d = a.validation.validate(this.currentSchema, this.readFormData()),
                            e = this.handleFieldError(b, d);
                        return c && c(d), e || !1
                    }, e.validateCaptcha = function(c) {
                        b.checkCaptcha(f(this.options.captchaSelector).val(), function(b) {
                            var d;
                            b ? (k = !1, a.getConfig("devMode") && a.log.log("Captcha is Valid!")) : (k = !0, d || (d = {}), d.validation = a.utils.extend({
                                captcha: {
                                    format: !0
                                }
                            }, d.validation)), this.handleFieldError("captcha", d), c && c(d)
                        }.bind(this))
                    }, e.readFormData = function() {
                        var a = {},
                            b = f(this.options.formSelector);
                        for (var c in g[this.currentSchema].properties) g[this.currentSchema].unused && -1 === g[this.currentSchema].unused.indexOf(c) && g[this.currentSchema].properties[c] && "file" === g[this.currentSchema].properties[c].type ? a[c] = b.find("#" + c).get(0) : b.find("#" + c).val() ? a[c] = b.find("#" + c).val() : b.find("input:radio[name=" + c + "]:checked").val() && (a[c] = b.find("input:radio[name=" + c + "]:checked").val());
                        return a
                    }, e.handleFieldError = function(a, c, e) {
                        var h = "#" + a,
                            l = "#error_message_" + a,
                            m = f(h).is('[type="hidden"]'),
                            n = g[this.currentSchema].required && -1 !== g[this.currentSchema].required.indexOf(a),
                            o = "";
                        g[this.currentSchema].properties[a] && (o = g[this.currentSchema].properties[a].type);
                        var p = g[this.currentSchema].map && g[this.currentSchema].map[a];
                        if (p && !e)
                            for (var q in g[this.currentSchema].map)
                                if (g[this.currentSchema].map.hasOwnProperty(q)) {
                                    var r = g[this.currentSchema].map[q];
                                    if (r === p) {
                                        var s = c && c.validation && c.validation[q];
                                        if (s) {
                                            a = q;
                                            break
                                        }
                                    }
                                }
                        return c && c.validation && c.validation[a] ? (f(h).addClass("error"), m && !n || (this.selectErrorSpan(a).removeClass(j).addClass(i).show().css("display", "block"), this.selectLabelSpan(a).addClass(i), f(l).text(" - "), this.selectField(a).hasClass(i) && this.selectField(a).is("select") && this.selectField(a).closest("li").find(".select2-selection.select2-selection--single").css("border-color", "#ff4538"), c.validation[a].required ? (this.selectErrorSpan(a).text(this.options.translations.required), f(l).addClass("hide")) : "hmflname" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.name), f(l).text(" - " + this.options.translations.name), f(l).removeClass("hide")) : "pseudonymname" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.pseudonym), f(l).text(" - " + this.options.translations.pseudonym), f(l).removeClass("hide")) : "digits" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.digits), f(l).text(" - " + this.options.translations.digits), f(l).removeClass("hide")) : "sq_flname_regx" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.sq_flname_regx), f(l).text(" - " + this.options.translations.sq_flname_regx), f(l).removeClass("hide")) : "PositiveNumber" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.PositiveNumber), f(l).text(" - " + this.options.translations.PositiveNumber), f(l).removeClass("hide")) : "date" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.date), f(l).text(" - " + this.options.translations.date), f(l).removeClass("hide")) : "nonZeroString" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.nonZeroString), f(l).text(" - " + this.options.translations.nonZeroString), f(l).removeClass("hide")) : "name" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.name), f(l).text(" - " + this.options.translations.name), f(l).removeClass("hide")) : "sadadalias" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.name), f(l).text(" - " + this.options.translations.name), f(l).removeClass("hide")) : "file" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.required), f(l).text(" - " + this.options.translations.required), f(l).removeClass("hide")) : "phoneNumber" === c.validation[a].type ? (this.selectErrorSpan(a).text(this.options.translations.phoneNumber), f(l).text(" - " + this.options.translations.phoneNumber), f(l).removeClass("hide")) : c.validation[a].minLength ? "digits" === o && this.options.translations.digitsMinLength ? (this.selectErrorSpan(a).text(d(this.options.translations.digitsMinLength, g[this.currentSchema].properties[a].minLength)), f(l).text(" - " + d(this.options.translations.digitsMinLength, g[this.currentSchema].properties[a].minLength)), f(l).removeClass("hide")) : (this.selectErrorSpan(a).text(d(this.options.translations.minLength, g[this.currentSchema].properties[a].minLength)), f(l).text(" - " + d(this.options.translations.minLength, g[this.currentSchema].properties[a].minLength)), f(l).removeClass("hide")) : c.validation[a].maxLength ? "digits" === o && this.options.translations.digitsMaxLength ? (this.selectErrorSpan(a).text(d(this.options.translations.digitsMaxLength, g[this.currentSchema].properties[a].maxLength)), f(l).text(" - " + d(this.options.translations.digitsMaxLength, g[this.currentSchema].properties[a].maxLength)), f(l).removeClass("hide")) : (this.selectErrorSpan(a).text(d(this.options.translations.maxLength, g[this.currentSchema].properties[a].maxLength)), f(l).text(" - " + d(this.options.translations.maxLength, g[this.currentSchema].properties[a].maxLength)), f(l).removeClass("hide")) : c.validation[a].equalsTo ? (this.selectErrorSpan(a).text(this.options.translations.equalsTo), f(l).text(" - " + this.options.translations.equalsTo), f(l).removeClass("hide")) : c.validation[a].size ? (this.selectErrorSpan(a).text(this.options.translations.maxSize), f(l).text(" - " + this.options.translations.maxSize), f(l).removeClass("hide")) : c.validation[a].mimetype ? (this.selectErrorSpan(a).text(this.options.translations.mimeType), f(l).text(" - " + this.options.translations.mimeType), f(l).removeClass("hide")) : c.validation[a].qitafdeposit ? (this.selectErrorSpan(a).text(this.options.translations.qitafdeposit), f(l).text(" - " + this.options.translations.qitafdeposit), f(l).removeClass("hide")) : c.validation[a].format ? "email" === a || "pm_email" === a ? (this.selectErrorSpan(a).text(this.options.translations.email), f(l).text(" - " + this.options.translations.email), f(l).removeClass("hide")) : "captcha" === a ? (this.selectErrorSpan(a).text(b.captchaError()), f(l).text(" - " + b.captchaError()), f(l).removeClass("hide")) : (this.selectErrorSpan(a).text(this.options.translations.format), f(l).text(" - " + this.options.translations.format), f(l).removeClass("hide")) : c.validation[a].checkEmail ? (this.selectErrorSpan(a).text(this.options.translations.checkEmail), f(l).text(" - " + this.options.translations.checkEmail), f(l).removeClass("hide")) : c.validation[a].checkAjaxResponse ? (this.selectErrorSpan(a).text(this.options.translations.email), f(l).text(" - " + this.options.translations.email), f(l).removeClass("hide")) : c.validation[a].enum && ("gender" === a ? (this.selectErrorSpan(a).text(this.options.translations.gender), f(l).text(" - " + this.options.translations.gender), f(l).removeClass("hide")) : (this.selectErrorSpan(a).text(this.options.translations.enum), f(l).text(" - " + this.options.translations.enum), f(l).removeClass("hide")))), !0) : (this.selectField(a).hasClass(i) && this.selectField(a).is("select") && this.selectField(a).closest("li").find(".select2-selection.select2-selection--single").css("border-color", ""), "captcha" === a && k || !g[this.currentSchema].properties[a] || (f(h).removeClass("error"), m && !n || (this.selectLabelSpan(a).removeClass(i), f(l).addClass("hide"), this.selectErrorSpan(a).removeClass(i).addClass(j).hide())), !1)
                    }, e.selectField = function(b) {
                        var c = "";
                        g[this.currentSchema].properties[b] && (c = g[this.currentSchema].properties[b].type);
                        var d = f(this.options.formSelector),
                            e = "#" + b;
                        "radio" === c && (e = "input:radio[name=" + b + "]:first");
                        var h = d.find(e);
                        return h.length || (h = d.find(e)), h.length || (h = d.find(e)), h.length || a.getConfig("devMode") && a.log.warn("no field found for : ", e), h
                    }, e.selectErrorSpan = function(b) {
                        var c = "";
                        g[this.currentSchema].properties[b] && (c = g[this.currentSchema].properties[b].type);
                        var d = f(this.options.formSelector),
                            e = "#" + b,
                            h = "small." + i + ",small." + j;
                        "radio" === c && (e = "input:radio[name=" + b + "]");
                        var k = d.find(e).siblings(h);
                        return k.length || (k = d.find(e).parent().siblings(h)), k.length || (k = d.find(e).parent().parent().next().find(h)), k.length || a.getConfig("devMode") && a.log.warn("no error field found for : ", e), k
                    }, e.selectLabelSpan = function(b) {
                        var c = "";
                        g[this.currentSchema].properties[b] && (c = g[this.currentSchema].properties[b].type);
                        var d = f(this.options.formSelector),
                            e = "label[for=" + b + "]",
                            h = "#" + b;
                        g[this.currentSchema].map && g[this.currentSchema].map[b] && (e = "label[for=" + g[this.currentSchema].map[b] + "]"), "radio" === c && (h = "input:radio[name=" + b + "]");
                        var i = d.find(h).siblings(e);
                        return i.length || (i = d.find(h).parent().siblings(e)), i.length || (i = d.find(h).parent().parent().next().find(e)), i.length || (i = d.find(h).parent().parent().parent().find(e)), i.length || a.getConfig("devMode") && a.log.warn("no label field found for :", b), i
                    }, e.selectRequiredSpan = function(b) {
                        var c = this.selectLabelSpan(b),
                            d = c.find("small");
                        if (d.length) {
                            if (d.length > 1) return d.first()
                        } else a.getConfig("devMode") && a.log.warn("no required field found for ", b);
                        return d
                    }, e
                });
            var c = b();
            if (void 0 === c) throw new Error((a || "Component") + " Build Error : no identifer detected!");
            if (window.hasDefine) {
                if (window.hasAlmondJS) return window.require(c)
            } else require(c)
        })
    }(), define("components/mobileVerificaion", ["souq", "components/form", "jquery", "extensions/tracking"], function(a, b, c) {
        function d() {
            n.find(".content").addClass("loading"), k.ajax({
                url: a.utils.flags().host + "/Action.php",
                type: "POST",
                dataType: "json",
                data: {
                    action: "ajaxRemote",
                    ajaxrequest: "Utils_drawCaptcha"
                },
                success: function(a) {
                    k("#imgCaptcha").attr("src", a.captcha_img), k("#id_captcha").val(a.captcha_id), k("#captchaVal").val(""), n.find(".content").removeClass("loading")
                }
            })
        }

        function e() {
            var a = "body";
            k("#confirmOneClickBuyReveal").foundation("reveal", "close");
            var b = k("#selectedPaymentMethodInPlace").val();
            if ("creditcard" === b) {
                var c = k("#CvvInput").val().replace(".", "");
                if (window.isNaN(c) || 3 !== c.length) return k("#CvvInput").focus().addClass("error"), !1;
                k("#CVC").val(c), k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#RevealCvvInput").removeClass("error");
                var d = k("#RevealCvvInput").val();
                k("#CvvInput").val(d), k("#buy_now_button_source").val("in_place"), f(b, "in_place"), k(a).block(), k("#buyNowForm").submit()
            } else f("cash_on_delivery", "in_place"), k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#buy_now_button_source").val("in_place"), k(a).block(), k("#buyNowForm").submit()
        }

        function f(b, c) {
            var d = a.log,
                e = k("#id_unit").val(),
                f = k("#__u_p").val();
            o && o.tl ? (o.linkTrackVars = "eVar12,products,events,prop30", o.linkTrackEvents = o.events = "event18", o.eVar12 = b, o.prop30 = "One-Click Checkout " + c + "(" + b + ")", o.products = ";" + e + ";1;" + f, o.tl(this, "o", "One-Click Checkout Tracking"), delete o.linkTrackVars, delete o.linkTrackEvents, delete o.eVar12, delete o.prop30, delete o.products) : d.logError(new Error("Omniture (s) not defined!"))
        }

        function g() {
            k(this).attr("disabled", "disabled"), k("form#save_use_address").submit()
        }
        var h, i, j = new a.Component("mobileVerificaion"),
            k = c,
            l = null,
            m = null,
            n = k("#mobileVerificaion"),
            o = a.tracking.getTracker();
        return j.ready = function(c, f) {
            c && k("[data-open=mobileVerificaion]").trigger("click"), j.trackingEvents.trackView(), k("#mobileVerificaion .show-edit").click(function() {
                k("#mobileVerificaion .edit-area").show(), k(".current-number-area").hide()
            }), k("#mobileVerificaion .resend-link").click(function(a) {
                a.preventDefault(), d(), k("#verfiyCode").val(""), k(".step-1").show(), k(".step-2").hide()
            }), k(".refreshCaptcha").click(function() {
                d()
            }), d(), h = b.clone("mobileVerificaion Form"), h.run({
                formSelector: "#mobileVerificationForm",
                submitSelector: "#mobileVerificationForm .send-code",
                blockSelector: "#mobileVerificaion .content",
                scrollOnValidation: !1,
                data: function() {
                    return {
                        type: "editPhoneAddress",
                        id_customer_address: k("#id_customer_address").val(),
                        phone: k("#CountryCode").val() + (k("#mobileVerificationForm #OperatorPrefix").val() ? k("#mobileVerificationForm #OperatorPrefix").val() : "") + k("#mobileNumber").val(),
                        id_captcha: k("#id_captcha").val(),
                        captcha: k("#captchaVal").val()
                    }
                },
                submitAjax: function(a, b) {
                    k(".wrongCaptcha").hide(), l = null, window.clearTimeout(l), d(), "logout" === b.message ? window.location.reload() : "error_captcha" !== b.message ? (j.trackingEvents.trackSendCode(), k(".mobileNumberConfirmation").text(k("#CountryCode").val() + (k("#mobileVerificationForm #OperatorPrefix").val() ? k("#mobileVerificationForm #OperatorPrefix").val() : "") + k("#mobileNumber").val()), k(".step-1").hide(), k(".step-2").show()) : l || (k(".wrongCaptcha").show(), l = window.setTimeout(function() {
                        k(".wrongCaptcha").hide(), window.clearTimeout(l), l = null
                    }, 3e3))
                },
                url: "Action.php",
                action: "ajaxRemote",
                translations: j.translations
            });
            var n = {
                type: "object",
                properties: {
                    mobileNumber: {
                        type: "digits",
                        minLength: 7
                    },
                    captchaVal: {
                        type: "digits",
                        minLength: 1
                    }
                },
                required: ["mobileNumber", "captchaVal"]
            };
            h.addSchema("mobileVerificaionSchema", n), h.setCurrentSchema("mobileVerificaionSchema"), i = b.clone("verfiyUserForm Form"), i.run({
                formSelector: "#verfiyUserForm",
                submitSelector: "#verfiyUserForm .send-code",
                blockSelector: "#mobileVerificaion .content",
                scrollOnValidation: !1,
                data: function() {
                    return {
                        type: "verifyCustomer",
                        verify_code: k("#verfiyCode").val(),
                        phone: k("#CountryCode").val() + (k("#mobileVerificationForm #OperatorPrefix").val() ? k("#mobileVerificationForm #OperatorPrefix").val() : "") + k("#mobileNumber").val()
                    }
                },
                submitAjax: function(a, b) {
                    d(), m = null, window.clearTimeout(m), "NOT_VERIFIED" === b.message ? (k(".wrongCode").show(), m || (m = window.setTimeout(function() {
                        k(".wrongCode").hide(), window.clearTimeout(m), m = null
                    }, 3e3))) : "VERIFIED" === b.message && (j.trackingEvents.trackSuccess(), k(".step-1,.step-2,.info").hide(), k(".step-3").show(), window.setTimeout(function() {
                        k("#mobileVerificaion").foundation("close")
                    }, 3e3), "OCC" === f ? e() : "checkout" === f && g())
                },
                url: "Action.php",
                action: "ajaxRemote",
                translations: j.translations
            });
            var o = {
                type: "object",
                properties: {
                    verfiyCode: {
                        type: "digits"
                    }
                },
                required: ["verfiyCode"]
            };
            i.addSchema("verfiyUserSchema", o), i.setCurrentSchema("verfiyUserSchema"), k(document).on("closed.zf.reveal", "#mobileVerificaion", function() {
                a.sandbox.publish("MobileVerification:closed")
            })
        }, j.trackingEvents = {
            trackView: function() {
                k(document).on("open.zf.reveal", "#mobileVerificaion", function() {
                    var b = a.tracking.getTracker();
                    b && b.tl && (b.linkTrackVars = "prop24,events", b.linkTrackEvents = b.events = "event35", b.prop24 = "Mobile Verification Appears", b.tl(this, "o", "Mobile Verification Appears"), b.events = b.linkTrackEvents = b.linkTrackVars = null)
                })
            },
            trackSendCode: function() {
                var b = a.tracking.getTracker();
                b && b.tl && (b.linkTrackVars = "prop24", b.prop24 = "send verification code clicked", b.tl(this, "o", "send verification code clicked"), b.events = b.linkTrackEvents = b.linkTrackVars = null)
            },
            trackSuccess: function() {
                var b = a.tracking.getTracker();
                b && b.tl && (b.linkTrackVars = "prop24", b.prop24 = "Mobile Number Verified", b.tl(this, "o", "Mobile Number Verified"), b.events = b.linkTrackEvents = b.linkTrackVars = null)
            }
        }, k.fn.centerMobileVerfication = function() {
            return this.css("position", "fixed"), this.css("top", k(window).height() / 2 - this.height() / 2 + "px"), this.css("left", k(window).width() / 2 - this.width() / 2 + "px"), this
        }, j
    }), define("pages/thankyou/nls/labels", {
        root: {
            required: "This field is required.",
            email: "Email address is invalid.",
            digits: "Enter digits only.",
            name: "Invalid characters.",
            nonZeroString: "Invalid characters.",
            PositiveNumber: "Enter a valid Number.",
            maxLength: "Enter a maximum of {0} characters.",
            minLength: "Enter at least {0} characters.",
            sShippingServicesErrorMessage: "There is no shipping available for this address. Please select or enter a new delivery address.",
            selectLocation: "Select this location",
            qitafdeposit: "Deposit amount must be less than the required amount",
            buyNowLoadingMessage: "Thank you. We are currently placing your order",
            expiredCard: "You have entered an expired credit card"
        },
        "en-US": !0,
        "ar-AE": !0
    }), define("pages/thankyou/nls/en-US/labels", {
        required: "This field is required.",
        email: "Email address is invalid.",
        digits: "Enter digits only.",
        name: "Invalid characters.",
        nonZeroString: "Invalid characters.",
        PositiveNumber: "Enter a valid Number.",
        maxLength: "Enter a maximum of {0} characters.",
        minLength: "Enter at least {0} characters.",
        sShippingServicesErrorMessage: "There is no shipping available for this address. Please select or enter a new delivery address.",
        selectLocation: "Select this location",
        qitafdeposit: "Deposit amount must be less than the required amount",
        expiredCard: "You have entered an expired credit card",
        englishCharacters: "Invalid characters"
    }), define("pages/thankyou/nls/ar-AE/labels", {
        required: "يجب إدخال هذه المعلومة.",
        email: "لقد قمت بإدخال عنوان بريد إلكتروني غير صحيح.",
        digits: "الرجاء إدخال أرقام فقط.",
        name: "أحرف غير صحيحة.",
        nonZeroString: "أحرف غير صحيحة.",
        PositiveNumber: "الرجاء إدخال أرقام صحيحة.",
        maxLength: "الرجاء إدخال كحد أقصى {0} حرفا",
        minLength: "الرجاء ادخال على الأقل {0} حرفا",
        sShippingServicesErrorMessage: "لا يـوجـد خـدمـة شـحـن لـهـذا الـعـنـوان، الـرجـاء اخـتـيـار عـنـوان آخـر أو إدخـال عـنـوان جـديـد.",
        selectLocation: "اختر الموقع",
        qitafdeposit: "يجب أن يكون المبلغ المراد إيداعه أقل من المبلغ المطلوب",
        buyNowLoadingMessage: "شكرا لك. نقوم بتنفيذ طلبك الآن",
        expiredCard: "لقد قمت بإدخال بطاقة ائتمانية منتهية الصلاحية",
        englishCharacters: "أحرف غير صحيحة."
    }),
    function() {
        var a = !1;
        ! function(b, c) {
            function d() {
                var b = define;
                define = function() {
                    arguments.length && "string" == typeof arguments[0] ? (define = b, a = arguments[0], window.hasAlmondJS ? define.apply(this, arguments) : c()) : (define = b, c())
                }
            }
            b.SouqLoading || (b.hasDefine = "function" == typeof b.define && b.define.amd, b.hasRequireJS = b.hasDefine && "function" == typeof b.require && "object" == typeof b.require.s, b.hasAlmondJS = b.hasDefine && !b.hasRequireJS), b.hasDefine ? (d(), define("extensions/formatters", [], c)) : c()
        }(window, function() {
            function b() {
                return "extensions/formatters"
            }
            var c = b(),
                d = function() {
                    if (window.hasRequireJS)
                        if (arguments.length && "string" == typeof arguments[0] && arguments[0] === c) {
                            var b = Array.prototype.slice.call(arguments);
                            a && a === c || (a ? b[0] = a : b.shift()), window.define.apply(this, b)
                        } else window.define.apply(this, arguments);
                    else window.define.apply(this, arguments)
                };
            d.amd = {
                jQuery: !0
            }, d("extensions/formatters", ["core"], function(a) {
                function b(b) {
                    a.utils.each(b, function(a) {
                        if ("function" != typeof a) throw new Error("All formatters should be functions.")
                    }), e = a.utils.extend(e, b)
                }

                function c(a, b) {
                    return e[a](b)
                }
                var d = a.dom,
                    e = {
                        hmcurrency: function(b) {
                            var c = a.getConfig(),
                                d = (b.currency || "") + "";
                            d.length <= 1 && (d = "000" + d), d.length < 2 && (d = "00" + d), d.length < 3 && (d = "0" + d);
                            var e = d.slice(0, d.length - c.currencyFraction) + c.fractionSeparator + d.slice(d.length - c.currencyFraction, d.length),
                                f = e.split(".");
                            return f[0] = f[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, c.thousandSeparator), f.join(c.fractionSeparator)
                        }
                    },
                    f = function(b, c) {
                        var e = d(b);
                        e.length > 0 && e.text(a.formatters.format("hmcurrency", {
                            currency: c
                        }))
                    },
                    g = function(a, b) {
                        var c = d(a);
                        c.length > 0 && c.text(b)
                    };
                a.formatters = {
                    defineFormatters: b,
                    format: c,
                    updatePrice: f,
                    updatePriceLocal: g
                }
            });
            var c = b();
            if (void 0 === c) throw new Error((a || "Component") + " Build Error : no identifer detected!");
            if (window.hasDefine) {
                if (window.hasAlmondJS) return window.require(c)
            } else require(c)
        })
    }(), define("components/buy-now-button", ["souq", "components/dropdown", "components/form", "components/mobileVerificaion", "i18n!../pages/thankyou/nls/labels", "../pages/thankyou/nls/ar-AE/labels", "extensions/formatters", "extensions/tracking"], function(a, b, c, d, e) {
        var f, g, h, i, j = new a.Component("BuyNowButton", {}),
            k = j.dom,
            l = a.getConfig(),
            m = ".id_warranty_radio",
            n = "#buyNowForm",
            o = !1,
            p = "body",
            q = a.tracking.getTracker(),
            r = !1,
            s = a.log,
            t = "data-shipping-service",
            u = "OCC",
            v = !1,
            w = !0,
            x = "",
            y = [],
            z = [],
            A = {
                mastercard: /^5[1-5]/,
                visa: /^4[0-9]/,
                amex: /^3[47][0-9]{5,}$/,
                discover: /^6(?:011|5[0-9]{2})[0-9]{3,}$/,
                diners: /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/,
                jcb: /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/,
                unknown: /.*/
            },
            B = "#oneMobileVerification",
            C = "#twoMobileVerification";
        return j.run = function(a) {
            this.base(a)
        }, j.ready = function() {
            o || (j.atatchEvents(), o = !0);
            var f = c.clone("BuyNow Form"),
                h = {};
            h.url = "one_click_checkout.php", h.action = "buy_now", h.formSelector = n, h.submitSelector = 'input[type="submit"]', h.sortEnabled = !1, h.translations = {}, h.aOptionalFields = null, h.submitAjax = function(b, c) {
                if ("0" === k("#is_cc_downgraded").val() && void 0 !== c.message) return void a.dom("body").html(c.message);
                var d = c.result;
                if ("fraud" === c.status || "invalid_csrf" === c.status) return c.msg && c.msg.length && a.messaging.addAlertMessage(c.msg, k("#flash-sess"), "alert", "insertBefore"), c.sRedirectURL && c.sRedirectURL.length && k("[data-open=mobileVerificaion]").trigger("click"), k(".off-canvas-wrapper").loadingOverlay("remove", {
                    containerClass: "buy-now-loading-container"
                }), !0;
                b ? (!1 === d.success ? ("UNIT_PRICE_CHANGED" === d.status ? k("#__u_p").val(d.new_price) : (a.messaging.addAlertMessage(a.translations.error, k("#flash-sess"), "alert", "insertBefore"), a.log.logError(new Error(JSON.stringify(c)))), k(".off-canvas-wrapper").loadingOverlay("remove", {
                    containerClass: "buy-now-loading-container"
                })) : !0 === d.success && (c.msg || j.onAfterPlaceOrderSuccess(k("#buy_now_button_source").val())), "" !== c.redirectUrl && null !== c.redirectUrl && (window.location = c.redirectUrl)) : (a.log.logError(new Error(JSON.stringify(c))), l.debug ? a.messaging.addAlertMessage(c.statusText, k("#flash-sess"), "alert", "insertBefore") : a.messaging.addAlertMessage(a.translations.error, k("#flash-sess"), "alert", "insertBefore"), k(p).unblock(), v = !1), k("#buy_now_button_source").val(null)
            }.bind(this), f.run(h), b.clone().run({
                DropdownId: "changePayBy",
                selector: "#changePayBy li",
                attrId: null
            }), k("[data-open=mobileVerificaion]").length && (d.translations = e, d.ready(0, u)), b.clone().run({
                DropdownId: "changeShipping",
                selector: "#changeShipping li",
                attrId: null
            }), k(".shipping-services").removeClass("loading").children().css("visibility", "visible"), k(".instant-checkout").removeClass("disabled");
            var i = ".serviceOption.selected";
            x = k("#selectedShippingOptionName").val(), k(".serviceOption").length > 1 ? (k("#selectedShippingOptionName").val(k(i).data("servicename")), g = k(i).data("servicename") + ":" + k(i).data("shipping-price-formated") + "|") : g = x + ":" + k("#shipping_price_formatted_track").val(), j.setShippingServiceOption(x)
        }, j.calculatePrice = function(b) {
            var c = 0,
                d = 0,
                e = k("#selectedPaymentMethod").val();
            if ("creditcard" === e || "fort" === e) c = parseInt(k("#total_price").val()) + parseInt(k("#shipping_price").val()) - (parseInt(k("#wallet_balance").val()) + parseInt(k("#cc_promotion").val())), d = a.formatters.format("hmcurrency", {
                currency: c
            }), k("#btnBuyNowReveal span").text(d), w && k("#btnBuyNowOnPlace span").text(d), k("#afterWallet").text(d), parseInt(k("#cc_promotion").val()) > 0 || parseInt(k("#total_promotion").val()) > 0 ? k("#ccPromotion").attr("class", "show") : k("#ccPromotion").attr("class", "hide");
            else if ("hitmeister" === e) c = parseInt(k("#total_price").val()) + parseInt(k("#shipping_price").val()), d = a.formatters.format("hmcurrency", {
                currency: c
            }), k("#btnBuyNowReveal span").text(d), w && k("#btnBuyNowOnPlace span").text(d), k("#afterFullWallet").text(d), k("#fees,#codPromotion").attr("class", "hide");
            else if ("new_creditcard" === e || "new_fort" === e) {
                void 0 === b && (b = 0), c = parseInt(k("#total_price").val()) - b + parseInt(k("#shipping_price").val()) - parseInt(k("#wallet_balance").val()), d = a.formatters.format("hmcurrency", {
                    currency: c
                }), k("#btnNewCCBuyNowReveal span").text(d), k("#afterWallet").text(d), k("#newCCPromotion").attr("class", "show");
                var f = parseInt(k("#normal_promotion").val());
                !isNaN(f) && 0 !== f || 0 !== b ? (k("#newCCPromotion").attr("class", "show"), b += f) : k("#newCCPromotion").attr("class", "hide"), void 0 !== b && b > 0 && k("#total_cc_promotion").text(a.formatters.format("hmcurrency", {
                    currency: b
                }))
            } else c = parseInt(k("#total_price").val()) + parseInt(k("#shipping_price").val()) + parseInt(k("#cod_price").val()), d = a.formatters.format("hmcurrency", {
                currency: c
            }), k("#btnCodBuyNowReveal span").text(d), k("#fees span").text(a.formatters.format("hmcurrency", {
                currency: k("#cod_price").val()
            }))
        }, j.oneClickBuyProceed = function() {
            k("#confirmOneClickBuyReveal").length && k("#confirmOneClickBuyReveal").foundation("close");
            var a = k("#selectedPaymentMethodInPlace").val();
            if ("creditcard" === a || "fort" === a) {
                var b = k("#CvvInput").val().replace(".", ""),
                    c = k("#token_reference").val();
                if (window.isNaN(b) || 3 !== b.length) return k("#CvvInput").focus().addClass("error"), !1;
                k("#CVC_OCC").val(b), "fort" === a && k("#token_reference").val(c), k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#RevealCvvInput").removeClass("error");
                var d = k("#RevealCvvInput").val();
                k("#CvvInput").val(d), k("#buy_now_button_source").val("in_place"), j.onPlaceOrdertrack(a, "in_place"), k(".off-canvas-wrapper").loadingOverlay({
                    overlayClass: "buy-now-loading-overlay",
                    textClass: "buy-now-loading-text",
                    loadingText: e.buyNowLoadingMessage,
                    withBackGround: !0,
                    containerClass: "buy-now-loading-container"
                }), k("#buyNowForm").submit()
            } else j.onPlaceOrdertrack("cash_on_delivery", "in_place"), k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#buy_now_button_source").val("in_place"), k(".off-canvas-wrapper").loadingOverlay({
                overlayClass: "buy-now-loading-overlay",
                textClass: "buy-now-loading-text",
                loadingText: e.buyNowLoadingMessage,
                withBackGround: !0,
                containerClass: "buy-now-loading-container"
            }), k("#buyNowForm").submit();
            j.setShippingFeesString(), j.onSelectedShippingServiceTrack(g, z), j.setShippingServiceOption(x)
        }, j.prepareNewCreditCard = function(b) {
            new a.API.Request("one_click_checkout.php", "prepare_new_credit_card", "POST").send({
                data: {
                    saveNewCreditCard: k("#save_new_credit_card").is(":checked"),
                    id_unit: k("#id_unit").val(),
                    bin: k("#CARDNO_rev").val().substr(0, 6),
                    shipping_option: k("#selectedShippingOption").val(),
                    id_customer_address: k("#id_customer_address").val(),
                    id_warranty: k("#id_warranty").val()
                },
                success: function(c) {
                    a.utils.isObject(c) && (!0 === c.success ? b(c.gateway_form) : k("#addItemToCart").submit())
                },
                error: function(b) {
                    l.debug ? a.messaging.addAlertMessage(b.message, k("#flash-sess"), "error", "append") : a.messaging.addAlertMessage(a.translations.error, k("#flash-sess"), "error", "append")
                },
                complete: function() {},
                beforeSend: function() {
                    k("#buyNow").loadingOverlay(), k("#content-body").loadingOverlay()
                }
            })
        }, j.prepareFort = function(b) {
            new a.API.Request("one_click_checkout.php", "prepare_fort_new_card", "POST").send({
                data: {
                    id_unit: k("#id_unit").val(),
                    bin: k("#CARDNO_rev").val().substr(0, 6),
                    shipping_option: k("#selectedShippingOption").val(),
                    id_customer_address: k("#id_customer_address").val(),
                    id_warranty: k("#id_warranty").val()
                },
                success: function(c) {
                    a.utils.isObject(c) && (!0 === c.success ? b() : k("#addItemToCart").submit())
                },
                error: function(b) {
                    l.debug ? a.messaging.addAlertMessage(b.message, k("#flash-sess"), "error", "append") : a.messaging.addAlertMessage(a.translations.error, k("#flash-sess"), "error", "append")
                },
                complete: function() {},
                beforeSend: function() {
                    k("#buyNow").loadingOverlay(), k("#content-body").loadingOverlay()
                }
            })
        }, j.submitFort = function() {
            k("#card_number").val(k("#CARDNO_rev").val()), k("#card_security_code").val(k("#newCreditCardCvv").val()), k("#card_holder_name").val(k("#cardHolder").val().trim().replace(/\s+/g, " "));
            var a = k("#ECOM_CARDINFO_EXPDATE_YEAR2").val().substring(2, 4) + k("#ECOM_CARDINFO_EXPDATE_MONTH2").val();
            k("#fort_expiration_date").val(a), k("#fort_expiration_date").val(a);
            var b = k("#save_new_credit_card").is(":checked") ? "YES" : "NO";
            k("#remember_me").val(b), k("#newCCForm").submit()
        }, j.checkNewCreditCardCvv = function() {
            var a = k("#newCreditCardCvv").val(),
                b = k("#CARDNO_rev").val(),
                c = k("#cardHolder").val(),
                d = k("#ECOM_CARDINFO_EXPDATE_MONTH2").val(),
                e = k("#ECOM_CARDINFO_EXPDATE_YEAR2").val(),
                f = d.length > 0,
                g = e.length > 0,
                h = c.length > 1 && new RegExp(/^([a-z]|[A-Z]?[ ]?)+$/).test(c),
                i = !window.isNaN(a) && 3 === a.length,
                j = !window.isNaN(b) && 16 === b.length;
            return f || g ? k("#ECOM_CARDINFO_EXPDATE_MONTH2").removeClass("error") : k("#ECOM_CARDINFO_EXPDATE_MONTH2").addClass("error"), g ? k("#ECOM_CARDINFO_EXPDATE_YEAR2").removeClass("error") : k("#ECOM_CARDINFO_EXPDATE_YEAR2").addClass("error"), k(".dateError")[f && g ? "hide" : "show"](), i ? k("#newCreditCardCvv").focus().removeClass("error").next().hide() : k("#newCreditCardCvv").focus().addClass("error").next().show(), h ? k("#cardHolder").focus().removeClass("error").next().hide() : k("#cardHolder").focus().addClass("error").next().show(), j ? k("#CARDNO_rev").focus().removeClass("error").next().hide() : k("#CARDNO_rev").focus().addClass("error").next().show(), !!(i && j && h && f && g) && (k("#CVC_Rev").val(a), k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#newCreditCardCvv").removeClass("error").next().hide(), k("#buy_now_button_source").val("reveal"), !0)
        }, j.callGateWayForNewCC = function(a) {
            k("#newCCForm").attr("action", a.form_action), k("#save_new_credit_card").is(":checked") ? (k("#SHASIGN").val(a.SHASIGNA), k("#ALIAS").val(a.ALIAS)) : (k("#ALIAS").remove(), k("#SHASIGN").val(a.SHASIGN)), k("#ACCEPTURL").val(a.ACCEPTURL), k("#EXCEPTIONURL").val(a.EXCEPTIONURL), k("#PSPID").val(a.PSPID), k("#CARDNO").val(k("#CARDNO_rev").val()), k("#CN").val(k("#cardHolder").val()), k("#ECOM_CARDINFO_EXPDATE_MONTH").val(k("#ECOM_CARDINFO_EXPDATE_MONTH2").val()), k("#ECOM_CARDINFO_EXPDATE_YEAR").val(k("#ECOM_CARDINFO_EXPDATE_YEAR2").val()), k("#CVC").val(k("#newCreditCardCvv").val()), j.onAfterPlaceOrderSuccess(k("#buy_now_button_source").val()), k("#newCCForm").submit()
        }, j.atatchEvents = function() {
            k(document).on("keydown", ".numberOnly", function(a) {
                return 13 === a.which || 9 === a.which || (8 === a.which || 0 === a.which || 9 === a.which || !(a.which < 48 || a.which > 57) || !(a.keyCode < 96 || a.keyCode > 105)) && void 0
            }), k(document).on("keydown", n, function() {
                if (13 === event.keyCode) return !1
            }), k(document).on("keydown", function() {
                13 === event.keyCode && k(".buy-now-popup").is(":visible") && k(".buy-now-popup").hasClass("revealOpened") && k("#occActions").find("button:visible").trigger("click")
            }), k(document).on("click", "#btnBuyNowOnPlace", function(a) {
                a.preventDefault(), "cash_on_delivery" === k("#selectedPaymentMethodInPlace").val() ? (k("#confirmOneClickBuyReveal").length && k("#confirmOneClickBuyReveal").foundation("open"),
                    j.trackConfirmRevealView()) : j.oneClickBuyProceed()
            }), k(document).on("click", "#confirmOneClickCheckout", function() {
                k(B).val() && !k(C).val() ? k("#mobileVerification").foundation("open") : j.oneClickBuyProceed()
            }), k(document).on("click", ".closeReveal", function() {
                k("#confirmOneClickBuyReveal").length && k("#confirmOneClickBuyReveal").foundation("close")
            }), k(document).on("closed.zf.reveal", "#buyNow", function() {
                v || (k("#selectedPaymentMethod").val(k("#selectedPaymentMethodInPlace").val()), k("#selectedShippingOption").val(k("#selectedShippingOptionInPlace").val()), y = []), j.resetNewCreditCardForm()
            }), k(document).on("click", ".serviceOption", function() {
                w = !0, k(".serviceOption").removeClass("selected"), k(".serviceOption").parent().removeClass("selected"), k(this).addClass("selected"), k(this).parent().addClass("selected");
                var a = k(this).data("shipping"),
                    b = k(this).data("shippingrate"),
                    c = k(this).data("codfee");
                x = k(this).data("servicename"), h = k(this).data("shipping-price-formated"), z.push(x + ":" + h), k("#selectedShippingOptionName").val(x), k("#selectedShippingOption").val(a), k("#cod_price").val(c), k("#shipping_price").val(b), j.setShippingServiceOption(x), j.calculatePrice()
            }), k(document).on("click", '[data-open="buyNow"]', function() {
                i = k(this).data("track-title"), j.onRevealOpendTrack();
                var a = k(".serviceOption.selected").data("shipping-service");
                a && (f = a)
            }), k(document).on("open.zf.reveal", "#buyNow", function() {
                k("#buyNow .price").text(k("#unit_price").val()), k("#buyNow .product-title").find("a").text(k("#unit_title").val()), k("#buyNow .product-title, .popup-img-bucket").find("a").attr("href", k("#unit_url").val()), k(".popup-img-bucket").find("a").html(k("#unit_image").val()), r = !0, window.setTimeout(function() {
                    k("#changePayBy .preSelect").trigger("click"), k("#changeShipping .preSelect").trigger("click"), f ? (k("#buyNow [" + t + "=" + f + "]").trigger("click"), f = null) : k("#buyNow [" + t + "]").first().trigger("click"), k("#changePayBy").find("li.hide").length === k("#changePayBy").find("li").length && (k("#changePayBy").remove(), k("[data-toggle=changePayBy]>.fi-chevron-bottom").remove()), k("#changeShipping").find("li.hide").length === k("#changeShipping").find("li").length && (k("#changeShipping").remove(), k("[data-toggle=changeShipping]>.fi-chevron-bottom").remove())
                }.bind(this), 0)
            }), k(document).on("keyup change", "#CARDNO_rev", function() {
                var a = k(this).val();
                if (a.length > 3) {
                    var b = "",
                        c = 0,
                        d = 0,
                        e = 0;
                    A.visa.test(a) && (b = "visa"), A.mastercard.test(a) && (b = "mastercard"), k("#CardNumIcon").attr("class", "card-num-icon payment-with-icon " + b);
                    for (var f = k(".cc-discount-" + b), g = 0; g < f.length; g += 1) {
                        var h = f[g];
                        "order" === k(h).data("promo-type") ? c += k(h).data("cc-discount") : d += k(h).data("cc-discount"), e += k(h).data("cc-discount")
                    }
                    if ("object" == typeof l.bins)
                        for (var i in l.itemBins)
                            if (l.itemBins.hasOwnProperty(i)) {
                                var m = l.itemBins[i].toString(); - 1 !== a.indexOf(m) && k("#" + i).data("card-type") === b && "" !== m && (e += k("#" + i).data("ccDiscount"))
                            }
                    j.calculatePrice(e)
                } else a.length < 2 && j.calculatePrice()
            });
            var b = function() {
                var a = k("#selectedPaymentMethod").val();
                if (v = !0, "creditcard" === a || "fort" === a) {
                    var b = k("#RevealCvvInput").val().replace(".", "");
                    if (window.isNaN(b) || 3 !== b.length) k("#RevealCvvInput").focus().addClass("error");
                    else {
                        k("#RevealCvvInput").removeClass("error");
                        var c = k("#RevealCvvInput").val();
                        k("#CvvInput").val(c), k("#buy_now_button_source").val("reveal"), k("#CVC_OCC").val(k("#RevealCvvInput").val()), k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#buyNow").foundation("close"), k(".off-canvas-wrapper").loadingOverlay({
                            overlayClass: "buy-now-loading-overlay",
                            textClass: "buy-now-loading-text",
                            loadingText: e.buyNowLoadingMessage,
                            withBackGround: !0,
                            containerClass: "buy-now-loading-container"
                        }), k(n).submit(), j.onPlaceOrdertrack(a, "reveal")
                    }
                } else if ("new_creditcard" === a) {
                    var d = j.checkNewCreditCardCvv();
                    if (!d) return;
                    j.prepareNewCreditCard(j.callGateWayForNewCC)
                } else if ("new_fort" === a) {
                    var f = j.checkNewCreditCardCvv();
                    if (!f) return;
                    j.prepareFort(j.submitFort)
                } else k("#buy_now_button_source").val("reveal"), ("cash_on_delivery" === k("#selectedPaymentMethod").val() && k("#mobileVerification").length || k(B).val()) && !k(C).val() ? k("#mobileVerification").foundation("open") : (k("#id_warranty_buy_now").val(k("#id_warranty").val()), k("#buyNow").foundation("close"), k(".off-canvas-wrapper").loadingOverlay({
                    overlayClass: "buy-now-loading-overlay",
                    textClass: "buy-now-loading-text",
                    loadingText: e.buyNowLoadingMessage,
                    withBackGround: !0,
                    containerClass: "buy-now-loading-container"
                }), k(n).submit(), j.onPlaceOrdertrack("cash_on_delivery", "reveal"));
                j.setShippingFeesString(), j.onSelectedShippingServiceTrack("", y), j.setShippingServiceOption(x)
            };
            k(document).on("click", "#btnBuyNowReveal", b), k(document).on("click", "#btnCodBuyNowReveal", b), k(document).on("click", "#btnNewCCBuyNowReveal", b), a.sandbox.subscribe("dropdown:click", function(a, b, c, d) {
                if (d && "changePayBy" === d) {
                    var e = k(a).data("creditcard"),
                        f = k(a).data("fort"),
                        g = k(a).data("cod"),
                        i = k(a).data("newCc"),
                        l = k(a).data("newFort"),
                        m = "",
                        n = "";
                    k("#new_cc_div").attr("class", "hide"), void 0 !== e || void 0 !== f ? (k("#cardViewer").attr("class", "payment-with-icon " + (e || "cash-on-delivery")), void 0 !== e ? m = "creditcard" : void 0 !== f && (m = "fort"), k("#payment").attr("class", "small-9 columns"), k("#partial").attr("class", "small-12 columns show"), k("#cvv").attr("class", "cvv  small-3 clearPadding-defaultFloat columns show"), k("#btnBuyNowReveal").attr("class", "cta button text-center buy-now-btn clearMargin-bottom"), k("#ccPromotion").attr("class", "small-12 columns"), k("#fees,#btnCodBuyNowReveal,#btnNewCCBuyNowReveal,#codPromotion,#ccPromotion,#newCCPromotion").attr("class", "hide")) : void 0 !== g ? (k("#cardViewer").attr("class", null).attr("class", "fi-dollar"), m = "cash_on_delivery", k("#payment").attr("class", "small-12 columns"), k("#fees").attr("class", "show Primary opposite-direction"), k("#btnCodBuyNowReveal").attr("class", "cta button text-center buy-now-btn clearMargin-bottom"), k("#codPromotion").attr("class", "small-12 columns"), k("#cvv,#partial,#btnBuyNowReveal,#newCCPromotion,#btnNewCCBuyNowReveal,#ccPromotion").attr("class", "hide"), n = "cash_on_delivery") : void 0 === i && void 0 === l || (void 0 !== i ? m = "new_creditcard" : void 0 !== l && (m = "new_fort"), k("#cvv").attr("class", "hide"), k("#payment").attr("class", "small-12 columns"), k("#partial").attr("class", "small-12 columns show"), k("#new_cc_div").attr("class", "small-12 columns new-credit"), k("#btnNewCCBuyNowReveal").attr("class", "cta button text-center buy-now-btn clearMargin-bottom"), k("#fees,#btnCodBuyNowReveal,#ccPromotion,#newCCPromotion,#codPromotion,#btnBuyNowReveal").attr("class", "hide")), r ? r = !1 : "new_creditcard" !== m && j.changePaymentMethodTrack(n, m), k("#selectedPaymentMethod").val(m), j.resetNewCreditCardForm(), j.calculatePrice()
                } else if (d && "changeShipping" === d) {
                    var o = k(a).data("shipping"),
                        p = k(a).data("shippingrate"),
                        q = k(a).data("codfee");
                    x = k(a).data("servicename"), k("#selectedShippingOptionName").val(x), k("#selectedShippingOption").val(o), k("#cod_price").val(q), k("#shipping_price").val(p), h = k(a).data("shipping-price-formated"), y.push(x + ":" + h), k(".shipping-select span.value").html(k(a).html()), k(a).click(function() {
                        k(".shipping-select span.value").html(k(a).html())
                    }), j.calculatePrice()
                }
                j.setShippingServiceOption(x), w = !1
            }), k(document).on("change", m, function() {
                var b = "yes" === k("#occ_is_eligable").val();
                if (b) {
                    k("#content-body").loadingOverlay();
                    new a.API.Request("one_click_checkout.php", "eligibility", "GET").send({
                        data: {
                            id_unit: k("#id_unit").val(),
                            id_warranty: k(this).val(),
                            ab: b,
                            abParam: a.utils.urlParam("ab")
                        },
                        beforeSend: function() {
                            k("#content-body").loadingOverlay()
                        },
                        complete: function() {
                            k("#content-body").loadingOverlay("remove"), k(document).foundation()
                        },
                        success: function(a) {
                            if (!0 === a.success && !0 === a.isEligable) {
                                var b = k("#occContainer");
                                b.html(""), b.prepend(a.sHtmlOneClickCheckoutInPlacePartial), j.ready()
                            }
                        }.bind(this),
                        error: function(b) {
                            l.debug ? a.messaging.addAlertMessage(b.message, k("#flash-sess"), "alert", "append") : a.messaging.addAlertMessage(a.translations.error, k("#flash-sess"), "alert", "append")
                        }
                    })
                }
            }), k(".occLoading").unblock(), j.onSelectedShippingServiceTrack = function(a, b) {
                q && q.tl ? (q.linkTrackVars = "prop30", q.prop30 = a + b.join("|"), q.tl(this, "o", "One-Click Checkout Tracking"), delete q.linkTrackVars, delete q.prop30, z = [], y = []) : s.logError(new Error("Omniture (s) not defined!"))
            }, j.onRevealOpendTrack = function() {
                q && q.tl ? (q.linkTrackVars = "prop30", q.prop30 = i || "OCC Reveal", q.tl(this, "o", "One-Click Checkout Tracking"), delete q.linkTrackVars, delete q.prop30) : s.logError(new Error("Omniture (s) not defined!")), i = null
            }, j.changePaymentMethodTrack = function(a, b) {
                q && q.tl ? (q.linkTrackVars = "prop30", q.prop30 = "Payment Method Changed from " + a + " to " + b, q.tl(this, "o", "One-Click Checkout Tracking"), delete q.linkTrackVars, delete q.prop30) : s.logError(new Error("Omniture (s) not defined!"))
            }, j.onAfterPlaceOrderSuccess = function(a) {
                q && q.tl ? (q.linkTrackVars = "prop30", q.prop30 = "Order placed with OCC " + a, q.tl(this, "o", "One-Click Checkout Tracking"), delete q.linkTrackVars, delete q.prop30) : s.logError(new Error("Omniture (s) not defined!"))
            }, j.onPlaceOrdertrack = function(a, b) {
                var c = k("#id_unit").val(),
                    d = k("#__u_p").val();
                q && q.tl ? (q.linkTrackVars = "eVar12,products,events,prop30", q.linkTrackEvents = q.events = "event18", q.eVar12 = a, q.prop30 = "One-Click Checkout " + b + "(" + a + ")", q.products = ";" + c + ";1;" + d, q.tl(this, "o", "One-Click Checkout Tracking"), delete q.linkTrackVars, delete q.linkTrackEvents, delete q.eVar12, delete q.prop30, delete q.products) : s.logError(new Error("Omniture (s) not defined!"))
            }, j.trackConfirmRevealView = function() {
                q && q.tl ? (q.linkTrackVars = "events,prop30", q.prop30 = "OCC_Confirmation_Popup_Appears", q.linkTrackEvents = q.events = "event56", q.tl(this, "o", "OccConfirmPopupTracking"), q.events = q.linkTrackEvents = q.linkTrackVars = null) : s.logError(new Error("Omniture (s) not defined!"))
            }, j.setShippingFeesString = function() {
                var b = "",
                    c = k("#shipping_price_formatted").val();
                b = 0 === parseInt(c) ? "0.00" : parseFloat(c).toFixed(2), a.utils.setCookie("setShippingFees", b)
            }, j.setShippingServiceOption = function(b) {
                a.utils.setCookie("s_shipping_service", b)
            }
        }, j.resetNewCreditCardForm = function() {
            k("#CARDNO_rev,#cardHolder,#newCreditCardCvv,#ECOM_CARDINFO_EXPDATE_MONTH2,#ECOM_CARDINFO_EXPDATE_YEAR2").val("").removeClass("error").next().hide(), k(".dateError").hide(), k("#CardNumIcon").attr("class", "card-num-icon payment-with-icon")
        }, j
    }), define("components/googleRecaptcha", ["souq", "jquery"], function(a, b) {
        var c = new a.Component("googleRecapcha"),
            d = b;
        return c.run = function() {}, c.ready = function() {}, c.generateRecaptcha = function(a, b, c) {
                var e = a.container;
                return grecaptcha.render(e, {
                    sitekey: d("#" + e).data("sitekey"),
                    callback: function(a) {
                        b(a)
                    },
                    "expired-callback": c
                })
            },
            c.reset = function(a) {
                grecaptcha.reset(a)
            }, c.getRecaptchaForForm = function() {
                var b = a.getConfig();
                d.getScript("https://www.google.com/recaptcha/api.js?hl=" + b.language, function() {})
            }, c
    }), define("components/mobileVerificaion-v1", ["souq", "jquery", "components/buy-now-button", "components/googleRecaptcha", "i18n!../pages/thankyou/nls/labels", "../pages/thankyou/nls/ar-AE/labels", "extensions/tracking", "components/form"], function(a, b, c, d, e) {
        var f = new a.Component("mobileVerificaion-v1"),
            g = a.tracking.getTracker(),
            h = b,
            i = !1,
            j = !1,
            k = "#firstStepRecaptcha",
            l = "#secondStepRecaptcha",
            m = ".mobile-verification-reveal",
            n = "#codeVerification",
            o = "#verifyCodeInput",
            p = "#verify-error-msg",
            q = "#error-msg",
            r = "#captcha-error-msg",
            s = "#submitFirstStep",
            t = "#submitSecondStep",
            u = "#submitSecondStepBanner",
            v = "",
            w = "",
            x = "#editPhoneNumber",
            y = "#editableInput",
            z = "#editableInputField",
            A = !1,
            B = !1,
            C = "",
            D = "",
            E = "",
            F = "",
            G = "#verifyCodeBannerInput",
            H = !1,
            I = !1,
            J = 0;
        return f.runCounter = 0, f.openMobileEventCounter = 0, f.ready = function() {
            d.getRecaptcha(), f.build(), f.verifyInputField(z, [32, 46, 8, 9, 27, 13, 110]), f.verifyInputField(G, [32, 46, 8, 9, 27, 13, 110]), f.verifyInputField(o, [46, 8, 9, 27, 13, 110]), f.submitOnEnter(z, s), f.submitOnEnter(G, u), f.separatedVerificationCode(), f.changeselectedCodeHolder(), f.showFlashMessage(), f.runCounter += 1, h("#bShowOnceAtHomePage").val() && h("#mobileVerification").length && h("#mobileVerification").foundation("open")
        }, f.showFlashMessage = function() {
            h("#authVirificationBar").length || "home" !== window.globals.controller || h("#flash-sess").css("display", "block")
        }, f.openMobileEvent = function() {
            f.openMobileEventCounter < 1 && (h(document).on("open.zf.reveal", "#mobileVerification", function() {
                if (f.validatePhoneNumber(h("#existingUnique").text())) {
                    var a = "+" === h("#existingUnique").text()[0] ? h("#existingUnique").text().replace(/ /g, "").substring(1) : h("#existingUnique").text().replace(/ /g, "");
                    h("#existingUnique").text("+" + f.addSpacesToNumber(a))
                }
                if ("checkout" !== window.globals.controller && f.tracking("Customer Mobile Verification Pop-Up Appears"), i) {
                    var b = "";
                    h(k).is(":empty") ? (b = d.generateRecaptcha({
                        container: "firstStepRecaptcha"
                    }, function(a) {
                        E = a
                    }, function() {
                        E = "", F = ""
                    }), "home" === window.globals.controller ? h("#mobileVerification .submit-container").css("margin-top", "10px") : h("#mobileVerification .submit-container").css("margin-top", "20px")) : d.reset(b), h(".first-step-captcha-container").removeClass("hide")
                }
                h("body").addClass("is-reveal-open")
            }), h(document).on("closed.zf.reveal", "#mobileVerification", function() {
                v = "mobileVerification", h(z).removeClass("error"), h(q).addClass("hide"), h(k).removeClass("error"), h(r).addClass("hide"), h("#ajax-error-msg").addClass("hide"), E = "", h("#existingUnique").removeClass("hide"), h(y).addClass("hide"), h("#editPhoneNumber").removeClass("hide"), A = !1, i = !1
            }), h(document).on("open.zf.reveal", "#codeVerification", function() {
                h("body").addClass("is-reveal-open"), h(".verification-field:eq(0)").val("").focus()
            }), h(document).on("open.zf.reveal", "#confirmToCloseReveal", function() {
                h("body").hasClass("is-reveal-open") || h("body").addClass("is-reveal-open"), "mobileVerification" === v ? f.tracking("Abort Mobile Verification Pop-Up Appears") : "codeVerification" === v && f.tracking("Abort Verify code Pop-Up Appears")
            }), h(document).on("closed.zf.reveal", "#codeVerification", function() {
                h("#verifyTitleChangedDesign").removeClass("hide"), h("#successVerificationTitle").addClass("hide"), h("#verifyContentSecondStep").removeClass("hide"), h("#successContentSecondStep").addClass("hide"), "home" !== window.globals.controller || H || f.checkIfMobile() || (h("#enteredNumberBanner").text("+" + w), h("#firstStepBanner").addClass("hide"), h("#secondStepBanner").removeClass("hide")), h(".verification-field").each(function() {
                    h(this).val("")
                }), v = "codeVerification", h(o).val(""), h(o).removeClass("error"), h(p).addClass("hide"), h(l).removeClass("error"), h("#verify-code-error-msg").addClass("hide"), F = "", j && (h(l).is(":empty") ? C = d.generateRecaptcha({
                    container: "secondStepRecaptcha"
                }, function(a) {
                    F = a
                }, function() {
                    E = "", F = ""
                }) : d.reset(C), h(".second-step-captcha-container").removeClass("hide"))
            })), f.openMobileEventCounter += 1
        }, f.separatedVerificationCode = function() {
            function a(a) {
                var b = a.which ? a.which : a.keyCode,
                    c = h(a.target),
                    d = c.next(".verification-field");
                return 37 !== b && 39 !== b && 9 !== b && 8 !== b && (b < 48 || b > 57) && (b < 96 || b > 105) ? (a.preventDefault(), !1) : 9 === b || (8 === b ? (a.preventDefault(), h(this).val() ? void h(this).val("") : void c.prev(".verification-field").select().focus()) : 37 === b ? void c.prev(".verification-field").select().focus() : 39 === b ? void c.next(".verification-field").select().focus() : (d && d.length || (d = e.find(".verification-field").eq(0)), void(c.val() && d.select().focus())))
            }

            function b(a) {
                var b = a.which;
                return 13 === b && h(t).trigger("click"), 37 === b || 39 === b || 9 === b || 8 === b || b >= 48 && b <= 57 || b >= 96 && b <= 105 || (a.preventDefault(), !1)
            }

            function c(a) {
                var b = a.which;
                return 37 === b ? void h(".verification-field:focus").prev(".verification-field").select().focus() : 39 === b ? void h(".verification-field:focus").next(".verification-field").select().focus() : 8 === b ? (a.preventDefault(), h(".verification-field:focus").val() ? void h(".verification-field:focus").val("") : void h(".verification-field:focus").prev(".verification-field").select().focus()) : void 0
            }

            function d(a) {
                h(a.target).select()
            }
            var e = h(n);
            e.on("input", ".verification-field", a), e.on("keyup", ".verification-field", c), e.on("keydown", ".verification-field", b), e.on("click", "input", d)
        }, f.tracking = function(a) {
            if (g && g.tl) {
                var b = "";
                switch (window.globals.controller) {
                    case "product":
                        b = "OCC - Int Shipping: ", h("#bFullfillmentRate").val() && (b = "OCC: ");
                        break;
                    case "checkout":
                        b = "1" !== h("#checkoutStep").val() ? h("#bFullfillmentRate").val() ? "Checkout: " : "Checkout - Int Shipping: " : "Checkout Address: ";
                        break;
                    case "home":
                        b = "Homepage: ";
                        break;
                    case "account_addresses":
                        b = "Account Address: ";
                        break;
                    case "thankyou":
                        b = "Thank you page: "
                }
                g.linkTrackVars = "prop40", g.prop40 = b + a, g.tl(this, "o", "account_addresses Tracking"), delete g.linkTrackVars, delete g.prop40
            }
        }, f.changeResend = function() {
            h("#removeToAddLoading").addClass("hide"), B ? (h(".lodingForCall").removeClass("hide"), h(".resendButton").css("visibility", "hidden"), h("#voiceCall").addClass("hide"), h(".editRevealBack").addClass("hide"), window.setTimeout(function() {
                h(".lodingForCall").addClass("hide"), h("#voiceCall").removeClass("hide"), h(".resendButton").css("visibility", "visible"), h("#removeToAddLoading").removeClass("hide"), h(".editRevealBack").removeClass("hide")
            }, 3e4)) : (J += 1, J >= 10 ? (h(".resendButton").addClass("hide"), h(".sentLabel").addClass("hide")) : (h(".resendButton").each(function() {
                "voice" !== h(this).data("method") && h(this).addClass("hide")
            }), h(".sentLabel").removeClass("hide"), h("#voiceCall").addClass("hide"), window.setTimeout(function() {
                h(".sentLabel").addClass("hide"), h("#voiceCall").removeClass("hide"), h("#removeToAddLoading").removeClass("hide"), h(".resendButton").each(function() {
                    "voice" !== h(this).data("method") && h(this).removeClass("hide")
                })
            }, 6e3))), B = !1
        }, f.submitFirstStep = function(a, b, c) {
            w = "+" === a.tel[0] ? a.tel.substr(1) : a.tel, h.ajax({
                url: b,
                data: a,
                type: "POST",
                beforeSend: function() {
                    h(m).addClass("loading")
                },
                success: function(a) {
                    a = JSON.parse(a), a.success ? (h("#secret_token").val(a.request_token), h("#enteredMobileNumber").text("+" + w), c ? f.changeResend() : (f.tracking("Send Customer Mobile Verification"), f.tracking("Verify code Pop-Up Appears"), h(n).foundation("open")), h("#phone").removeClass("error"), h(q).addClass("hide"), h(k).removeClass("error"), h(r).addClass("hide"), h("#ajax-error-msg").addClass("hide"), a.show_gr && (i = !0)) : (a.reload ? window.location.reload() : a.error_msg && (h("ajax-error-msg").text(a.error_msg), h("#ajax-error-msg").removeClass("hide")), a.show_gr && (E = "", h(k).is(":empty") ? (D = d.generateRecaptcha({
                        container: "firstStepRecaptcha"
                    }, function(a) {
                        h(k).removeClass("error"), h(r).addClass("hide"), E = a
                    }, function() {
                        E = "", F = ""
                    }), h("#mobileVerification .submit-container").css("margin-top", "10px")) : d.reset(D))), h(m).removeClass("loading")
                },
                error: function() {
                    h(m).removeClass("loading")
                }
            })
        }, f.showBannerAbuseMessage = function(a, b, c, d) {
            h(a).fadeIn(b), window.setTimeout(function() {
                h(a).fadeOut(b)
            }, c), d && (h(G).attr("disabled", "true"), h(u).attr("disabled", "true"))
        }, f.submitSecondStep = function(a, b) {
            h(m).addClass("loading"), h.ajax({
                url: b,
                data: a,
                type: "POST",
                success: function(a) {
                    a = JSON.parse(a), a.verify_banner_attempts && 10 === a.verify_banner_attempts && f.showBannerAbuseMessage("#firstTimeAbuse", 700, 4e3, !1), a.verify_banner_attempts && 20 === a.verify_banner_attempts && (f.showBannerAbuseMessage("#secondTimeAbuse", 700, 4e3, !0), I = !0), a.show_gr && (F = "", h(l).is(":empty") ? C = d.generateRecaptcha({
                        container: "secondStepRecaptcha"
                    }, function(a) {
                        h(p).addClass("hide"), h(l).removeClass("error"), F = a
                    }, function() {
                        E = "", F = ""
                    }) : d.reset(C), h(".second-step-captcha-container").removeClass("hide"), j = !0), a.success ? ("home" === window.globals.controller ? (H = !0, h("#authVirificationBar").removeClass("error-auth-banner").addClass("success-step-show"), h("#firstStepBanner").addClass("hide"), h("#secondStepBanner").addClass("hide"), h("#successStepBanner").removeClass("hide"), h("#doneLogoHeader").removeClass("hide"), h("#smsLogoHeader").addClass("hide"), h("#verifyTitleChangedDesign").addClass("hide"), h("#successVerificationTitle").removeClass("hide"), h("#verifyContentSecondStep").addClass("hide"), h("#successContentSecondStep").removeClass("hide"), window.setTimeout(function() {
                        h(n).foundation("close"), h("#authVirificationBar").addClass("hide")
                    }, 3e3)) : "account_addresses" === window.globals.controller || "1" === h("#checkoutStep").val() && "checkout" === window.globals.controller ? (h("#removeVerify-" + a.idCustomerAddress).remove(), h("#changePhone-" + a.idCustomerAddress).text(a.newTel), h('.showMobileVerification[data-mobile-number="' + a.newTel + '"]').each(function() {
                        h(this).remove()
                    }), h("#verifyTitleChangedDesign").addClass("hide"), h("#successVerificationTitle").removeClass("hide"), h("#verifyContentSecondStep").addClass("hide"), h("#successContentSecondStep").removeClass("hide"), window.setTimeout(function() {
                        h(n).foundation("close")
                    }, 1e3)) : h(n).foundation("close"), f.tracking("Customer Mobile Number Verified"), "reveal" === h("#buy_now_button_source").val() ? (h("#id_warranty_buy_now").val(h("#id_warranty").val()), h("#buyNow").foundation("close"), h(".off-canvas-wrapper").loadingOverlay({
                        overlayClass: "buy-now-loading-overlay",
                        textClass: "buy-now-loading-text",
                        loadingText: e.buyNowLoadingMessage,
                        withBackGround: !0,
                        containerClass: "buy-now-loading-container"
                    }), h("#buyNowForm").submit(), c.onPlaceOrdertrack("cash_on_delivery", "reveal")) : "product" === window.globals.controller ? c.oneClickBuyProceed() : "checkout" === window.globals.controller && h("#payment_form").submit()) : (a.reload && window.location.reload(), h(o).addClass("error"), h("#verify-code-error-msg").removeClass("hide"), h("#authVirificationBar").addClass("error-auth-banner"), h(G).val(""), h(".verification-field").each(function() {
                        h(this).val("")
                    })), h(m).removeClass("loading"), h("#submitSecondStepBanner").removeClass("loading")
                },
                error: function() {
                    h(m).removeClass("loading"), h("#submitSecondStepBanner").removeClass("loading")
                }
            })
        }, f.verifyInputField = function(a, b) {
            h(a).keydown(function(a) {
                -1 !== h.inArray(a.keyCode, b) || 65 === a.keyCode && !0 === a.ctrlKey || 67 === a.keyCode && !0 === a.ctrlKey || 88 === a.keyCode && !0 === a.ctrlKey || 86 === a.keyCode && !0 === a.ctrlKey || 90 === a.keyCode && !0 === a.ctrlKey || a.keyCode >= 35 && a.keyCode <= 39 || (a.shiftKey || a.keyCode < 48 || a.keyCode > 57) && (a.keyCode < 96 || a.keyCode > 105) && a.preventDefault()
            })
        }, f.submitOnEnter = function(a, b) {
            h(document).on("keydown", a, function(a) {
                13 === a.keyCode && h(b).trigger("click")
            })
        }, f.changeselectedCodeHolder = function() {
            h("#selectedCountries").change(function() {
                h(z).val("");
                for (var a = h("#flagContainerVerification").attr("class").split(" "), b = [], c = 0; c < a.length; c++) a[c].search(/flag_+/) && (b[b.length] = a[c]);
                h("#flagContainerVerification").removeClass().addClass(b.join(" ") + " flag_" + h("#selectedCountries option:selected").text()), h("#countryCodeHolder").text(h(this).val())
            })
        }, f.checkIfMobile = function() {
            return !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        }, f.validatePhoneNumber = function(a) {
            var b = "+" === a[0] ? a : "+" + a,
                c = /^\+?\d+$/.test(b),
                d = b.substring(4, 6),
                e = b.substring(3, 5),
                f = b.substring(4, 5),
                g = 7 === b.substring(6, b.length).length,
                h = 8 === b.substring(5, b.length).length,
                i = 7 === b.substring(5, b.length).length,
                j = "50" === d || "58" === d || "51" === d || "52" === d || "54" === d || "55" === d || "56" === d,
                k = "50" === d || "51" === d || "52" === d || "53" === d || "54" === d || "55" === d || "56" === d || "57" === d || "58" === d || "59" === d,
                l = "10" === e || "11" === e || "12" === e,
                m = "3" === f,
                n = "7" === f || "9" === f,
                o = "3" === f || "5" === f || "6" === f || "7" === f,
                p = "971" === b.substring(1, 4),
                q = "965" === b.substring(1, 4),
                r = "966" === b.substring(1, 4),
                s = "20" === b.substring(1, 3),
                t = "973" === b.substring(1, 4),
                u = "968" === b.substring(1, 4),
                v = "974" === b.substring(1, 4),
                w = g && j && p,
                x = h && l && s,
                y = !1,
                z = t && i && m,
                A = u && i && n,
                B = v && i && o,
                C = g && k && r;
            if (q) {
                var D = b.substring(4, b.length),
                    E = /(?:5(?:[05]\d{2}|1[0-7]\d|2(?:22|5[25])|66\d)|6(?:0[034679]\d|222|5[015-9]\d|6\d{2}|7[067]\d|9[0369]\d)|9(?:0[09]\d|22\d|4[01479]\d|55\d|6[0679]\d|[79]\d{2}|8[057-9]\d))\d{4}/g,
                    F = D.match(E);
                F && null !== F && (y = !0)
            }
            return !(!c || !(C || w || y || x || z || B || A))
        }, f.addSpacesToNumber = function(a) {
            var b = a,
                c = b.substr(0, 3);
            return b = "201" === c ? b.substr(0, 2) + " " + b.substr(2, 2) + " " + b.substr(4) : "973" === c || "974" === c || "968" === c || "965" === c ? b.substr(0, 3) + " " + b.substr(3, 1) + " " + b.substr(4) : b.substr(0, 3) + " " + b.substr(3, 2) + " " + b.substr(5)
        }, f.build = function() {
            var a = {
                    tel: "",
                    captcha: "",
                    action: "ajaxRemote",
                    ajaxcall: "Customers_requestCode",
                    hitCfs: h("#hitscfs_requestCode").val(),
                    hitCfsMeta: h("#hitsCfsMeta_requestCode").val(),
                    id_customer_address: h("#id_customer_address_mobile_reveal").val(),
                    type: "SMS"
                },
                b = window.globals.HOST + "/Action.php";
            h(".editRevealBack").on("click", function() {
                h(x).trigger("click", [!0, h(this).data("place")]), "checkout" === window.globals.controller && f.tracking("Customer Mobile Verification Pop-Up Appears")
            }), h(x).on("click", function(a, b, c) {
                var d = h("#existingUnique"),
                    e = d.data("value").toString();
                b && (e = h("#enteredMobileNumber").text().slice(1)), "authBanner" === c && (e = h("#enteredNumberBanner").text().slice(1));
                var f = "+" + e.substr(0, 3),
                    g = e.substr(3);
                "+201" === f && (f = "+" + e.substr(0, 2), g = e.substr(2)), h("#countryCodeHolder").text(f), h(z).val(g), h("#existingUnique").addClass("hide"), h(y).removeClass("hide"), h("#editPhoneNumber").addClass("hide"), A = !0
            }), h("#openValidationReveal").unbind().click(function() {
                f.tracking("Cancel Order Abort"), "mobileVerification" === v ? (h("#mobileVerification").foundation("open"), "checkout" === window.globals.controller && f.tracking("Customer Mobile Verification Pop-Up Appears")) : "codeVerification" === v && h(n).foundation("open")
            }), h("#hideForEverSession").click(function() {
                var a = {
                    action: "ajaxRemote",
                    ajaxcall: "Customers_hideMobileVerification"
                };
                h.ajax({
                    url: window.globals.HOST + "/Action.php",
                    data: a,
                    type: "POST",
                    success: function() {}
                })
            }), h(s).click(function() {
                var c = h("#existingUnique");
                if (i && !E) return h(k).addClass("error"), void h(r).removeClass("hide");
                if (i && E && (a.captcha = E), A || h("#newAddress").val()) {
                    h("#newAddress").val() && (a.isNewAddress = h("#newAddress").val());
                    var d = (h("#countryCodeHolder").text() + h(z).val()).replace(/\s/g, "");
                    f.validatePhoneNumber(d) ? (a.tel = d, f.submitFirstStep(a, b)) : (h(z).addClass("error"), h(q).removeClass("hide"))
                } else a.tel = c.data("value"), f.submitFirstStep(a, b)
            }), h(".resendButton, #resendVoiceBanner").click(function() {
                var c = h(this).data("place");
                a.tel = "authBanner" === c ? h("#enteredNumberBanner").text() : h("#enteredMobileNumber").text(), "voice" === h(this).data("method") ? (a.type = "VOICE", B = !0, "authBanner" === c ? f.tracking("Call me clicked - Banner") : f.tracking("Call me clicked")) : ("authBanner" === c ? f.tracking("Resend Banner Clicked") : f.tracking("Resend Customer Mobile Verification"), a.type = "SMS"), h(".verification-field").each(function() {
                    h(this).val("")
                }), a.captcha = "", f.submitFirstStep(a, b, !0), h(o).val(""), h(o).removeClass("error"), h("#verify-code-error-msg").addClass("hide"), a.type = "SMS"
            }), h(u).click(function() {
                if (I) return !1;
                var a = h(G).val(),
                    b = h("#secret_token").val(),
                    c = h("#hitscfs_verifyCode").val(),
                    d = h("#hitsCfsMeta_verifyCode").val();
                if (f.tracking("Verify Banner Clicked"), 4 === a.length) {
                    h(this).addClass("loading");
                    var e = {
                            verifyCode: a,
                            secret_token: b,
                            hitCfs: c,
                            hitCfsMeta: d,
                            action: "ajaxRemote",
                            ajaxcall: "Customers_verifyCode",
                            id_customer_address: h("#id_customer_address_mobile_reveal").val(),
                            verify_from_banner: !0
                        },
                        g = window.globals.HOST + "/Action.php";
                    f.submitSecondStep(e, g)
                } else h("#authVirificationBar").addClass("error-auth-banner")
            }), h(t).click(function() {
                var a = [];
                h(".verification-field").each(function() {
                    a.push(h(this).val())
                });
                var b = a.join(""),
                    c = h("#secret_token").val(),
                    d = h("#hitscfs_verifyCode").val(),
                    e = h("#hitsCfsMeta_verifyCode").val();
                if (h(o).removeClass("error"), h("#verify-code-error-msg").addClass("hide"), f.tracking("Verify Customer Mobile Number Clicked"), 4 === b.length) {
                    var g = {
                        verifyCode: b,
                        secret_token: c,
                        hitCfs: d,
                        hitCfsMeta: e,
                        action: "ajaxRemote",
                        ajaxcall: "Customers_verifyCode",
                        id_customer_address: h("#id_customer_address_mobile_reveal").val()
                    };
                    if (j && !F) return h(l).addClass("error"), void h(p).removeClass("hide");
                    j && F && (g.captcha = F);
                    var i = window.globals.HOST + "/Action.php";
                    f.submitSecondStep(g, i)
                } else h(o).addClass("error"), h("#verify-code-error-msg").removeClass("hide")
            }), h("#confitmCloseReveal").click(function() {
                f.tracking("Confirm Order Abort")
            }), f.openMobileEvent()
        }, f
    }), define("pages/rating_experience/nls/labels", {
        root: {
            Error: "Sorry, your review wasn’t submitted due to an error, please try again",
            STAR_TEXT_1: "I hate it",
            STAR_TEXT_2: "I don’t like it",
            STAR_TEXT_3: "It’s ok",
            STAR_TEXT_4: "I like it",
            STAR_TEXT_5: "I love it",
            INVALID_ITEM: "Sorry, this item does not exist",
            REASONS_ERROR: "Please select at least one reason for being unhappy with this item. Help us to understand the problem!"
        },
        "en-US": !0,
        "ar-AE": !0
    }), define("pages/rating_experience/nls/en-US/labels", {
        Error: "Sorry, your review wasn’t submitted due to an error, please try again",
        STAR_TEXT_1: "I hate it",
        STAR_TEXT_2: "I don’t like it",
        STAR_TEXT_3: "It’s ok",
        STAR_TEXT_4: "I like it",
        STAR_TEXT_5: "I love it",
        INVALID_ITEM: "Sorry, this item does not exist",
        REASONS_ERROR: "Please select at least one reason for being unhappy with this item. Help us to understand the problem!"
    }), define("pages/rating_experience/nls/ar-AE/labels", {
        Error: "نأسف, لم يتم حفظ التقييم بسبب وجود خطأ ما, الرجاء المحاولة مرة أخرى",
        STAR_TEXT_1: "غير راض تماما",
        STAR_TEXT_2: "لم ينل إعجابي",
        STAR_TEXT_3: "مقبول",
        STAR_TEXT_4: "راض",
        STAR_TEXT_5: "راض تماما",
        INVALID_ITEM: "عفوا, هذا المنتج غير موجود",
        REASONS_ERROR: "الرجاء اختيار سبب واحد على الأقل لعدم رضاك عن هذه السلعه. ساعدنا على فهم المشكلة!"
    }),
    function() {
        var a = !1;
        ! function(b, c) {
            function d() {
                var b = define;
                define = function() {
                    arguments.length && "string" == typeof arguments[0] ? (define = b, a = arguments[0], window.hasAlmondJS ? define.apply(this, arguments) : c()) : (define = b, c())
                }
            }
            b.SouqLoading || (b.hasDefine = "function" == typeof b.define && b.define.amd, b.hasRequireJS = b.hasDefine && "function" == typeof b.require && "object" == typeof b.require.s, b.hasAlmondJS = b.hasDefine && !b.hasRequireJS), b.hasDefine ? (d(), define("jquery-rating", [], c)) : c()
        }(window, function() {
            function b() {
                return "jquery-rating"
            }
            var c = b(),
                d = function() {
                    if (window.hasRequireJS)
                        if (arguments.length && "string" == typeof arguments[0] && arguments[0] === c) {
                            var b = Array.prototype.slice.call(arguments);
                            a && a === c || (a ? b[0] = a : b.shift()), window.define.apply(this, b)
                        } else window.define.apply(this, arguments);
                    else window.define.apply(this, arguments)
                };
            d.amd = {
                jQuery: !0
            }, d("jquery-rating", ["jquery"], function(a) {
                return function(a) {
                    "use strict";
                    var b = {
                        init: function(c) {
                            return this.each(function() {
                                this.self = a(this), b.destroy.call(this.self), this.opt = a.extend(!0, {}, a.fn.raty.defaults, c), b._adjustCallback.call(this), b._adjustNumber.call(this), b._adjustHints.call(this), this.opt.score = b._adjustedScore.call(this, this.opt.score), "img" !== this.opt.starType && b._adjustStarType.call(this), b._adjustPath.call(this), b._createStars.call(this), this.opt.cancel && b._createCancel.call(this), this.opt.precision && b._adjustPrecision.call(this), b._createScore.call(this), b._apply.call(this, this.opt.score), b._setTitle.call(this, this.opt.score), b._target.call(this, this.opt.score), this.opt.readOnly ? b._lock.call(this) : (this.style.cursor = "pointer", b._binds.call(this))
                            })
                        },
                        _adjustCallback: function() {
                            for (var a = ["number", "readOnly", "score", "scoreName", "target"], b = 0; b < a.length; b += 1) "function" == typeof this.opt[a[b]] && (this.opt[a[b]] = this.opt[a[b]].call(this))
                        },
                        _adjustedScore: function(a) {
                            return a ? b._between(a, 0, this.opt.number) : a
                        },
                        _adjustHints: function() {
                            if (this.opt.hints || (this.opt.hints = []), this.opt.halfShow || this.opt.half)
                                for (var a = this.opt.precision ? 10 : 2, b = 0; b < this.opt.number; b += 1) {
                                    var c = this.opt.hints[b];
                                    "[object Array]" !== Object.prototype.toString.call(c) && (c = [c]), this.opt.hints[b] = [];
                                    for (var d = 0; d < a; d += 1) {
                                        var e = c[d],
                                            f = c[c.length - 1];
                                        void 0 === f && (f = null), this.opt.hints[b][d] = void 0 === e ? f : e
                                    }
                                }
                        },
                        _adjustNumber: function() {
                            this.opt.number = b._between(this.opt.number, 1, this.opt.numberMax)
                        },
                        _adjustPath: function() {
                            this.opt.path = this.opt.path || "", this.opt.path && "/" !== this.opt.path.charAt(this.opt.path.length - 1) && (this.opt.path += "/")
                        },
                        _adjustPrecision: function() {
                            this.opt.half = !0
                        },
                        _adjustStarType: function() {
                            var a = ["cancelOff", "cancelOn", "starHalf", "starOff", "starOn"];
                            this.opt.path = "";
                            for (var b = 0; b < a.length; b += 1) this.opt[a[b]] = this.opt[a[b]].replace(".", "-")
                        },
                        _apply: function(a) {
                            b._fill.call(this, a), a && (a > 0 && this.score.val(a), b._roundStars.call(this, a))
                        },
                        _between: function(a, b, c) {
                            return Math.min(Math.max(parseFloat(a), b), c)
                        },
                        _binds: function() {
                            this.cancel && (b._bindOverCancel.call(this), b._bindClickCancel.call(this), b._bindOutCancel.call(this)), b._bindOver.call(this), b._bindClick.call(this), b._bindOut.call(this)
                        },
                        _bindClick: function() {
                            var c = this;
                            c.stars.on("click.raty", function(d) {
                                var e = !0,
                                    f = c.opt.half || c.opt.precision ? c.self.data("score") : this.alt || a(this).data("alt");
                                c.opt.click && (e = c.opt.click.call(c, +f, d)), (e || void 0 === e) && (c.opt.half && !c.opt.precision && (f = b._roundHalfScore.call(c, f)), b._apply.call(c, f))
                            })
                        },
                        _bindClickCancel: function() {
                            var a = this;
                            a.cancel.on("click.raty", function(b) {
                                a.score.removeAttr("value"), a.opt.click && a.opt.click.call(a, null, b)
                            })
                        },
                        _bindOut: function() {
                            var a = this;
                            a.self.on("mouseleave.raty", function(c) {
                                var d = +a.score.val() || void 0;
                                b._apply.call(a, d), b._target.call(a, d, c), b._resetTitle.call(a), a.opt.mouseout && a.opt.mouseout.call(a, d, c)
                            })
                        },
                        _bindOutCancel: function() {
                            var a = this;
                            a.cancel.on("mouseleave.raty", function(c) {
                                var d = a.opt.cancelOff;
                                if ("img" !== a.opt.starType && (d = a.opt.cancelClass + " " + d), b._setIcon.call(a, this, d), a.opt.mouseout) {
                                    var e = +a.score.val() || void 0;
                                    a.opt.mouseout.call(a, e, c)
                                }
                            })
                        },
                        _bindOver: function() {
                            var a = this,
                                c = a.opt.half ? "mousemove.raty" : "mouseover.raty";
                            a.stars.on(c, function(c) {
                                var d = b._getScoreByPosition.call(a, c, this);
                                b._fill.call(a, d), a.opt.half && (b._roundStars.call(a, d, c), b._setTitle.call(a, d, c), a.self.data("score", d)), b._target.call(a, d, c), a.opt.mouseover && a.opt.mouseover.call(a, d, c)
                            })
                        },
                        _bindOverCancel: function() {
                            var a = this;
                            a.cancel.on("mouseover.raty", function(c) {
                                var d = a.opt.path + a.opt.starOff,
                                    e = a.opt.cancelOn;
                                "img" === a.opt.starType ? a.stars.attr("src", d) : (e = a.opt.cancelClass + " " + e, a.stars.attr("class", d)), b._setIcon.call(a, this, e), b._target.call(a, null, c), a.opt.mouseover && a.opt.mouseover.call(a, null)
                            })
                        },
                        _buildScoreField: function() {
                            return a("<input />", {
                                name: this.opt.scoreName,
                                id: this.opt.scoreID,
                                type: "hidden"
                            }).appendTo(this)
                        },
                        _createCancel: function() {
                            var b = this.opt.path + this.opt.cancelOff,
                                c = a("<" + this.opt.starType + " />", {
                                    title: this.opt.cancelHint,
                                    class: this.opt.cancelClass
                                });
                            "img" === this.opt.starType ? c.attr({
                                src: b,
                                alt: "x"
                            }) : c.attr("data-alt", "x").addClass(b), "left" === this.opt.cancelPlace ? this.self.prepend("&#160;").prepend(c) : this.self.append("&#160;").append(c), this.cancel = c
                        },
                        _createScore: function() {
                            var c = a(this.opt.targetScore);
                            this.score = c.length ? c : b._buildScoreField.call(this)
                        },
                        _createStars: function() {
                            for (var c = 1; c <= this.opt.number; c += 1) {
                                var d = b._nameForIndex.call(this, c),
                                    e = {
                                        alt: c,
                                        src: this.opt.path + this.opt[d]
                                    };
                                "img" !== this.opt.starType && (e = {
                                    "data-alt": c,
                                    class: e.src
                                }), e.title = b._getHint.call(this, c), 0 === this.self.children(this.opt.starType).length ? (a("<" + this.opt.starType + " />", e).appendTo(this), this.opt.space && this.self.append(c < this.opt.number ? "&#160;" : "")) : this.self.children(this.opt.starType).eq(c - 1).attr(e)
                            }
                            this.stars = this.self.children(this.opt.starType)
                        },
                        _error: function(b) {
                            a(this).text(b), a.error(b)
                        },
                        _fill: function(a) {
                            for (var c = 0, d = 1; d <= this.stars.length; d += 1) {
                                var e, f = this.stars[d - 1],
                                    g = b._turnOn.call(this, d, a);
                                if (this.opt.iconRange && this.opt.iconRange.length > c) {
                                    var h = this.opt.iconRange[c];
                                    e = b._getRangeIcon.call(this, h, g), d <= h.range && b._setIcon.call(this, f, e), d === h.range && (c += 1)
                                } else e = this.opt[g ? "starOn" : "starOff"], b._setIcon.call(this, f, e)
                            }
                        },
                        _getFirstDecimal: function(a) {
                            var b = a.toString().split(".")[1],
                                c = 0;
                            return b && (c = parseInt(b.charAt(0), 10), "9999" === b.slice(1, 5) && (c += 1)), c
                        },
                        _getRangeIcon: function(a, b) {
                            return b ? a.on || this.opt.starOn : a.off || this.opt.starOff
                        },
                        _getScoreByPosition: function(c, d) {
                            var e = parseInt(d.alt || d.getAttribute("data-alt"), 10);
                            if (this.opt.half) {
                                var f = b._getWidth.call(this);
                                e = e - 1 + parseFloat((c.pageX - a(d).offset().left) / f)
                            }
                            return e
                        },
                        _getHint: function(a, c) {
                            if (0 !== a && !a) return this.opt.noRatedMsg;
                            var d = b._getFirstDecimal.call(this, a),
                                e = Math.ceil(a),
                                f = this.opt.hints[(e || 1) - 1],
                                g = f,
                                h = !c || this.move;
                            return this.opt.precision ? (h && (d = 0 === d ? 9 : d - 1), g = f[d]) : (this.opt.halfShow || this.opt.half) && (d = h && 0 === d ? 1 : d > 5 ? 1 : 0, g = f[d]), "" === g ? "" : g || a
                        },
                        _getWidth: function() {
                            var a = this.stars[0].width || parseFloat(this.stars.eq(0).css("font-size"));
                            return a || b._error.call(this, "Could not get the icon width!"), a
                        },
                        _lock: function() {
                            var a = b._getHint.call(this, this.score.val());
                            this.style.cursor = "", this.title = a, this.score.prop("readonly", !0), this.stars.prop("title", a), this.cancel && this.cancel.hide(), this.self.data("readonly", !0)
                        },
                        _nameForIndex: function(a) {
                            return this.opt.score && this.opt.score >= a ? "starOn" : "starOff"
                        },
                        _resetTitle: function() {
                            for (var a = 0; a < this.opt.number; a += 1) this.stars[a] && (this.stars[a].title = b._getHint.call(this, a + 1))
                        },
                        _roundHalfScore: function(a) {
                            var c = parseInt(a, 10),
                                d = b._getFirstDecimal.call(this, a);
                            return 0 !== d && (d = d > 5 ? 1 : .5), c + d
                        },
                        _roundStars: function(a, c) {
                            var d, e = (a % 1).toFixed(2);
                            if (c || this.move ? d = e > .5 ? "starOn" : "starHalf" : e > this.opt.round.down && (d = "starOn", this.opt.halfShow && e < this.opt.round.up ? d = "starHalf" : e < this.opt.round.full && (d = "starOff")), d) {
                                var f = this.opt[d],
                                    g = this.stars[Math.ceil(a) - 1];
                                b._setIcon.call(this, g, f)
                            }
                        },
                        _setIcon: function(a, b) {
                            a["img" === this.opt.starType ? "src" : "className"] = this.opt.path + b
                        },
                        _setTarget: function(a, b) {
                            b && (b = this.opt.targetFormat.toString().replace("{score}", b)), a.is(":input") ? a.val(b) : a.html(b)
                        },
                        _setTitle: function(a, c) {
                            if (a) {
                                var d = parseInt(Math.ceil(a), 10);
                                this.stars[d - 1].title = b._getHint.call(this, a, c)
                            }
                        },
                        _target: function(c, d) {
                            if (this.opt.target) {
                                var e = a(this.opt.target);
                                e.length || b._error.call(this, "Target selector invalid or missing!");
                                var f = d && "mouseover" === d.type;
                                if (void 0 === c) c = this.opt.targetText;
                                else if (null === c) c = f ? this.opt.cancelHint : this.opt.targetText;
                                else {
                                    "hint" === this.opt.targetType ? c = b._getHint.call(this, c, d) : this.opt.precision && (c = parseFloat(c).toFixed(1));
                                    var g = d && "mousemove" === d.type;
                                    f || g || this.opt.targetKeep || (c = this.opt.targetText)
                                }
                                b._setTarget.call(this, e, c)
                            }
                        },
                        _turnOn: function(a, b) {
                            return this.opt.single ? a === b : a <= b
                        },
                        _unlock: function() {
                            this.style.cursor = "pointer", this.removeAttribute("title"), this.score.removeAttr("readonly"), this.self.data("readonly", !1);
                            for (var a = 0; a < this.opt.number; a += 1) this.stars[a].title = b._getHint.call(this, a + 1);
                            this.cancel && this.cancel.css("display", "")
                        },
                        cancel: function(c) {
                            return this.each(function() {
                                var d = a(this);
                                !0 !== d.data("readonly") && (b[c ? "click" : "score"].call(d, null), this.score.removeAttr("value"))
                            })
                        },
                        click: function(c) {
                            return this.each(function() {
                                !0 !== a(this).data("readonly") && (c = b._adjustedScore.call(this, c), b._apply.call(this, c), this.opt.click && this.opt.click.call(this, c, a.Event("click")), b._target.call(this, c))
                            })
                        },
                        destroy: function() {
                            return this.each(function() {
                                var b = a(this),
                                    c = b.data("raw");
                                c ? b.off(".raty").empty().css({
                                    cursor: c.style.cursor
                                }).removeData("readonly") : b.data("raw", b.clone()[0])
                            })
                        },
                        getScore: function() {
                            var a, b = [];
                            return this.each(function() {
                                a = this.score.val(), b.push(a ? +a : void 0)
                            }), b.length > 1 ? b : b[0]
                        },
                        move: function(c) {
                            return this.each(function() {
                                var d = parseInt(c, 10),
                                    e = b._getFirstDecimal.call(this, c);
                                d >= this.opt.number && (d = this.opt.number - 1, e = 10);
                                var f = b._getWidth.call(this),
                                    g = f / 10,
                                    h = a(this.stars[d]),
                                    i = h.offset().left + g * e,
                                    j = a.Event("mousemove", {
                                        pageX: i
                                    });
                                this.move = !0, h.trigger(j), this.move = !1
                            })
                        },
                        readOnly: function(c) {
                            return this.each(function() {
                                var d = a(this);
                                d.data("readonly") !== c && (c ? (d.off(".raty").children("img").off(".raty"), b._lock.call(this)) : (b._binds.call(this), b._unlock.call(this)), d.data("readonly", c))
                            })
                        },
                        reload: function() {
                            return b.set.call(this, {})
                        },
                        score: function() {
                            var c = a(this);
                            return arguments.length ? b.setScore.apply(c, arguments) : b.getScore.call(c)
                        },
                        set: function(b) {
                            return this.each(function() {
                                a(this).raty(a.extend({}, this.opt, b))
                            })
                        },
                        setScore: function(c) {
                            return this.each(function() {
                                !0 !== a(this).data("readonly") && (c = b._adjustedScore.call(this, c), b._apply.call(this, c), b._target.call(this, c))
                            })
                        }
                    };
                    a.fn.raty = function(c) {
                        return b[c] ? b[c].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof c && c ? void a.error("Method " + c + " does not exist!") : b.init.apply(this, arguments)
                    }, a.fn.raty.defaults = {
                        cancel: !1,
                        cancelClass: "raty-cancel",
                        cancelHint: "Cancel this rating!",
                        cancelOff: "cancel-off",
                        cancelOn: "cancel-on",
                        cancelPlace: "left",
                        click: void 0,
                        half: !1,
                        halfShow: !0,
                        hints: ["", "", "", "", ""],
                        iconRange: void 0,
                        mouseout: void 0,
                        mouseover: void 0,
                        noRatedMsg: "Not rated yet!",
                        number: 5,
                        numberMax: 20,
                        path: void 0,
                        precision: !1,
                        readOnly: !1,
                        round: {
                            down: .25,
                            full: .6,
                            up: .76
                        },
                        score: void 0,
                        scoreName: "score",
                        scoreID: "score",
                        single: !1,
                        space: !0,
                        starHalf: "fi-star half",
                        starOff: "fi-star off",
                        starOn: "fi-star on",
                        starType: "i",
                        target: void 0,
                        targetFormat: "{score}",
                        targetKeep: !1,
                        targetScore: void 0,
                        targetText: "",
                        targetType: "hint"
                    }
                }(a), a
            });
            var c = b();
            if (void 0 === c) throw new Error((a || "Component") + " Build Error : no identifer detected!");
            if (window.hasDefine) {
                if (window.hasAlmondJS) return window.require(c)
            } else require(c)
        })
    }(), define("components/ratingPopup", ["souq", "i18n!../pages/rating_experience/nls/labels", "../pages/rating_experience/nls/ar-AE/labels", "jquery-rating"], function(a, b) {
        var c = new a.Component("ratingPopup"),
            d = c.dom;
        return c.run = function() {
            var a = !0;
            d(".reveal").each(function() {
                d(this).is(":visible") && (a = !1)
            }), a && c.getNextRateableProduct()
        }, c.getNextRateableProduct = function() {
            new a.API.Request("rating_experience.php", "get_next_rateable_product", "POST").send({
                data: {
                    source: "ratingPopup",
                    hitsCfs: d("#hitsCfs").val(),
                    hitsCfsMeta: d("#hitsCfsMeta").val(),
                    r_url: window.location.href
                },
                beforeSend: function() {},
                success: function(a) {
                    a.success && (d("#rate_your_experience_popup").replaceWith(a.sBody), d("#rate_your_experience_popup").foundation().foundation("open"), c.rateYourExperience())
                },
                complete: function() {},
                error: function() {
                    a.log.warn(new Error("failed to get next rateable product"))
                }
            })
        }, c.rateYourExperience = function() {
            d(".star-rating-container").each(function() {
                d(this).find(".star-rating").raty({
                    score: d("#popup-score").val(),
                    cancel: !0,
                    click: function(a) {
                        d(".reasons-error").addClass("hide"), d("#popup-score").val(a), d(".rating-box").find(".star-rating").raty("score", a), d(".rating-msg").hide(), c.startStatus(a), d(".star-status").css("display", "inline-block"), d(".rating-action").removeClass("hide"), c.displayReasons(a), d("#rating-main").hide(), d("#rating-secondary").removeClass("hide")
                    },
                    mouseover: function(a) {
                        c.startStatus(a)
                    },
                    mouseout: function() {
                        c.startStatus(parseInt(d("#popup-score").val()))
                    }
                }), d(this).find(".rating-action").click(function() {
                    d(".star-rating-container").find(".star-rating").raty("cancel", !0), d("#popup-score").val(0), d("#rating-main").show(), d("#rating-secondary").addClass("hide"), d(".rating-msg").show(), d(".star-status-text").html(""), d(".rating-action").addClass("hide")
                })
            }), d("#save-rating").click(function() {
                c.saveRating()
            })
        }, c.startStatus = function(a) {
            switch (a) {
                case 1:
                    d(".star-status-text").html(b.STAR_TEXT_1);
                    break;
                case 2:
                    d(".star-status-text").html(b.STAR_TEXT_2);
                    break;
                case 3:
                    d(".star-status-text").html(b.STAR_TEXT_3);
                    break;
                case 4:
                    d(".star-status-text").html(b.STAR_TEXT_4);
                    break;
                case 5:
                    d(".star-status-text").html(b.STAR_TEXT_5);
                    break;
                default:
                    d(".star-status-text").html("")
            }
        }, c.displayReasons = function(a) {
            d(".rating-reasons-container").removeClass("hide"), a > 0 && a < 4 ? (c.getReasons(), d(".happy-title").addClass("hide"), d(".unhappy-title").removeClass("hide"), d(".rating-reasons").removeClass("hide")) : (d(".unhappy-title").addClass("hide"), d(".happy-title").removeClass("hide"), d(".rating-reasons").addClass("hide"))
        }, c.getReasons = function() {
            new a.API.Request("rating_experience.php", "bad_reasons", "POST").send({
                data: {
                    source: "ratingPopup",
                    item_id: d("#id_item").val(),
                    hitsCfs: d("#hitsCfs").val(),
                    hitsCfsMeta: d("#hitsCfsMeta").val(),
                    r_url: window.location.href
                },
                beforeSend: function() {
                    d(".unhappy-case-container").addClass("loading")
                },
                success: function(a) {
                    a.success && (d(".connections-list").replaceWith(a.sBody), d(".rating-reasons").removeClass("hide"))
                },
                complete: function() {
                    d(".unhappy-case-container").removeClass("loading")
                },
                error: function() {
                    a.log.warn(new Error("failed to get rating experience reasons"))
                }
            })
        }, c.saveRating = function() {
            d(".reasons-error").addClass("hide");
            var c = new a.API.Request("rating_experience.php", "save_rating_experience", "POST"),
                e = "",
                f = d("#popup-score").val();
            f < 4 && f > 0 && d(".valign-mid:checked").each(function() {
                "" !== e && (e += ","), e += d(this).val()
            }), f > 3 && f < 6 || f < 4 && f > 0 && "" !== e ? c.send({
                data: {
                    source: "ratingPopup",
                    id_order: d("#id_order").val(),
                    id_item: d("#id_item").val(),
                    item_score: f,
                    review_text: d(".review-text").val(),
                    reasons: e,
                    hitsCfs: d("#hitsCfs").val(),
                    hitsCfsMeta: d("#hitsCfsMeta").val(),
                    r_url: window.location.href
                },
                beforeSend: function() {
                    d("#rating-secondary").addClass("loading")
                },
                success: function(a) {
                    a.success ? (d("#rating-secondary").addClass("hide"), d(".thank-you-container").removeClass("hide"), f > 3 && f < 6 ? (d(".thank-you-text").show(), d(".sorry-text").hide()) : (d(".thank-you-text").hide(), d(".sorry-text").show())) : (d(".rating-form").find(".reasons-error").html(b.Error), d(".rating-form").find(".reasons-error").removeClass("hide"))
                },
                complete: function() {
                    d("#rating-secondary").removeClass("loading")
                },
                error: function() {
                    d(".rating-form").find(".reasons-error").html(b.Error), d(".rating-form").find(".reasons-error").removeClass("hide"), a.log.warn(new Error("failed to save rating experience"))
                }
            }) : (d(".rating-reasons-container").find(".reasons-error").html(b.REASONS_ERROR), d(".rating-reasons-container").find(".reasons-error").removeClass("hide"))
        }, c
    }), define("home", ["souq", "components/dropdown", "components/multi-tabs-curation", "components/slider", "components/mobileLeadAcquisition", "components/mobileVerificaion-v1", "components/ratingPopup", "jquery-rating"], function(a, b, c, d, e, f, g) {
        function h(a) {
            var b = j("#md" + a),
                c = j("a[data-dropdown=md" + a + "]");
            b.on("opened.fndtn.dropdown", function() {
                c.addClass("active");
                var a = b.find(".before"),
                    d = b.find(".after"),
                    e = c.parent("li").position();
                a.length && a.css("top", e.top + 13), d.length && d.css("top", e.top + 12)
            }), b.on("closed.fndtn.dropdown", function() {
                c.removeClass("active")
            })
        }
        var i = new a.Page("homePage"),
            j = i.dom,
            k = [];
        return i.run = function() {
            this.base(), b.run({
                DropdownId: "tabsDropdown",
                selector: "ul[id^=tabsDropdownLarge] li a",
                attrId: "data-id-box"
            }), c.run(), j("#leadAcquisitionContainer").length && e.run()
        }, i.ready = function() {
            if (this.sandbox.subscribe("dropdown:click", function(a, b, c, d) {
                    this.loadContent(b, c, d)
                }.bind(this)), !a.utils.flags().hasTouch)
                for (var b = 1; b <= 10; b += 1) h(b);
            if (j("ul.tabs li a").click(function() {
                    var a = j(this).closest(".filter-tabs-container").find(".tabs-content"),
                        b = a.find(".content"),
                        c = j(this).attr("data-id-box");
                    i.loadContent(a, b, c)
                }), f.run(), j("#can_show_popup").val() && setTimeout(function() {
                    g.run()
                }, 1e3), "souq:ae:HomePage" === window.page_name && 0 === window.s_isUserLoggedIn && "mobile" !== window.globals.actualDevice && "tablet" !== window.globals.actualDevice) {
                j(".souq-top-bar .account-list").append('<div class="user-menu login-tooltip"><ul></ul></div>');
                var c = j(".souq-top-bar .login-user-name .flyout.user-menu .filter-group").find(".login-li.row").clone();
                j(c).find("a.login-button").attr("href", j(c).find("a.login-button").attr("href") + "?src=tooltip"), j(c).find("a.login_to_register").attr("href", j(c).find("a.login_to_register").attr("href") + "?src=tooltip"), j(c).fadeIn(600).appendTo(".souq-top-bar .account-list .login-tooltip ul"), j(".account-list .login-tooltip").prepend('<i>&#x25B2;</i> <a class="close-tooltip"><i class="fa fa-times"></i></a>'), j(".account-list .login-tooltip ul li.login-li > div:first-of-type").removeClass("small-12").addClass("small-10 small-offset-1"), "mobile" === window.globals.device && (j(".souq-top-bar .account-list .login-tooltip").appendTo(".header-middle-row > .row > .mobile-menu .login-user-name"), j(".login-user-name .login-tooltip .close-tooltip, .header-middle-row .login-user-name .userNameField").click(function() {
                    j(".login-user-name .login-tooltip").detach()
                })), j(".souq-top-bar .login-user-name .userNameField").hover(function() {
                    j(".account-list .login-tooltip").detach(), j(".login-user-name").addClass("show-list"), j(".login-user-name .flyout.user-menu").removeClass("hidden")
                }), j(".login-tooltip .close-tooltip").click(function() {
                    j(".account-list .login-tooltip").detach(), j(".login-user-name").addClass("show-list"), j(".login-user-name .flyout.user-menu").toggleClass("hidden")
                }), j(".souq-top-bar .login-user-name").find(".login-tooltip").get(0) || (j(".souq-top-bar .login-user-name").addClass("show-list"), j(".souq-top-bar .login-user-name .flyout.user-menu").removeClass("hidden")), "tablet" === window.globals.actualDevice && (j('<div class="show-menu-list"></div>').appendTo(".souq-top-bar .account-list"), j(".login-user-name .flyout.user-menu").addClass("hidden"), j(".login-user-name").removeClass("show-list"), j(".show-menu-list").click(function() {
                    j(".account-list .login-tooltip").detach(), j(".login-user-name .flyout.user-menu").toggleClass("hidden"), j(".login-user-name").toggleClass("show-list active")
                }));
                var d = a.tracking.getTracker();
                d && d.t ? (d.linkTrackVars = "events", d.linkTrackEvents = d.events = "event23", d.tl(this, "o", "Show a tool tip for non logged in users"), d.events = d.linkTrackEvents = d.linkTrackVars = null) : a.log.logError(new Error("Omniture (s) not defined!")), window.setTimeout(function() {
                    j(".login-tooltip").remove()
                }, 25e3)
            } else j(".souq-top-bar .login-user-name").addClass("show-list"), j(".souq-top-bar .login-user-name .flyout.user-menu").removeClass("hidden")
        }, i.loadContent = function(b, c, e) {
            if (k[e] || void 0 === e) b.css("min-height", ""), c.html(k[e]), c.foundation(), d.startSliders(c, null, !0);
            else {
                var f, g, h;
                e.split("-").length > 1 ? (f = "index.php", g = "items-box", h = {
                    data: e,
                    no_of_boxes_large: 7,
                    abParam: a.utils.urlParam("ab")
                }) : (e = e.replace("categories_", ""),
                    f = "index.php?dispatch=ajax_data.tabs",
                    g = "box",
                    h = {
                        id_cms_box: e,
                        no_of_boxes_large: 7,
                        dispatcher: a.getConfig("controller"),
                        abParam: a.utils.urlParam("ab")
                    }
                );

                var i = new a.API.Request(f, g);
                i.dataType = "html", i.send({
                    data: h,
                    beforeSend: function() {
                        b.block()
                    },
                    complete: function() {
                        b.unblock(), b.css("min-height", "")
                    },
                    success: function(a) {
                        b.css("min-height", ""), k[e] = a, c.html(a), c.foundation(), d.startSliders(c, null, !0)
                    },
                    error: function() {
                        a.log.warn(new Error("failed to load CMS box Content"))
                    }
                })
            }
        }, i
    });