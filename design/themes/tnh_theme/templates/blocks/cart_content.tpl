{*{assign var="dropdown_id" value=$block.snapping_id}
{assign var="r_url" value=$config.current_url|escape:url}
{hook name="checkout:cart_content"}
<div class="ty-dropdown-box" id="cart_status_{$dropdown_id}">
<div id="sw_dropdown_{$dropdown_id}" class="ty-dropdown-box__title cm-combination">
<a href="{"checkout.cart"|fn_url}">
{hook name="checkout:dropdown_title"}
<i class="ty-minicart__icon ty-icon-moon-commerce{if $smarty.session.cart.amount} filled{else} empty{/if}"></i>
<span class="ty-minicart-title{if !$smarty.session.cart.amount} empty-cart{/if} ty-hand">
<span class="ty-block ty-minicart-title__header ty-uppercase">{__("my_cart")}</span>
<span class="ty-block">
{if $smarty.session.cart.amount}
{$smarty.session.cart.amount}&nbsp;{__("items")} {__("for")}&nbsp;{include file="common/price.tpl" value=$smarty.session.cart.display_subtotal}
{else}
{__("cart_is_empty")}
{/if}
</span>
</span>
{/hook}
</a>
</div>
<div id="dropdown_{$dropdown_id}" class="cm-popup-box ty-dropdown-box__content hidden">
{hook name="checkout:minicart"}
<div class="cm-cart-content {if $block.properties.products_links_type == "thumb"}cm-cart-content-thumb{/if} {if $block.properties.display_delete_icons == "Y"}cm-cart-content-delete{/if}">
<div class="ty-cart-items">
{if $smarty.session.cart.amount}
<ul class="ty-cart-items__list">
{hook name="index:cart_status"}
{assign var="_cart_products" value=$smarty.session.cart.products|array_reverse:true}
{foreach from=$_cart_products key="key" item="product" name="cart_products"}
{hook name="checkout:minicart_product"}
{if !$product.extra.parent}
<li class="ty-cart-items__list-item">
{hook name="checkout:minicart_product_info"}
{if $block.properties.products_links_type == "thumb"}
<div class="ty-cart-items__list-item-image">
{include file="common/image.tpl" image_width="40" image_height="40" images=$product.main_pair no_ids=true}
</div>
{/if}
<div class="ty-cart-items__list-item-desc">
<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{$product.product_id|fn_get_product_name nofilter}</a>
<p>
<span>{$product.amount}</span><span>&nbsp;x&nbsp;</span>{include file="common/price.tpl" value=$product.display_price span_id="price_`$key`_`$dropdown_id`" class="none"}
</p>
</div>
{if $block.properties.display_delete_icons == "Y"}
<div class="ty-cart-items__list-item-tools cm-cart-item-delete">
{if (!$runtime.checkout || $force_items_deletion) && !$product.extra.exclude_from_calculate}
{include file="buttons/button.tpl" but_href="checkout.delete.from_status?cart_id=`$key`&redirect_url=`$r_url`" but_meta="cm-ajax cm-ajax-full-render" but_target_id="cart_status*" but_role="delete" but_name="delete_cart_item"}
{/if}
</div>
{/if}
{/hook}
</li>
{/if}
{/hook}
{/foreach}
{/hook}
</ul>
{else}
<div class="ty-cart-items__empty ty-center">{__("cart_is_empty")}</div>
{/if}
</div>

{if $block.properties.display_bottom_buttons == "Y"}
<div class="cm-cart-buttons ty-cart-content__buttons buttons-container{if $smarty.session.cart.amount} full-cart{else} hidden{/if}">
<div class="ty-float-left">
<a href="{"checkout.cart"|fn_url}" rel="nofollow" class="ty-btn ty-btn__secondary">{__("view_cart")}</a>
</div>
{if $settings.General.checkout_redirect != "Y"}
<div class="ty-float-right">
<a href="{"checkout.checkout"|fn_url}" rel="nofollow" class="ty-btn ty-btn__primary">{__("checkout")}</a>
</div>
{/if}
</div>
{/if}

</div>
{/hook}
</div>
<!--cart_status_{$dropdown_id}--></div>
{/hook}
*}

<div class="mobile-menu show-for-small-only small-7 columns">
    <div id="mini-cart_mobile" class="mini-cart mini-cart_mobile_aj with-flyout" data-xleft-items="0" data-flyout-content>
        <a href="/ae-en/shopping_cart.php" class="disabled" data-options="align:left" data-flyout="cartDrop_mobile"><i class='fi-cart'></i> <span class='cart-item-count-aj show-mini-cart-aj cart-icon-item-count hide'></span></a>  
        <div id="cartDrop_mobile" class="cart-dropdown flyout  cartDropItem" data-dropdown-content></div>
    </div>
    <ul class="clearfix">
        <li class="mobile-search-button ">
            <i class="fi-magnifying-glass"></i>
        </li>
        <li id="login_user_name_mobile" class="login-user-name ellipsis-loading with-flyout user_login" data-flyout-content>
            <a id="userNameField_mobile" href="javascript:void(0);" class="userNameField truncate" data-options="align:bottom" data-flyout="userName_mobile"></a>
            <div class="flyout user-menu">
                <ul id="userName_mobile" class="filter-group">
                    <li  class="login-li row loginRemoval">
                        <div class="small-12 columns">
                            <a href="/ae-en/login.php" class="login-button" local="">Log In</a>	</div>
                        <div class="to-sign-up small-12 columns">
                            <span class="register-now">Don't have an account?			<a href="/ae-en/register.php" class="login_to_register">Sign Up</a>		</span>
                        </div>

                    </li>

                    <li><a href="/ae-en/account.php" class="change-if-seller">My Orders</a>	</li>
                    <li><a href="/ae-en/account_addresses.php">My Addresses</a>	</li>
                    <li><a href="/ae-en/account_lists.php">Wish Lists</a>	</li>
                    <li><a href="/ae-en/account_settings.php">Account Settings</a>	</li>
                    <li class="toRemove"><a href="/ae-en/account_overview.php">Account Summary</a>	</li>
                    <li>	</li>

                    <li class="toRemove"><a href="/ae-en/logout.php">Logout</a>	</li>	
                </ul>
            </div>
        </li>
    </ul>
</div>