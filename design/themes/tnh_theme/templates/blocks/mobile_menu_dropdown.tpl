{hook name="blocks:topmenu_dropdown"}
{*<pre>{$items|@print_r}</pre>*}
<div id="appboy-newsfeed-cont" show-newsfeed-attr="1">
    <i class="close-button close-newsfeed">×</i>
</div>
                <aside class="main-side-canvas off-canvas position-left" id="offCanvasLeft" data-off-canvas data-position="left" data-close-on-click="true">
                    <ul class="off-canvas-list mobile-ofc vertical menu">
                        <li>
                            <dl class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                                <dt class="font-normal"><a href="" class="level2">Home</a></dt>
                                <dt class="font-normal"><a href="" class="level2">Deals</a></dt>
                                <dt class="font-normal"><a href="" class="level2">Track Orders</a></dt>
                                <dt class="font-normal"><a href="" class="level2">My Account</a></dt>
                                <dt class="font-normal"><a href="" class="level2">Contact Us</a></dt>
                                <dt class="font-normal">
                                    <a id="appboy-newsfeed">
                                        Newsfeed <span id="appboy-newsfeed-counter" style="display: none;"></span>
                                    </a>
                                </dt>
                            </dl>
                        </li>
                        <li><label>Categories</label></li>
                        <li>
                            <dl class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                                <dd class="accordion-navigation accordion-item" data-accordion-item>
                                    <a href="#" class="level2 accordion-title tr-enabled" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Souq Fashion <i></i></a>
                                    <div id="panel6" class="content accordion-content" data-tab-content>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">New Arrivals</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Women</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Men</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Handbags</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Watches</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Sports</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Kids</a>
                                        <a href="" class="tr-enabled" prefix="1" data-tr-page-name="HP" data-tr-box-type="MarketingBox" data-tr-campaign-date="" data-tr-campaign-name="" data-tr-landing-page-name="" data-tr-landing-page-url="">Outlet</a>
                                    </div>
                                </dd>
                                <dt class="font-normal">
                                    <a href="/ae-en/advanced_search.php" class="level2">Advanced Search</a>
                                </dt>
                            </dl>
                        </li>
                    </ul>
                </aside>

                <div class="js-off-canvas-exit"></div>
                <!-- //end -->
{/hook}