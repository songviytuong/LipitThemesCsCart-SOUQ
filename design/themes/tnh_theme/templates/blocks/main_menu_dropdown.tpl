{hook name="blocks:topmenu_dropdown"}
{*<pre>{$items|@print_r}</pre>*}
{if $items}
    <ul id="megaMenuNav" class="small-12 menu-list dropdown menu" data-dropdown-menu="menu-0" data-hover-delay="300" data-closing-time="0">
        {foreach from=$items item="item1" name="foo"}
            {assign var="item1_url" value=$item1|fn_form_dropdown_object_link:$block.type}
            {assign var="unique_elm_id" value=$item1_url|md5}
            {assign var="unique_elm_id" value="topmenu_`$block.block_id`_`$unique_elm_id`"}
            
            {if ($smarty.foreach.foo.first)}
                <li class="level0 menu-link no-child">
                    <a href="{$item1_url}" class="menu-tr-enabled" data-menu-id="menu-{$item1.param_id}" data-tr-menu-name="{__("all_categories")}" data-tr-menu-pos="1" data-tr-box-type="Flyout">{__("all_categories")}</a>
                </li>
            {/if}
            {if ($item1.param_2 == 'brand')}
                <li class="level0 menu-link is-dropdown-submenu-parent">
                    <a href="" data-menu-id="menu-7">{$item1.item}</a>
                    <ul class="menu-content center-menu-content small-12 columns">
                        <div class="small-7 columns" style="margin-top: 25px;">
                            <!--------------------- Row 01 --------------------->
                            <div class="row">
                                <!-- Logo 01 -->    
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onClick="pushData('internal_promotions', 'Motorola Flyout', 'Flyout-Motorola-Brandstore 1807/2017', '/ae-en/motorola-uae/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'Motorola|Flyout|20160714|Flyout-Motorola-Brandstore'; s.eVar20 = 'Motorola|Flyout|20160714|Flyout-Motorola-Brandstore'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/motorola-uae/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/logos/flyout/motorola-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Motorola"/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 02 -->        
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onClick="pushData('internal_promotions', 'CCC-Samsung Flyout', 'CCC-Samsung-Brandstore 25/01/2017', '/ae-en/samsung-store/p/'); s.linkTrackVars = 'eVar3,eVar20'; s.eVar3 = 'CCC-Samsung|Flyout|20160515|CCC-Samsung'; s.eVar20 = 'CCC-Samsung|Flyout|20160509|CCC-Samsung'; s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/samsung-store/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2016_LP/brand-stores-cms/logo/samsung-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Samsung"/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 03 -->        
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onclick="pushData('internal_promotions', 'OnePlus FlyOut', 'OnePlus-Brandstore 3001/2017', '/ae-en/oneplus-store/p/'); s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'OnePlus|FlyOut|080617|OnePlus BrandStore';
                                                                        s.eVar20 = 'OnePlus|FlyOut|080617|OnePlusBrandStore';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/oneplus-store/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/flyout/logos/oneplus.png" style="border-bottom: 1px solid #e8ebee;" alt="OnePlus"/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 04 -->
                                <div class="medium-3 columns bottom-padding">
                                    <a onclick="pushData('internal_promotions', 'Dell FlyOut', 'Dell-Brandstore 3001/2017', '/ae-en/dellstore-uae/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Dell|FlyOut|030216|Dell-BrandStore';
                                                                        s.eVar20 = 'Dell|FlyOut|030216|GoPro-BrandStore';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/dellstore-uae/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/dell-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Dell"/> <!--image path-->
                                    </a>
                                </div>
                            </div>
                            <!--------------------- Row 02 --------------------->
                            <div class="row">
                                <!-- Logo 04 -->
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onclick="pushData('internal_promotions', 'Cerelac FlyOut', 'Cerelac-Brandstore 3001/2017', '/ae-en/cerelac/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Cerelac|FlyOut|030216|CerelacBrandStore';
                                                                        s.eVar20 = 'Cerelac|FlyOut|030216|CerelacBrandStore';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/cerelac/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/cerelac-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Cerelac"/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 04 -->
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onClick="pushData('internal_promotions', 'Vimto Flyout', 'Vimto-Brandstore 1605/2017', '/ae-en/aujancocacola/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Vimto|Flyout|20161008|Purina';
                                                                        s.eVar20 = 'Vimto|Flyout|20170516|Vimto';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/aujancocacola/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/logos/flyout/vimto-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Vimto"/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 04 -->
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onclick="pushData('internal_promotions', 'Philips Flyout', 'Philips-Brandstore 3001/2017', '/ae-en/philips/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Philips|Flyout|20161026|Philips';
                                                                        s.eVar20 = 'Philips|Flyout|20161026|Philips';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/philips/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/philips-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Philips"/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 04 -->
                                <div class="medium-3 columns bottom-padding">
                                    <a onClick="pushData('internal_promotions', 'Pepsi Flyout', 'Pepsi-Brandstore 3001/2017', '/ae-en/pepsicostore/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Pepsi|Flyout|20160515|Pepsi';
                                                                        s.eVar20 = 'Pepsi|Flyout|20160509|Pepsi';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/pepsicostore/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/pepsi-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Pepsi"/> <!--image path-->
                                    </a>
                                </div>
                            </div>
                            <!--------------------- Row 03 --------------------->
                            <div class="row">
                                <!-- Logo 05 -->
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onclick="pushData('internal_promotions', 'Microsoft FlyOut', 'Microsoft-Brandstore 1807/2017', '/ae-en/microsoft-store/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Microsoft|FlyOut|030216|Microsoft-BrandStore';
                                                                        s.eVar20 = 'Microsoft|FlyOut|180717|Xiaomi-BrandStore';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/microsoft-store/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2016_LP/brand-stores-cms/logo/microsoft-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Microsoft "/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 06 -->      
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onClick="pushData('internal_promotions', 'Marina Flyout', 'Marina-Brandstore 02/04/2017', '/ae-en/marina-home-outlet/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Marina|Flyout|20161008|Brandstore-Marina';
                                                                        s.eVar20 = 'Marina|Flyout|20161008|Brandstore-Marina';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/marina-home-outlet/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/FlyLogo.png" style="border-bottom: 1px solid #e8ebee;" alt="Marina "/> <!--image path-->
                                    </a>
                                </div>
                                <!-- Logo 07 -->
                                <div class="medium-3 columns bottom-padding" style="border-right: 1px solid #e8ebee;">
                                    <a onclick="pushData('internal_promotions', 'Puma FlyOut', 'Puma-Brandstore 3001/2017', '/ae-en/only-uae/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Puma|FlyOut|030216|Puma-BrandStore';
                                                                        s.eVar20 = 'Forever 21|FlyOut|030216|Puma-BrandStore';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/puma-uae/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2016_LP/brand-stores-cms/logo/puma-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Puma"/> <!--image path-->
                                    </a>
                                </div>


                                <!-- Logo 08 -->    
                                <div class="medium-3 columns bottom-padding">
                                    <a onclick="pushData('internal_promotions', 'Only Flyout', 'Only-Brandstore 3001/2017', '/ae-en/Only-uae/p/');
                                                                        s.linkTrackVars = 'eVar3,eVar20';
                                                                        s.eVar3 = 'Only|Flyout|20161008|Brandstore-Only';
                                                                        s.eVar20 = 'Only|Flyout|20161008|Brandstore-Only';
                                                                        s.tl(this, 'o', 'Internal Campaign Tracking');" href="/ae-en/Only-uae/p/">
                                        <img src="templates/public/style/img/blank.gif" data-src="https://cms.souqcdn.com/spring/cms/en/ae/2017_LP/brand-stores/logo/flyout/only02-flyout-logo.png" style="border-bottom: 1px solid #e8ebee;" alt="Only"/> <!--image path-->
                                    </a>
                                </div>
                            </div>    
                            <!-- Row 04 - Shop All -->	
                            <div class="row" style="text-align:center;">
                                <ul>
                                    <li>             
                                        <a onClick="pushData('internal_promotions', 'Shop All Brands Flyout', 'Shop All Brands-brandstore 3001/2017', '/ae-en/ae-brand-stores/c/');
                                                                            s.linkTrackVars = 'eVar3,eVar20';
                                                                            s.eVar3 = 'Shop All Brands|Flyout|20161023|brandstore-Shop All Brands';
                                                                            s.eVar20 = 'Shop All Brands|Flyout|20161023|brandstore-Shop All Brands';
                                                                            s.tl(this, 'o', 'Internal Campaign Tracking');" 
                                           href="/ae-en/ae-brand-stores/c/">
                                            <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/en-btn-flyout.png" alt="Shop All Brands"></a>           
                                    </li>
                                </ul>
                            </div>
                            <!-- End Row 04 - Shop All -->	         
                        </div>
                        <div class="small-5 columns">
                            <a onClick="pushData('internal_promotions', 'Motorola July 18 Flyout', 'Motorola-brandstore 3001/2017', '/ae-en/ae-motorola-g5plus/c/');
                                                                s.linkTrackVars = 'eVar3,eVar20';
                                                                s.eVar3 = 'Motorola|Flyout|20161023|brandstore-Motorola';
                                                                s.eVar20 = 'Motorola|Flyout|20161023|brandstore-Motorola';
                                                                s.tl(this, 'o', 'Internal Campaign Tracking');" 
                               href="/ae-en/ae-motorola-g5plus/c/">
                                <img src="templates/public/style/img/blank.gif" data-src="https://souqcms.s3.amazonaws.com/spring/cms/en/ae/2017_LP/motorola/g5plus/moto-g5-flyout.jpg" alt="Moto G5 Plus"/> <!--image path-->
                            </a>
                        </div>
                    </ul>
                </li>
                
            {else}
                {if $item1.$childs|count > 0}
                    {assign var="submenu" value="is-dropdown-submenu-parent"}
                    {assign var="isparent" value="parent"}
                {/if}
            <li class="level0 menu-link {if $isparent == 'parent'}{$submenu|cat:' fmcg-menu-link'}{else}{$submenu}{/if}">
                <a href="{$item1_url}">{$item1.item}</a>
                {if $item1.$childs|count > 0}
                    <ul class="{if $isparent == 'parent'}fmcg-menu-content {/if}type-menu-content menu menu-content small-12 columns">
                        <div class="level1-container">
                            <div class="row">
                                <ul id="megaMenuNav" class="small-12 menu-list dropdown menu" data-dropdown-menu="menu-1" data-hover-delay="300" data-closing-time="0">
                                    {foreach from=$item1.$childs item="item2"}
                                        {assign var="item2_url" value=$item2|fn_form_dropdown_object_link:$block.type}
                                        <li class="level1 menu-link">
                                            <a href="{$item2_url}" class="menu-tr-enabled" data-menu-id="menu-0" data-tr-menu-name="Beverages" data-tr-menu-pos="1" data-tr-box-type="Flyout">{$item2.item}</a>
                                        </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </ul>
                {/if}
            </li>
            {/if}
        {/foreach}
    </ul>
{/if}
{/hook}

{*<ul class="ty-menu__items cm-responsive-menu">
        {hook name="blocks:topmenu_dropdown_top_menu"}
            <li class="ty-menu__item ty-menu__menu-btn visible-phone">
                <a class="ty-menu__item-link">
                    <i class="ty-icon-short-list"></i>
                    <span>{__("menu")}</span>
                </a>
            </li>

        {foreach from=$items item="item1" name="item1"}
            {assign var="item1_url" value=$item1|fn_form_dropdown_object_link:$block.type}
            {assign var="unique_elm_id" value=$item1_url|md5}
            {assign var="unique_elm_id" value="topmenu_`$block.block_id`_`$unique_elm_id`"}

            {if $subitems_count}

            {/if}
            <li class="ty-menu__item{if !$item1.$childs} ty-menu__item-nodrop{else} cm-menu-item-responsive{/if}{if $item1.active || $item1|fn_check_is_active_menu_item:$block.type} ty-menu__item-active{/if}{if $item1.class} {$item1.class}{/if}">
                    {if $item1.$childs}
                        <a class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
                            <i class="ty-menu__icon-open ty-icon-down-open"></i>
                            <i class="ty-menu__icon-hide ty-icon-up-open"></i>
                        </a>
                    {/if}
                    <a {if $item1_url} href="{$item1_url}"{/if} class="ty-menu__item-link">
                        {$item1.$name}
                    </a>
                {if $item1.$childs}

                    {if !$item1.$childs|fn_check_second_level_child_array:$childs}
                        <div class="ty-menu__submenu">
                            <ul class="ty-menu__submenu-items ty-menu__submenu-items-simple cm-responsive-menu-submenu">
                                {hook name="blocks:topmenu_dropdown_2levels_elements"}

                                {foreach from=$item1.$childs item="item2" name="item2"}
                                    {assign var="item_url2" value=$item2|fn_form_dropdown_object_link:$block.type}
                                    <li class="ty-menu__submenu-item{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item2.class} {$item2.class}{/if}">
                                        <a class="ty-menu__submenu-link" {if $item_url2} href="{$item_url2}"{/if}>{$item2.$name}</a>
                                    </li>
                                {/foreach}
                                {if $item1.show_more && $item1_url}
                                    <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
                                        <a href="{$item1_url}"
                                           class="ty-menu__submenu-alt-link">{__("text_topmenu_view_more")}</a>
                                    </li>
                                {/if}

                                {/hook}
                            </ul>
                        </div>
                    {else}
                        <div class="ty-menu__submenu" id="{$unique_elm_id}">
                            {hook name="blocks:topmenu_dropdown_3levels_cols"}
                                <ul class="ty-menu__submenu-items cm-responsive-menu-submenu">
                                    {foreach from=$item1.$childs item="item2" name="item2"}
                                        <li class="ty-top-mine__submenu-col">
                                            {assign var="item2_url" value=$item2|fn_form_dropdown_object_link:$block.type}
                                            <div class="ty-menu__submenu-item-header{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-header-active{/if}{if $item2.class} {$item2.class}{/if}">
                                                <a{if $item2_url} href="{$item2_url}"{/if} class="ty-menu__submenu-link">{$item2.$name}</a>
                                            </div>
                                            {if $item2.$childs}
                                                <a class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
                                                    <i class="ty-menu__icon-open ty-icon-down-open"></i>
                                                    <i class="ty-menu__icon-hide ty-icon-up-open"></i>
                                                </a>
                                            {/if}
                                            <div class="ty-menu__submenu">
                                                <ul class="ty-menu__submenu-list cm-responsive-menu-submenu">
                                                    {if $item2.$childs}
                                                        {hook name="blocks:topmenu_dropdown_3levels_col_elements"}
                                                        {foreach from=$item2.$childs item="item3" name="item3"}
                                                            {assign var="item3_url" value=$item3|fn_form_dropdown_object_link:$block.type}
                                                            <li class="ty-menu__submenu-item{if $item3.active || $item3|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item3.class} {$item3.class}{/if}">
                                                                <a{if $item3_url} href="{$item3_url}"{/if}
                                                                        class="ty-menu__submenu-link">{$item3.$name}</a>
                                                            </li>
                                                        {/foreach}
                                                        {if $item2.show_more && $item2_url}
                                                            <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
                                                                <a href="{$item2_url}"
                                                                   class="ty-menu__submenu-link">{__("text_topmenu_view_more")}</a>
                                                            </li>
                                                        {/if}
                                                        {/hook}
                                                    {/if}
                                                </ul>
                                            </div>
                                        </li>
                                    {/foreach}
                                    {if $item1.show_more && $item1_url}
                                        <li class="ty-menu__submenu-dropdown-bottom">
                                            <a href="{$item1_url}">{__("text_topmenu_more", ["[item]" => $item1.$name])}</a>
                                        </li>
                                    {/if}
                                </ul>
                            {/hook}
                        </div>
                    {/if}

                {/if}
            </li>
        {/foreach}

        {/hook}
    </ul>*}
