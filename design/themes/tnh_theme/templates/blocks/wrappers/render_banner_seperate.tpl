{** block-description:render_banner_seperate **}
{if $items|count > 1}
{capture name="items"}{$items|@shuffle}{/capture}
{/if}
<div class="row" {if $block.user_class != "show_mobile"}style="margin-bottom:2rem;"{/if}>
    <div class="columns small-12 medium-12 large-12 {if $block.user_class == "show_mobile"}show-for-small-only{else}hide-for-small-only{/if}"> 
{foreach from=$items item="banner" name="foo" key="key"}
    {if $smarty.foreach.foo.iteration==1}
        {if $banner.url != ""}<a href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
            {include file="common/image.tpl" images=$banner.main_pair lazy_load="true"}
            {if $banner.url != ""}</a>{/if}
    {/if}
{/foreach}
    </div>
</div>