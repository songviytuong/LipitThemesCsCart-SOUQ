{** block-description:render_main_slider **}
{*{capture name="items"}{$items|@shuffle}{/capture}*}
<div class="small-12 xlarge-9 rel">
<div class="large-12 columns market-box" data-track-banner="marketingBox6518">
{foreach from=$items item="banner" name="foo" key="key"}
    <div class="slide feature-panel hide">
        {if $banner.url != ""}<a href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
            {include file="common/image.tpl"  class="slick-loading" images=$banner.main_pair lazy_load="true"}
            {if $banner.url != ""}</a>{/if}
            <a href="{$banner.url|fn_url}" class="overlay-label">{$banner.banner}</a>
    </div>
{/foreach}
</div>
</div>