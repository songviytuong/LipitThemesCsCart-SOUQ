{** block-description:render_logo **}
<div class="small-5 medium-5 large-3 columns">
    <ul class="h-list logoList text-default">
        <li class="burger-menu">
            <a data-toggle="offCanvasLeft" href="javascript:void(0);" class="left-off-canvas-toggle">
                <i class="fi-menu"></i>
            </a>
        </li>
        <li>
            <a href="{""|fn_url}" title="{$logos.theme.image.alt}"><img src="images/logos/8/souq-logo-v2.png"  alt="{$logos.theme.image.alt}" class="logo" data-interchange="[https://cf1.s3.souqcdn.com/public/style/img/en/souq-logo-v2-X2.png, retina]" /></a>
            <span class="header-title-text hide"></span>
        </li>
    </ul>
</div>