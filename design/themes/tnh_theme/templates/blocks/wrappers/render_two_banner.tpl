{** block-description:render_two_banner **}
{*{capture name="items"}{$items|@shuffle}{/capture}*}
<div class="show-for-xlarge-up xlarge-3 columns">
    {foreach from=$items item="banner" name="foo" key="key"}
    <div class="large-12 columns">
        <div class="row ad">
            <div class="show-for-medium-up small-12 columns ads-box clearPadding-h">
                {if $banner.url != ""}<a href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
            {include file="common/image.tpl" images=$banner.main_pair lazy_load="true"}
            {if $banner.url != ""}</a>{/if}
            </div>
        </div>
    </div>
    {/foreach}
</div>