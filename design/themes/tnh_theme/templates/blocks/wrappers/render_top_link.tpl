{** block-description:render_top_link **}
{if $block.properties.show_items_in_line == 'Y'}
    {assign var="inline" value=true}
{/if}
{assign var="text_links_id" value=$block.snapping_id}

{if $block.user_class == 'Left'}
    <ul class="teasers-list small-5 small-push-1 columns show-for-xlarge ">
        {if $items}
            {foreach from=$items item="menu"}
                {if $menu.param_2 == 'free-shipping'}
                    <li class="free-shipping-teaser">
                    <i class="fi-cargo-truck"></i>&nbsp;
                    <a href="javascript:void(0);" title="{$menu.class}" data-tooltip="" data-options=""><strong>{$menu.item}</strong></a> </li>
                {/if}
            {/foreach}
        
        <li id="returnsTeaser">
            <i class="fi-loop"></i>&nbsp;
            <a href="javascript:void(0);" title="Changed your mind, you can return your product and get a full refund." data-tooltip="" data-options=""><small>Free Returns</small></a> </li>
        <li id="CODTeaser" style="position: relative;">
            <i class="fi-dollar"></i>&nbsp;
            <a href="javascript:void(0);" title="Pay for your order in cash at the moment the shipment is delivered to your doorstep." data-tooltip="" data-options=""><small>Cash on Delivery</small></a> </li>
        <li class="show-for-xlarge"><i class="fi-telephone"></i>&nbsp;
            <span dir="ltr">
                <small>(+971)-4-437 0900</small>
            </span>
        </li>
        {/if}
    </ul>
{else}
    <ul class="h-list account-list small-12 medium-12 large-8 xlarge-6 columns">
        <li id="isPrime" class="hide">

        </li>
        <li class="dodLink show-for-non-minimal">
            <a href="https://deals.souq.com/ae-en/">Daily Deals</a> </li>

        <li class="show-for-non-minimal">
            <a href="https://sell.souq.com">Sell with Us</a> </li>

        <li class="myAccountLink show-for-non-minimal">
            <a href="/ae-en/account.php">Track Orders</a> </li>

        <li id="Ship_To" class="region with-flyout" data-flyout-content><a href="javascript: void(0);" data-flyout="cbt-popUp" title="Ship to United Arab Emirates"><i class="header-country-flag flag_ae"></i></a>
            <div class="flyout cbt-popUp">
                <ul id="cbt-popUp" class="option2">
                    <li>
                        <label><b>Ship to </b>
                        </label>
                        <select id="selectCountries" name="selectCountries">
                            <option value="ae" selected="selected">UAE</option>
                            <option value="eg">Egypt</option>
                            <option value="sa">Saudi</option>
                            <option value="kw">Kuwait</option>
                            <option disabled="disabled">-- International Shipping Countries --</option>
                            <option value="bh">Bahrain</option>
                            <option value="om">Oman</option>
                            <option value="qa">Qatar</option>
                        </select>
                    </li>
                    <li>
                        <label><b>Language</b>
                        </label>
                        <input type="radio" id="languge-typeen" name="languge-type" value="en" checked="checked" />
                        <label for="languge-typeen">English</label>
                        <input type="radio" id="languge-typear" name="languge-type" value="ar" />
                        <label for="languge-typear">عربي</label>
                        <a href="javascript:void(0);" id="change-contries" class="button tiny expand">Save & Continue</a> </li>
                    <li class="local">Souq.com Local Sites</li>
                    <li class="countries">
                        <a href="/">UAE</a> <a href="https://egypt.souq.com/">Egypt</a> <a href="https://saudi.souq.com/">Saudi</a> <a href="https://kuwait.souq.com/">Kuwait</a> </li>
                </ul>
            </div>
        </li>
        <li class="region">
            <a href="/ae-ar/" class="lang-link">العربية</a> </li>
        <li id="login_user_name_topbar" class=" ellipsis-loading with-flyout user_login login-user-name text-center remove-pointer-events has-tip">
            <span class="White" id="userNameLoading">...</span>
            <a id="userNameField_topbar" href="javascript:void(0);" class="truncate userNameField"></a>
            <!--  -->
            <div class="flyout user-menu hidden">
                <ul id="userName_topbar" class="filter-group">
                    <li class="login-li row loginRemoval">
                        <div class="small-12 columns">
                            <a href="/ae-en/login.php" class="login-button" local="">Log In</a> </div>
                        <div class="to-sign-up small-12 columns">
                            <span class="register-now">Don't have an account?			<a href="/ae-en/register.php" class="login_to_register">Sign Up</a>		</span>
                        </div>

                    </li>

                    <li><a href="/ae-en/account.php" class="change-if-seller">My Orders</a> </li>
                    <li><a href="/ae-en/account_addresses.php">My Addresses</a> </li>
                    <li><a href="/ae-en/account_lists.php">Wish Lists</a> </li>
                    <li><a href="/ae-en/account_settings.php">Account Settings</a> </li>
                    <li class="toRemove"><a href="/ae-en/account_overview.php">Account Summary</a> </li>
                    <li> </li>

                    <li class="toRemove"><a href="/ae-en/logout.php">Logout</a> </li>
                </ul>
            </div>
        </li>
    </ul>
{/if}