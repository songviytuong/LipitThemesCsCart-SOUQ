{** block-description:render_small_banner_3 **}
{capture name="items"}{$items|@shuffle}{/capture}
<div class="spring-dynamic-dfp" data-width="300" data-height="60" style="height:60px;">
{foreach from=$items item="banner" name="foo" key="key"}
    {if $smarty.foreach.foo.iteration==1}
        {if $banner.url != ""}<a href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
            {include file="common/image.tpl" images=$banner.main_pair lazy_load="true"}
            {if $banner.url != ""}</a>{/if}
    {/if}
{/foreach}
</div>