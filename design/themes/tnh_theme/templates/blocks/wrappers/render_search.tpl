{** block-description:render_search **}
<div class="small-12 medium-7 large-9 columns extra search-wrapper clearPadding-h-small" id="search-mobile">
    <div id="mini-cart_topbar" class="mini-cart mini-cart_topbar_aj with-flyout" data-xleft-items="0" data-flyout-content>
        <a href="/ae-en/shopping_cart.php" class="disabled" data-options="align:left" data-flyout="cartDrop_mobile"><i class='fi-cart'></i> <span class='cart-item-count-aj show-mini-cart-aj cart-icon-item-count hide'></span></a>  
        <div id="cartDrop_topbar" class="cartDropItem cart-dropdown flyout " data-dropdown-content></div>
    </div>

    <div class="search clearfix">
        <div class="wrap small-12 medium-9 large-7 columns">
            <div>
                <form action="/ae-en/search_results.php" id="searchForm" method="get"><input type="hidden" id="hitsCfs" name="hitsCfs" value="bc1a0621c5b7a22b176a94830c93c3a2" /><input type="hidden" id="hitsCfsMeta" name="hitsCfsMeta" value="vOD9d6iA6iuwDFl+xBbszg9dx3EcY2v+OqOM8hOoNgjj6VoZN0OMKDedm8oAX5hDhtNQuKmUXMFG1K0wyLATka1DjpwP+foc1Ukw+MFWhiPwl3DjbI6cTlXT0UMHSVkbbJYphFqLC/qgQyPsrEFHcod9TZAAodAeP1Ys3OlObgQ=" />            <div id="search_box" class="row collapse relative">
                        <div class="columns small-10  large-11 relative">
                            <input type="text" id="search_value" name="q" placeholder="What are you looking for?" autocomplete="off" tabindex="-1">                </div>
                        <div class="columns small-2 large-1 relative">
                            <div class="button radius postfix relative">
                                <input type="submit" id="searchButton" value="" class="formButton expand block" />                                                            <i class="fi-magnifying-glass"></i>
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
    </div>
</div>
{*
<div class="ty-search-block">
    <form action="{""|fn_url}" name="search_form" method="get">
        <input type="hidden" name="subcats" value="Y" />
        <input type="hidden" name="pcode_from_q" value="Y" />
        <input type="hidden" name="pshort" value="Y" />
        <input type="hidden" name="pfull" value="Y" />
        <input type="hidden" name="pname" value="Y" />
        <input type="hidden" name="pkeywords" value="Y" />
        <input type="hidden" name="search_performed" value="Y" />

        {hook name="search:additional_fields"}{/hook}

        {strip}
            {if $settings.General.search_objects}
                {assign var="search_title" value=__("search")}
            {else}
                {assign var="search_title" value=__("search_products")}
            {/if}
            <input type="text" name="q" value="{$search.q}" id="search_input{$smarty.capture.search_input_id}" title="{$search_title}" class="ty-search-block__input cm-hint" />
            {if $settings.General.search_objects}
                {include file="buttons/magnifier.tpl" but_name="search.results" alt=__("search")}
            {else}
                {include file="buttons/magnifier.tpl" but_name="products.search" alt=__("search")}
            {/if}
        {/strip}

        {capture name="search_input_id"}{$block.snapping_id}{/capture}

    </form>
</div>*}
